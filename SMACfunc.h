#ifndef _WINDOWS_
#include "windows.h"
#endif

#define HEIGHT 30
#define MAINWIDTH 42

enum INFO
{
	INFO_NAME,		// Names of all players
	INFO_HLTH,		// Health of all players
	INFO_HAND,		// Send card index for each card in player's hand
	INFO_CARD,		// Signals request for card selection; Must send endx, deque<TARGET>*, a string to display on client side or a zero-length string to default to "Choose a card:", and CARD* (activecard)
	INFO_XSCR,		// Sent after INFO_CARD/INFO_CNTR/INFO_TRGT to add stats for extra menus; Must send a string, card, callback function, and ELIST*
	INFO_CLRS,		// Signals request to clear client output screen
	INFO_CNTR,		// Signals request for counter selection; Must send endx, deque<TARGET>*, a string to display on client side or a zero-length string to default to "Choose a counter:", and CARD* (activecard)
	INFO_TRGT,		// Signals player targeting; Must send endx, deque<TARGET>*, and a string to display on client side or a zero-length string to default to "Choose a target:", and CARD* (activecard)
	INFO_DECK,		// Send card information for master deck
	INFO_FFLD,		// Send information on the entire visible playing field
	INFO_MMSG,		// Send a message to be displayed; Must send a string (can be multiline) and a WORD for color
	INFO_EPLR,		// Send health of an extra player; Must send card index and health
	INFO_QUIT,		// Signals end of game
	INFO_MENU,		// PassInfo: Send a menu choice from client back to host
	INFO_MSEL,		// Signals request for multiple card selection; Must send endx, deque<TARGET>*, and a string to display on client side or a zero-length string to default to "Choose multiple cards:"
};

unsigned long	cwrite(char* str, int x, int y, WORD attr = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE, int clrlength = MAINWIDTH);
int				kget(void); //224 + 72U,80D,75L,77R
void			clearkbd();
int				iget(int scrindex, int x = 0, WORD attr = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
int				lget(int scrindex, char buf[], int len);
bool			bget(int scrindex);
int				menu(int scrindex, int max, int defchoice = 0);
