#include <stdio.h>
#include <process.h>
#include <tchar.h>
#include "CommFunc.h"

extern void LazyFail(char* errmsg);

static int (*BlockFunction)(void);

void SocketError(char* errmsg, int nError)
{
	char buf[256];

	if(nError == 0)
		nError = WSAGetLastError();

	sprintf_s(buf, 256, "%S: %d", errmsg, nError);
	LazyFail(buf);
}

int InitSock(void)
{
	WSADATA wsaData;
	int nError = 0;

	nError = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if(nError != 0)
	{
		SocketError("InitSock: WSAStartup( failed", nError);
		return 1;
	}

	SetBlock(NULL);

	return 0;
}

int InitServ(SOCKET *mysock)
{
	int nError = 0;
	int reuse = 1;
	ADDRINFO hints, *result;
	unsigned long nblock = 1;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	nError = getaddrinfo(NULL, PORT, &hints, &result);
	if(nError != 0)
	{
		SocketError("InitServ: getaddrinfo( failed", nError);
		WSACleanup();
		return 1;
	}

	*mysock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if(*mysock == INVALID_SOCKET)
	{
		SocketError("InitSock: socket( failed");
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	nError = ioctlsocket(*mysock, FIONBIO, &nblock);
	if(nError == SOCKET_ERROR)
	{
		SocketError("InitServ: ioctlsocket( failed");
		closesocket(*mysock);
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	nError = setsockopt(*mysock, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse, sizeof(reuse));
	if(nError == SOCKET_ERROR)
	{
		SocketError("InitServ: setsockopt( failed");
		closesocket(*mysock);
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	nError = bind(*mysock, result->ai_addr, result->ai_addrlen);
	if(nError == SOCKET_ERROR)
	{
		nError = WSAGetLastError();
		SocketError("InitServ: bind( failed");
		closesocket(*mysock);
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}
	freeaddrinfo(result);

	nError = listen(*mysock, SOMAXCONN);
	if(nError == SOCKET_ERROR)
	{
		SocketError("InitServ: listen( failed");
		closesocket(*mysock);
		WSACleanup();
		return 1;
	}

	return 0;
}

int GetClient(SOCKET *mysock, SOCKET *theirsock, SOCKADDR_IN *theiraddr, bool block)
{
	int nError = 0;
	int addrlen = sizeof(*theiraddr);

	nError = SockBlock(1, 0, block, mysock);
	if(nError == 0)
		return -1;
	*theirsock = accept(*mysock, (SOCKADDR*)theiraddr, &addrlen);
	if(*theirsock == INVALID_SOCKET)
	{
		nError = WSAGetLastError();
		if(nError == WSAEWOULDBLOCK)
			return -1;
		else
		{
			SocketError("GetClient: accept( failed");
			closesocket(*mysock);
			WSACleanup();
			return 1;
		}
	}

	return 0;
}

int GetServ(SOCKET *mysock, char* addr)
{
	int nError;
	ADDRINFO hints, *result;
	unsigned long nblock = 1;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	nError = getaddrinfo(addr, PORT, &hints, &result);
	if(nError != 0)
	{
		printf("GetServ: getaddrinfo( failed: %d\n", nError);
		closesocket(*mysock);
		WSACleanup();
		return 1;
	}

	*mysock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if(*mysock == INVALID_SOCKET)
	{
		SocketError("GetServ: socket( failed");
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	nError = ioctlsocket(*mysock, FIONBIO, &nblock);
	if(nError == SOCKET_ERROR)
	{
		SocketError("GetServ: ioctlsocket( failed");
		freeaddrinfo(result);
		closesocket(*mysock);
		WSACleanup();
		return 1;
	}

	nError = connect(*mysock, result->ai_addr, result->ai_addrlen);
	if(nError == SOCKET_ERROR)
	{
		nError = WSAGetLastError();
		if(WSAGetLastError() != WSAEWOULDBLOCK)
		{
			printf("InitServ: connect( failed: %d\n", nError);
			closesocket(*mysock);
			freeaddrinfo(result);
			WSACleanup();
			return 1;
		}
		nError = SockBlock(0, 1, true, mysock); // Wait until socket is readable
	}
	freeaddrinfo(result);

	return 0;
}

int SockSend(SOCKET *ssock, void* buf, int len)
{
	int nError = 0;
	int nsent = 0;

	do
	{
		SockBlock(0, 1, true, ssock);
		nError = send(*ssock, ((char*)buf) + nsent, len - nsent, 0);
		if(nError == SOCKET_ERROR)
		{
			SocketError("SockSend: send( failed");
			closesocket(*ssock);
			WSACleanup();
			return 0;
		}
		nsent += nError;
	} while(nsent < len);

	return nsent;
}

int SockRecv(SOCKET *ssock, void* buf, int len, bool async)
{
	int nError = 0;
	int nrcvd = 0;
	bool asyncFlag = true;

	do
	{
		if(SockBlock(1, 0, !async, ssock) == 0) // Technically, (async ? false : true)
			return 0; // There is nothing to be read (timed out) so don't bother even trying
		nError = recv(*ssock, (char*)buf + nrcvd, len - nrcvd, 0);
		if(nError == SOCKET_ERROR)
		{
			SocketError("SockRecv: recv( failed");
			closesocket(*ssock);
			WSACleanup();
			return 0;
		}
		nrcvd += nError;
		if(async)
		{ // Even in async mode, we'll only break the loop if absolutely nothing was retrieved
			if(nrcvd == 0) // because if there _was_ something retrieved, we want the rest of it as well
				asyncFlag = false;
		}
	} while(nrcvd < len && asyncFlag);

	return nrcvd;
}

int LazySend(SOCKET *ssock, void *buf, int buflen, char type[4])
{
	char* tbuf;
	short sbuf;
	long lbuf;
	int blen;
	short nlen;

	if(type[0] == 'S')
	{
		blen = strlen((char*)buf);
		tbuf = (char*)malloc(blen + 5);
#ifdef _DEBUG
OutputDebugStringA("H:");
OutputDebugString((LPCTSTR)buf);
OutputDebugStringA("\n");
#endif // _DEBUG
		tbuf[0] = 'S'; // Because it's a string
		nlen = htons((short)blen);
		memcpy_s(&tbuf[1], blen + 4, &nlen, 2); // Two bytes for the length
		memcpy_s(&tbuf[3], blen + 2, buf, blen); // Copy the actual string
	}
	else if(type[0] == 'I')
	{
		blen = buflen;
#ifdef _DEBUG
OutputDebugStringA("H:");
tbuf = (char*)malloc(24);
sprintf_s(tbuf, 24, "%d(0x%X)", *(int*)buf, *(int*)buf);
OutputDebugStringA(tbuf);
OutputDebugStringA("\n");
free(tbuf);
#endif // _DEBUG
		tbuf = (char*)malloc(blen + 4);
		if(buf == NULL)
		{
			char buffer[128];
			_strerror_s(buffer, 128, NULL);
			MessageBox(NULL, buffer, "Error", MB_ICONERROR);
		}
		tbuf[0] = 'I'; // Because it's a string
		nlen = htons((short)blen);
		memcpy_s(&tbuf[1], blen + 3, &nlen, 2); // Two bytes for the length
		if(blen == 2)
		{
			sbuf = *(short*)buf;
			sbuf = htons(sbuf);
			memcpy_s(&tbuf[3], blen + 1, &sbuf, sizeof(short));
		}
		else if(blen == 4)
		{
			lbuf = *(long*)buf;
			lbuf = htonl(lbuf);
			memcpy_s(&tbuf[3], blen + 1, &lbuf, sizeof(long));
		}
		else
		{
			MessageBox(NULL, (LPCTSTR)buf, "Unknown integer length", MB_ICONERROR);
			LazyFail("From LazySend");
			return 2;
		}
	}
	else
	{
		MessageBox(NULL, (LPCTSTR)buf, "Unknown message code", MB_ICONERROR);
		LazyFail("From LazySend");
		return 2;
	}

	if(SockSend(ssock, tbuf, blen + 3) == 0) // + 3 for the message code
	{
		LazyFail("LazySend: Could not SockSend(");
		return 1;
	}
	free(tbuf);

	return 0;
}

int LazyRecv(SOCKET *ssock, void* obuf, int buflen, bool async)
{
	char buf[4];
	short len;
	short sbuf;
	long lbuf;
	int nrcvd;

	nrcvd = SockRecv(ssock, buf, 3, async);
	if(nrcvd == 0 && !async) // Retrieve message code
	{
		LazyFail("LazyRecv: Could not SockRecv( first three bytes");
		return 1;
	}
	else if(nrcvd == 0) // async == true
		return -1;
	buf[3] = 0; // Null-terminate
//	len = atoi(&buf[1]); // Retrieve length of data
	memcpy_s(&len, 2, &buf[1], 2); // Retrieve length of data...
	len = ntohs(len); // ...and convert back to host format
	if(buf[0] == 'S')
	{
		if(len == 0)
		{
			((char*)obuf)[0] = '\0';
			return 0;
		}
		if(len >= (signed)(buflen * sizeof(char)))
		{
			LazyFail("LazyRecv: len received is greater than buflen");
			return 2;
		}
		if(SockRecv(ssock, obuf, len) == 0)
		{
			LazyFail("LazyRecv: Could not SockRecv( rest of message");
			return 3;
		}
		((char*)obuf)[len / sizeof(char)] = '\0'; // Null-terminate
	}
	else if(buf[0] == 'I')
	{
		if(len == 2)
		{
			if(len > buflen)
			{
				LazyFail("LazyRecv: len/buflen mismatch");
				return 2;
			}
			if(SockRecv(ssock, &sbuf, len) == 0)
			{
				LazyFail("LazyRecv: Could not SockRecv( rest of message");
				return 3;
			}
			sbuf = ntohs(sbuf);
			memcpy(obuf, &sbuf, len);
		}
		else if(len == 4)
		{
			if(len > buflen)
			{
				LazyFail("LazyRecv: len/buflen mismatch");
				return 2;
			}
			if(SockRecv(ssock, &lbuf, len) == 0)
			{
				LazyFail("LazyRecv: Could not SockRecv( rest of message");
				return 3;
			}
			lbuf = ntohl(lbuf);
			memcpy(obuf, &lbuf, len);
		}
		else
		{
			MessageBox(NULL, buf, "Aborting...", MB_ICONERROR);
			LazyFail("LazyRecv: Unknown message code");
			return 5;
		}
	}
	else
	{
		MessageBox(NULL, buf, "Aborting...", MB_ICONERROR);
		LazyFail("LazyRecv: Unknown message code");
		return 4;
	}

	return 0;
}

int SetBlock(int (*func)(void))
{
	BlockFunction = func;
	return 0;
}

int SockBlock(int nrsock, int nwsock, bool block, ...)
{
	FD_SET rfds, wfds;
	FD_SET tmprfds, tmpwfds;
	int i, nError = 0;
	va_list arglist;
	SOCKET *ssock;
	TIMEVAL tval;

	tval.tv_sec = 0;
	tval.tv_usec = 100;

	va_start(arglist, block);

	FD_ZERO(&rfds);
	FD_ZERO(&wfds);

	for(i = 0; i < nrsock; i++)
	{
		ssock = va_arg(arglist, SOCKET*);
		#pragma warning(disable: 4127) // Necessary for FD_SET
		FD_SET(*ssock, &rfds);
		#pragma warning(default:4127)
	}
	for(i = 0; i < nwsock; i++)
	{
		ssock = va_arg(arglist, SOCKET*);
		#pragma warning(disable: 4127) // Necessary for FD_SET
		FD_SET(*ssock, &wfds);
		#pragma warning(default:4127)
	}

	va_end(arglist);

	if(BlockFunction != NULL && block == true)
	{
		do
		{
			memcpy(&tmprfds, &rfds, sizeof(FD_SET));
			memcpy(&tmpwfds, &wfds, sizeof(FD_SET));

			nError = select(0, &tmprfds, &tmpwfds, NULL, &tval);
			if(nError == SOCKET_ERROR)
			{
				SocketError("SockBlock: select( failed");
				WSACleanup();
				return 0;
			}
			BlockFunction();
		} while(nError == 0);
	}
	else
	{
		nError = select(0, &rfds, &wfds, NULL, block ? NULL : &tval);
		if(nError == SOCKET_ERROR)
		{
			SocketError("SockBlock: select( failed");
			WSACleanup();
			return 0;
		}
	}

	return nError;
}
