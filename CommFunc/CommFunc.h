#define _WINSOCKAPI_
#ifndef _WINSOCK2API_
#include <WinSock2.h>
#endif
#ifndef _WS2TCPIP_H_
#include <ws2tcpip.h>
#endif

#define PORT "8081"

void SocketError(char* errmsg, int nError = 0);

int InitSock(void);

int InitServ(SOCKET *mysock);
int GetClient(SOCKET *mysock, SOCKET *theirsock, SOCKADDR_IN *theiraddr, bool block);

int GetServ(SOCKET *mysock, char* addr);

int SockSend(SOCKET *ssock, void* buf, int len);
int SockRecv(SOCKET *ssock, void* buf, int len, bool async = false);

int LazySend(SOCKET *ssock, void* buf, int buflen, char type[4]);
int LazyRecv(SOCKET *ssock, void* buf, int buflen, bool async = false);
/*
   LazySend/Recv Formats:
	First three bytes are info, rest is data
	First byte: "S"tring or "I"nt
	Second two bytes: Size of data in bytes (as a short in network byte order, not as a string)
	String data is Unicode format
	Integer data is in network byte order
*/

int SetBlock(int (*func)(void));
int SockBlock(int nrsock, int nwsock, bool block, ...);
/*
	nrsock = number of sockets to be blocked on read
	nwsock = number of sockets to be blocked on write
	...    = list of sockets
*/
