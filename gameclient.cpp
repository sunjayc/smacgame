#include <conio.h>
#include <deque>
using namespace std;
#include "CommFunc/CommFunc.h"
#include <shellapi.h>
#include "SMACclient.h"
#include "interface.h"
#include "SMACfunc.h"

extern int lzf;
extern HANDLE hScr;

// Globals
CPLAY cf;
int infoboxchoice; // Currently selected infobox (0 = main window  1 = player window  2 = field window)
bool inmenu;
CCARD *activecard; // Active card that the player's menu is countering/whatever
bool dbgmodec;

/**
 * Contains information about a subsection of the game screen.
 */
struct INFOBOX
{
	WORD* normAttrib; ///< Unselected colors
	WORD* selAttrib; ///< Selected colors
	COORD size; ///< Dimensions of this infobox
	SMALL_RECT sm; ///< Position of this infobox in the main buffer
	int sz; ///< Number of total WORDs in normAttrib and selAttrib
	int oldheight; ///< For playerw and fieldw so they know how much to clear when their lists decrease

	short choice; ///< Currently selected card or menu item
	short xchoice; ///< Currently selected player in player infobox OR currently selected submenu in mainw
	short maxval; ///< Number of cards / menu items in infobox
	CCARD *curc; ///< Current card pointed to, once again can also be used for the menus _when_ I get around to it
};
INFOBOX ib[4];

#define MENU_REG	0x0001
#define MENU_CARDS	0x0002
#define MENU_MULTI	0x0004
struct CLIENTMENUSTRUCT
{
	short endx; ///< Corresponding event in server's event list
	char msg[81]; ///< Title message
	short cindx; ///< Index of active card
	deque<TARGET> ct; ///< Target list used for menus
	unsigned short menustyle; ///< Type of menu (MENU_ above)
};
deque<CLIENTMENUSTRUCT> menus;

// Ease-of-access #defines
#define thisp cf.cp[cf.cnum]
#define sindx (cf.nump + 2)
#define mainw ib[0]
#define playerw ib[1]
#define fieldw ib[2]
#define discardw ib[3]
#define curib ib[infoboxchoice]

// Functions
int		InitGameC();
int		RecvDeck();
int		RecvInfo();
int		LazyPull();
int		Interface();
void	RefreshField();
void	ChangeInfoboxes();
void	Arrow(int ibnum = infoboxchoice);
void	LazyFailC(char* errmsg);

short			swrite(char* str, WORD baseattr = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE, bool highlight = false, bool menusetup = false);
int				smenu(char* msg, short max, short defchoice = 0);
void			bottomcard();
void			playerib();
void			fieldib();

int gameclient()
{
	int i;

	lzf = 1;
	dbgmodec = false;

	infoboxchoice = 0;
	inmenu = false;
	mainw.normAttrib = NULL;
	mainw.selAttrib = NULL;
	mainw.choice = 0;
	mainw.xchoice = 0;
	playerw.normAttrib = NULL;
	playerw.selAttrib = NULL;
	playerw.choice = 0;
	playerw.xchoice = 0;
	playerw.oldheight = 0;
	fieldw.normAttrib = NULL;
	fieldw.selAttrib = NULL;
	fieldw.choice = 0;
	fieldw.oldheight = 0;
	discardw.normAttrib = NULL;
	discardw.selAttrib = NULL;

	if(InitGameC() != 0)
	{
		LazyFailC("InitGameC failed");
		return 1;
	}

	mainw.sz = MAINWIDTH * (HEIGHT - sindx); // Because sindx is based on cf.nump, which isn't defined until after InitGameC(
	mainw.normAttrib = new WORD[mainw.sz];
	mainw.selAttrib = new WORD[mainw.sz];
	mainw.size.X = MAINWIDTH;
	mainw.size.Y = HEIGHT - sindx;
	mainw.sm.Top = sindx;
	mainw.sm.Left = 0;
	mainw.sm.Bottom = HEIGHT - 1;
	mainw.sm.Right = 41;
	playerw.xchoice = cf.cnum; // Default choice is the current player
	for(i = 0; i < mainw.sz; i++)
	{
		mainw.normAttrib[i] = FOREGROUND_INTENSITY;
		mainw.selAttrib[i] = 7;
	}

	cwrite("", 0, sindx - 1); // Just in case there's anything there
	swrite("\n");

	SetBlock(&Interface);
	RecvInfo(); // Essentially main game loop

	closesocket(cf.psock);
	WSACleanup();

	return 0;
}

int RecvDeck()
{
	CCARD ctmp;
	short dnum = 0;
	int i;

	LazyRecv(&cf.psock, &dnum, 2);
	if(dnum == -1)
	{
		cf.d.clear();
		return 0;
	}
	for(i = 0; i < dnum; i++)
	{
		LazyRecv(&cf.psock, ctmp.name, 35);
		LazyRecv(&cf.psock, ctmp.desc, 133);
		LazyRecv(&cf.psock, ctmp.flavtxt, 67);
		LazyRecv(&cf.psock, &ctmp.aff, 2);
		LazyRecv(&cf.psock, &ctmp.type, 4);
		LazyRecv(&cf.psock, &ctmp.id, 2);
		LazyRecv(&cf.psock, &ctmp.atype, 2);
		LazyRecv(&cf.psock, &ctmp.health, 2);
		cf.d.push_back(ctmp);
	}

	return 0;
}

int InitDbgC(int nump)
{
	int i;
	char hostname[81];

	lzf = 1;
	dbgmodec = true;

	infoboxchoice = 0;
	inmenu = false;
	mainw.normAttrib = NULL;
	mainw.selAttrib = NULL;
	mainw.choice = 0;
	mainw.xchoice = 0;
	playerw.normAttrib = NULL;
	playerw.selAttrib = NULL;
	playerw.choice = 0;
	playerw.xchoice = 0;
	playerw.oldheight = 0;
	fieldw.normAttrib = NULL;
	fieldw.selAttrib = NULL;
	fieldw.choice = 0;
	fieldw.oldheight = 0;
	discardw.normAttrib = NULL;
	discardw.selAttrib = NULL;

	hostname[0] = '\0'; // "Blank out" hostname
	cwrite("Connecting...", 0, 0);
	if(InitSock() != 0) // Sets blocking on by default
		return 1;
	if(GetServ(&cf.psock, hostname) != 0) // Will block
	{
		LazyFailC("Could not get host");
		return 1;
	}

	cwrite("Waiting for game start...", 0, 3);

	if(nump > 0)
	{
		LazySend(&cf.psock, &nump, 2, "I");
		for(i = 0; i < (nump - 1); i++)
		{
			ShellExecute(NULL, "open", "SMACgame.exe", "DEBUGC", NULL, SW_SHOWNA);
		}
	}

	LazyRecv(&cf.psock, &cf.nump, sizeof(cf.nump)); // Receive number of players from server
	LazyRecv(&cf.psock, &cf.cnum, sizeof(cf.cnum)); // Receive index of this player

	mainw.sz = MAINWIDTH * (HEIGHT - sindx); // Because sindx is based on cf.nump, which isn't defined until after InitGameC(
	mainw.normAttrib = new WORD[mainw.sz];
	mainw.selAttrib = new WORD[mainw.sz];
	mainw.size.X = MAINWIDTH;
	mainw.size.Y = HEIGHT - sindx;
	mainw.sm.Top = sindx;
	mainw.sm.Left = 0;
	mainw.sm.Bottom = HEIGHT - 1;
	mainw.sm.Right = 41;
	playerw.xchoice = cf.cnum; // Default choice is the current player
	for(i = 0; i < mainw.sz; i++)
	{
		mainw.normAttrib[i] = FOREGROUND_INTENSITY;
		mainw.selAttrib[i] = 7;
	}

	cwrite("", 0, sindx - 1); // Just in case there's anything there
	swrite("\n");

	SetBlock(&Interface);
	RecvInfo(); // Essentially main game loop

	closesocket(cf.psock);
	WSACleanup();

	return 0;
}

int InitGameC()
{
	char buf[81];
	char hostname[81];

	cwrite("Host to connect to? (Max: 31)", 0, 0);
	lget(1, hostname, 35);

	cwrite("Connecting...", 0, 0);
	if(InitSock() != 0) // Sets blocking on by default
		return 1;
	if(GetServ(&cf.psock, hostname) != 0) // Will block
	{
		LazyFailC("Could not get host");
		return 1;
	}

	cwrite("Player name? (Max: 34)", 0, 0);
	cwrite("", 0, 1);
	lget(1, buf, 35);

	if(buf[0] == '\0') // If the user didn't enter anything
	{
		gethostname(buf, 35); // Default the name to the user's computer's name truncated to 35 bytes
		cwrite(buf, 0, 1);
	}

	LazySend(&cf.psock, buf, 0, "S");

	cwrite("Waiting for game start...", 0, 3);

	LazyRecv(&cf.psock, &cf.nump, sizeof(cf.nump)); // Receive number of players from server
	LazyRecv(&cf.psock, &cf.cnum, sizeof(cf.cnum)); // Receive index of this player

	return 0;
}

int RecvInfo()
{
	int i;
	long info = 0;
	INFO linfo;
	char buf[81];
	short tnum;
	short force;
	TARGET ttmp;
	WORD attr;

	do
	{
		LazyRecv(&cf.psock, &info, 4);
		linfo = (INFO)info;

		clearkbd();

		switch(linfo)
		{
			case INFO_MMSG:
			{
				LazyRecv(&cf.psock, buf, 81);
				LazyRecv(&cf.psock, &attr, 2);
				swrite(buf, attr, true);
				break;
			}
			case INFO_NAME:
			{
				for(i = 0; i < cf.nump; i++)
				{
					LazyRecv(&cf.psock, cf.cp[i].name, 32);
				}

				cwrite("--Health--", 0, 0); // (Re)update everyone's name
				for(i = 0; i < cf.nump; i++)
				{
					sprintf_s(buf, 81, "%s: %d", cf.cp[i].name, cf.cp[i].health); // Print everyone's name and health
					if(i == cf.cnum)
						cwrite(buf, 0, i + 1, FOREGROUND_GREEN);
					else
						cwrite(buf, 0, i + 1);
				}
				break;
			}
			case INFO_HLTH:
			{
				for(i = 0; i < cf.nump; i++)
				{
					LazyRecv(&cf.psock, &cf.cp[i].health, 2);
				}

				cwrite("--Health--", 0, 0); // (Re)update everyone's health
				for(i = 0; i < cf.nump; i++)
				{
					sprintf_s(buf, 81, "%s: %d", cf.cp[i].name, cf.cp[i].health); // Print everyone's name and health
					if(i == cf.cnum)
						cwrite(buf, 0, i + 1, FOREGROUND_GREEN);
					else
						cwrite(buf, 0, i + 1);
				}
				break;
			}
			case INFO_DECK:
			{
				RecvDeck();
				break;
			}
			case INFO_HAND:
			{
				short cindx;
				short hnum = 0;

				cf.hand.clear();
				LazyRecv(&cf.psock, &hnum, 2);
				for(i = 0; i < hnum; i++)
				{
					LazyRecv(&cf.psock, &cindx, 2);
					cf.hand.push_back(&cf.d[cindx]);
				}
				playerib(); // Refresh the player infobox
				break;
			}
			case INFO_FFLD:
			{
				RefreshField();
				break;
			}
			case INFO_EPLR:
			{
				short cindx;

				LazyRecv(&cf.psock, &cindx, 2);
				LazyRecv(&cf.psock, &cf.d[cindx].health, 2);
				playerib();
				break;
			}
			case INFO_CLRS:
			{
				swrite("\n");
				break;
			}
			case INFO_CARD:
			case INFO_CNTR:
			{
				CLIENTMENUSTRUCT cms;
				LazyRecv(&cf.psock, &cms.endx, 2);
				LazyRecv(&cf.psock, cms.msg, 81);
				if(cms.msg[0] == '\0')
				{
					if(linfo == INFO_CARD)
					{
						strcpy_s(cms.msg, 81, "Choose a card:");
					}
					else // linfo == INFO_CNTR
					{
						strcpy_s(cms.msg, 81, "Choose a counter:");
					}
				}
				LazyRecv(&cf.psock, &cms.cindx, 2);
				LazyRecv(&cf.psock, &tnum, 2);
				force = -1;
				for(i = 0; i < tnum; i++)
				{
					LazyRecv(&cf.psock, &ttmp.pnum, 2);
					strcpy_s(ttmp.name, 35, cf.d[ttmp.pnum].name);
					LazyRecv(&cf.psock, &ttmp.stat, 2);
					cms.ct.push_back(ttmp);
				}
				if(linfo == INFO_CNTR)
				{
					strcpy_s(ttmp.name, 35, "Don't Counter");
					ttmp.pnum = -1;
					ttmp.stat = 0;
					cms.ct.push_back(ttmp);
					tnum++;
				}
				cms.menustyle = MENU_REG | MENU_CARDS;
				menus.push_back(cms);
				break;
			}
			case INFO_MSEL:
			{
				CLIENTMENUSTRUCT cms;
				LazyRecv(&cf.psock, &cms.endx, 2);
				LazyRecv(&cf.psock, cms.msg, 81);
				if(cms.msg[0] == '\0')
				{
					strcpy_s(cms.msg, 81, "Choose multiple cards:");
				}
				LazyRecv(&cf.psock, &tnum, 2);
				for(i = 0; i < tnum; i++)
				{
					LazyRecv(&cf.psock, &ttmp.pnum, 2);
					strcpy_s(ttmp.name, cf.d[ttmp.pnum].name);
					ttmp.stat = 0;
					cms.ct.push_back(ttmp);
				}
				strcpy_s(ttmp.name, "Done");
				ttmp.stat = 0;
				cms.ct.push_back(ttmp);
				cms.menustyle = MENU_MULTI | MENU_CARDS;
				menus.push_back(cms);
				break;
			}
			case INFO_TRGT:
			{
				CLIENTMENUSTRUCT cms;
				LazyRecv(&cf.psock, &cms.endx, 2);
				LazyRecv(&cf.psock, cms.msg, 81);
				if(cms.msg[0] == '\0')
					strcpy_s(cms.msg, 81, "Choose a target:");
				LazyRecv(&cf.psock, &cms.cindx, 2);
				LazyRecv(&cf.psock, &tnum, 2);
				for(i = 0; i < tnum; i++)
				{
					LazyRecv(&cf.psock, ttmp.name, 35);
					LazyRecv(&cf.psock, &ttmp.pnum, 2);
					LazyRecv(&cf.psock, &ttmp.stat, 2);
					cms.ct.push_back(ttmp);
				}
				cms.menustyle = MENU_REG;
				menus.push_back(cms);
				break;
			}
			default:
				break;
		}
	} while(linfo != INFO_QUIT);

	return 0;
}

int Interface()
{
	int c;
	static int choice;
	CLIENTMENUSTRUCT *cm;

	if(_kbhit() != 0)
	{
		c = kget();
		if(c == 9) // Tab key
		{
			ChangeInfoboxes();
		}
		else if(c == 13) // Enter key
		{
			if(infoboxchoice == 0 && !menus.empty())
			{
				cm = &menus.front();
				if(cm->menustyle & MENU_REG)
				{
					if(cm->ct[mainw.choice].stat == -2)
					{
						COORD coord;
						unsigned long lnum = 0;
						int i;
						coord.X = 2;
						coord.Y = HEIGHT - (short)cm->ct.size() + mainw.choice;
						FillConsoleOutputAttribute(hScr, 15, MAINWIDTH, coord, &lnum);
						for(i = 0; i < mainw.size.X; i++)
						{
							mainw.normAttrib[(coord.Y - sindx) * mainw.size.X + i] = FOREGROUND_INTENSITY;
							mainw.selAttrib[(coord.Y - sindx) * mainw.size.X + i] = 15;
						}
						cm->ct[mainw.choice].stat = 0;
					}
					else if(cm->ct[mainw.choice].stat >= 0)
					{
						LazySend(&cf.psock, &cm->endx, 2, "I");
						LazySend(&cf.psock, &cm->ct[mainw.choice].pnum, 2, "I");
						Arrow(-1); // Special code to make the arrow gray
						menus.pop_front();
						inmenu = false;
						activecard = NULL;
						bottomcard();
						swrite("");
					}
				}
				else if(cm->menustyle & MENU_MULTI)
				{
					if(mainw.choice == mainw.maxval - 1) // "Done"
					{
						unsigned short num;
						deque<short> csel;
						for(unsigned int i = 0; i < cm->ct.size(); i++)
						{
							if(cm->ct[i].stat == 1)
								csel.push_back(cm->ct[i].pnum);
						}
						num = (unsigned short)csel.size();
						LazySend(&cf.psock, &cm->endx, 2, "I");
						LazySend(&cf.psock, &num, 2, "I");
						for(unsigned int i = 0; i < num; i++)
						{
							LazySend(&cf.psock, &csel[i], 2, "I");
						}
						Arrow(-1);
						menus.pop_front();
						inmenu = false;
						activecard = NULL;
						bottomcard();
						swrite("");
					}
					else
					{
						COORD coord;
						unsigned long lnum = 0;
						int i;
						coord.X = 2;
						coord.Y = HEIGHT - (short)cm->ct.size() + mainw.choice;
						short stat = cm->ct[mainw.choice].stat;
						WORD normAttrib = stat == 0 ? FOREGROUND_GREEN : FOREGROUND_INTENSITY;
						WORD selAttrib = stat == 0 ? FOREGROUND_GREEN | FOREGROUND_INTENSITY : 15;
						FillConsoleOutputAttribute(hScr, selAttrib, MAINWIDTH, coord, &lnum);
						for(i = 0; i < mainw.size.X; i++)
						{
							mainw.normAttrib[(coord.Y - sindx) * mainw.size.X + i] = normAttrib;
							mainw.selAttrib[(coord.Y - sindx) * mainw.size.X + i] = selAttrib;
						}
						cm->ct[mainw.choice].stat = stat == 0 ? 1 : 0;
					}
				}
			}
		}
		else if(c == 'p')
		{
			if(infoboxchoice == 0 && !menus.empty())
			{
				cm = &menus.front();
				unsigned int i;
				bool flag = true;
				for(i = 0; i < cm->ct.size(); i++)
				{
					if(cm->ct[i].stat != -1)
					{
						flag = false;
						break;
					}
					if(!flag)
						break;
				}
				if(flag)
				{
					short rval = -1;
					LazySend(&cf.psock, &cm->endx, 2, "I");
					LazySend(&cf.psock, &rval, 2, "I");
					Arrow(-1);
					menus.pop_front();
					inmenu = false;
					activecard = NULL;
					bottomcard();
					swrite("");
				}
			}
		}
		else if(c == 224) // Arrow key
		{
			c = kget(); // To get the second part of the keycode
			if(c == 72 && curib.choice > 0) // Up
			{
				curib.choice--;
			}
			else if(c == 80 && curib.choice < (curib.maxval - 1)) // Down
			{
				curib.choice++;
			}
			else if(infoboxchoice == 1) // Player infobox
			{
				if(c == 75 && playerw.xchoice > 0) // Left
				{
					playerw.xchoice--;
					playerib();
				}
				else if(c == 77 && playerw.xchoice < (cf.nump - 1)) // Right
				{
					playerw.xchoice++;
					playerib();
				}
			}
			Arrow();
		}
	}

	if(!inmenu && !menus.empty())
	{
		cm = &menus.front();
		smenu(cm->msg, (short)cm->ct.size());
		if(cm->cindx >= 0)
		{
			activecard = &cf.d[cm->cindx];
			bottomcard();
		}
		inmenu = true;
	}
	return -1;
}

void RefreshField()
{
	short pindx;
	unsigned long loc;
	short cindx;
	deque<CCARD*> *pile = NULL;
	short ncards;
	int i;

	cf.discard.clear();
	cf.field.clear();
	cf.pranknum = 0;
	for(i = 0; i < cf.nump; i++)
	{
		cf.cp[i].item.clear();
		cf.cp[i].ally.clear();
		cf.cp[i].enmy.clear();
		cf.cp[i].othr.clear();
	}

	LazyRecv(&cf.psock, &ncards, 2);
	for(i = 0; i < ncards; i++)
	{
		LazyRecv(&cf.psock, &cindx, 2);
		LazyRecv(&cf.psock, &pindx, 2);
		LazyRecv(&cf.psock, &loc, 4);

		pile = NULL;

		switch(ID(loc))
		{
			case LOC_DCRD:
			{
				pile = &cf.discard;
				break;
			}
			case LOC_FFLD:
			{
				pile = &cf.field;
				break;
			}
			case LOC_PRNK:
			{
				pile = NULL;
				cf.pranknum++;
				break;
			}
			case LOC_ITEM:
			{
				pile = &cf.cp[pindx].item;
				break;
			}
			case LOC_ALLY:
			{
				pile = &cf.cp[pindx].ally;
				break;
			}
			case LOC_ENMY:
			{
				pile = &cf.cp[pindx].enmy;
				break;
			}
			case LOC_OTHR:
			{
				pile = &cf.cp[pindx].othr;
				break;
			}
		}
		if(pile != NULL)
		{
			pile->push_back(&cf.d[cindx]);
		}
	}
	discardw.maxval = (short)cf.discard.size();
	playerib(); // Refresh the player infobox
	fieldib(); // Refresh the field infobox
}

void ChangeInfoboxes()
{
	COORD coord;
	short i;
	unsigned long linfo;

	if(infoboxchoice != 3) // Not the discard pile
	{
		coord.X = curib.sm.Left;
		for(i = 0; i < curib.size.Y; i++)
		{
			coord.Y = i + curib.sm.Top;
			WriteConsoleOutputAttribute(hScr, curib.normAttrib + (i * (curib.size.X)), curib.size.X, coord, &linfo);
		}
	}
	else
		bottomcard();

	infoboxchoice++;
	infoboxchoice %= 4;

	if(infoboxchoice != 3) // Not the discard pile
	{
		coord.X = curib.sm.Left;
		for(i = 0; i < curib.size.Y; i++)
		{
			coord.Y = i + curib.sm.Top;
			WriteConsoleOutputAttribute(hScr, curib.selAttrib + (i * (curib.size.X)), curib.size.X, coord, &linfo);
		}
	}
	else
		curib.choice = 0;

	Arrow(); // This also takes care of calling card()
}

void Arrow(int ibnum)
{
	int i, j;
	deque<CCARD*> *pile = NULL;

	if(ibnum == -1) // Main (menu) Infobox, menu has just completed
	{
		int scrindex = HEIGHT - menus.front().ct.size() + mainw.choice;
		cwrite(">", 0, scrindex, FOREGROUND_INTENSITY | (infoboxchoice == 0 ? 7 : 0), 1); // Make a gray arrow
	}
	if(ibnum == 0) // Main (menu) Infobox
	{
		if(!menus.empty()) // If there is a menu to be displayed
		{
			CLIENTMENUSTRUCT *cm = &menus.front();
			int scrindex = HEIGHT - cm->ct.size();

			for(i = 0; i < mainw.maxval; i++)
				cwrite("", 0, scrindex + i, FOREGROUND_INTENSITY, 1);

			if(mainw.maxval <= 0)
				return;

			_ASSERTE(mainw.choice < mainw.maxval && mainw.choice >= 0);

			scrindex += mainw.choice;
			cwrite(">", 0, scrindex, FOREGROUND_INTENSITY | (infoboxchoice == 0 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);

			if(infoboxchoice == 0)
			{
				if(cm->menustyle & MENU_CARDS) // cmenu
				{
					if(cm->ct[mainw.choice].pnum >= 0)
						mainw.curc = &cf.d[cm->ct[mainw.choice].pnum];
					else
						mainw.curc = NULL;
					card(mainw.curc);
				}
			}
		}
	}
	else if(ibnum == 1) // Player Infobox
	{
		int hn, in, an, en, ncards;
		i = playerw.xchoice;
		hn = (i == cf.cnum ? cf.hand.size() : 0); // Include the hand if it's this player
		in = hn + cf.cp[i].item.size();
		an = in + cf.cp[i].ally.size();
		en = an + cf.cp[i].enmy.size();
		ncards = en + cf.cp[i].othr.size();
		// No need for an on

		for(i = 0; i < playerw.size.Y; i++)
			cwrite("", 80 + playerw.xchoice * 4, i, FOREGROUND_INTENSITY, 1);

		if(ncards < 0)
			return;
		else if(ncards == 0) // If there are no cards
		{
			cwrite(">", 80 + playerw.xchoice * 4, 2, FOREGROUND_INTENSITY | (infoboxchoice == 1 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);
			playerw.curc = NULL;
			card(NULL);
			return;
		}

		if(playerw.choice >= playerw.maxval)
			playerw.choice = playerw.maxval - 1;

		i = playerw.choice;
		j = i;
		if(hn != 0 && i >= hn)
			j++;
		if(in != hn && i >= in)
			j++;				// For these, if i changes, then the if statements get messed up
		if(an != in && i >= an)
			j++;
		if(en != an && i >= en)
			j++;
		cwrite(">", 80 + playerw.xchoice * 4, j + 2, FOREGROUND_INTENSITY | (infoboxchoice == 1 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);

		i = playerw.choice;
		if(i >= en)
		{
			i -= en;
			pile = &cf.cp[playerw.xchoice].othr;
		}
		else if(i >= an)
		{
			i -= an;
			pile = &cf.cp[playerw.xchoice].enmy;
		}
		else if(i >= in)
		{
			i -= in;
			pile = &cf.cp[playerw.xchoice].ally;
		}
		else if(i >= hn)
		{
			i -= hn;
			pile = &cf.cp[playerw.xchoice].item;
		}
		else
			pile = &cf.hand;
		playerw.curc = (*pile)[i];
		if(infoboxchoice == 1)
			card(playerw.curc);
	}
	else if(ibnum == 2) // Field Infobox
	{
		int fn, ncards;
		fn = cf.field.size();
		ncards = fn + cf.pranknum;

		for(i = 0; i < fieldw.size.Y; i++)
			cwrite("", 80, fieldw.sm.Top + i, FOREGROUND_INTENSITY, 1);

		if(ncards < 0)
			return;
		else if(ncards == 0)
		{
			cwrite(">", 80, fieldw.sm.Top + 1, FOREGROUND_INTENSITY | (infoboxchoice == 2 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);
			fieldw.curc = NULL;
			card(NULL);
			return;
		}

		if(fieldw.choice >= fieldw.maxval)
			fieldw.choice = fieldw.maxval - 1;

		i = fieldw.choice;
		if(fn != 0 && i >= fn)
			i++;
		cwrite(">", 80, fieldw.sm.Top + i + 1, FOREGROUND_INTENSITY | (infoboxchoice == 2 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);

		i = fieldw.choice;
		pile = &cf.field;
		if(i < fn)
			fieldw.curc = cf.field[i];
		else
		{
			i -= fn;
			fieldw.curc = NULL;
		}
		if(infoboxchoice == 2)
			card(fieldw.curc);
	}
	else if(ibnum == 3)
	{
		char buf[16];
		COORD pos;
		unsigned long linfo;

		if(infoboxchoice == 3 && !cf.discard.empty())
		{
			card(NULL);
			card(cf.discard[discardw.choice], true);
			pos.X = 80 - 38 + 2;
			pos.Y = 16;
			sprintf_s(buf, 16, "(%d/%d)", discardw.choice + 1, discardw.maxval);
			WriteConsoleOutputCharacterA(hScr, buf, strlen(buf), pos, &linfo);
			FillConsoleOutputAttribute(hScr, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY, strlen(buf), pos, &linfo);
		}
		else
			card(NULL, true);
	}
}

void LazyFailC(char* errmsg)
{
	MessageBox(NULL, errmsg, "Error from gameclient's LazyFailC(", MB_ICONERROR);
	exit(1);
}





short swrite(char* str, WORD baseattr, bool highlight, bool menusetup)
{
	int i, j;
	short val;
	COORD dest;
	CHAR_INFO cInfo;
	WORD normattr, selattr;
	int len = strlen(str);
	short numlines = (short)((len - 1) / MAINWIDTH) + 1; // 1-MAINWIDTH -> 1; 43-84 -> 2; etc. (0 -> 1)
	char buf[MAINWIDTH + 1]; //                     The width of the message area is MAINWIDTH characters
	short scrndex;
	short height = HEIGHT;
	SMALL_RECT sm = mainw.sm;
	int sz = mainw.sz;
	static short ypos = 0;

	if(inmenu)
	{
		height = ypos;
		sm.Bottom = height - 1;
		sz -= (HEIGHT - height) * sizeof(WORD);
	}

	if(strcmp(str, "\n") == 0)
	{
		for(i = sindx; i < height; i++)
		{
			cwrite("", 0, i);
		}
		return 0;
	}
	
	if(highlight)
	{
		if(baseattr == 7) // Default (whitish)
		{
			normattr = FOREGROUND_INTENSITY; // Dark gray
			selattr = 7 | FOREGROUND_INTENSITY; // Bright white
		}
		else
		{
			normattr = baseattr & (~FOREGROUND_INTENSITY); // Remove intensity
			selattr = baseattr | FOREGROUND_INTENSITY; // Enforce intensity
		}
	}
	else
	{
		normattr = baseattr;
		selattr = baseattr;
	}

	dest.X = 0;
	dest.Y = sindx - numlines;
	cInfo.Attributes = 0;
	cInfo.Char.AsciiChar = ' ';
	ScrollConsoleScreenBuffer(hScr, &sm, &sm, dest, &cInfo);
	memmove_s(mainw.normAttrib, mainw.sz * sizeof(WORD), mainw.normAttrib + (mainw.size.X * numlines), (sz - (mainw.size.X * numlines)) * sizeof(WORD));
	memmove_s(mainw.selAttrib, mainw.sz * sizeof(WORD), mainw.selAttrib + (mainw.size.X * numlines), (sz - (mainw.size.X * numlines)) * sizeof(WORD));
	scrndex = height - numlines;
	if(menusetup)
		ypos -= numlines;

	val = scrndex;
	for(j = 0; j < numlines; j++)
	{
		int buflen;
		strncpy_s(buf, MAINWIDTH + 1, str + (MAINWIDTH * j), MAINWIDTH);
		buflen = strlen(buf);
		cwrite(buf, 0, scrndex, (infoboxchoice == 0 && highlight == true) ? selattr : normattr);
		for(i = 0; i < buflen; i++)
		{
			mainw.normAttrib[(scrndex - sindx) * mainw.size.X + i] = normattr;
			mainw.selAttrib[(scrndex - sindx) * mainw.size.X + i] = selattr;
		}
		scrndex++;
	}

	if(!menusetup && !inmenu)
		ypos = HEIGHT;

	return val;
}

int smenu(char* msg, short max, short defchoice)
{
	int i;
	char buf[81];
	int force = -1;
	CLIENTMENUSTRUCT *cm = &menus.front();

	swrite(msg, 7, true, true);
	
	for(i = 0; i < max && i < (signed)cm->ct.size(); i++)
	{
		sprintf_s(buf, 81, "  %s", cm->ct[i].name);
		if(cm->ct[i].stat == 0)
			swrite(buf, 7, true, true);
		else if(cm->ct[i].stat == -1)
			swrite(buf, FOREGROUND_RED, true, true);
		else if(cm->ct[i].stat == 1)
		{
			force = (short)i;
			swrite(buf, FOREGROUND_GREEN, true, true);
		}
		else if(cm->ct[i].stat == -2)
			swrite(buf, FOREGROUND_RED | FOREGROUND_GREEN, true, true);
	}

	if(force != -1) // If there is something being forced...
	{
		for(i = 0; i < (signed)cm->ct.size(); i++)
		{
			if(i != force)
				cm->ct[i].stat = -3; // ...make everything else unselectable
		}
	}
	
	inmenu = true;
	mainw.choice = defchoice;
	mainw.maxval = max;
	mainw.xchoice = 0;
	Arrow(0);
	
	return mainw.choice;
}





















void bottomcard()
{
	char buf[16];
	COORD pos;
	unsigned long linfo;

	if(activecard != NULL)
		card(activecard, true, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	else if(!cf.discard.empty())
	{
		card(cf.discard.front(), true, FOREGROUND_INTENSITY);
		pos.X = 80 - 38 + 2;
		pos.Y = 16;
		sprintf_s(buf, 16, "(1/%d)", discardw.maxval);
		WriteConsoleOutputCharacterA(hScr, buf, strlen(buf), pos, &linfo);
		FillConsoleOutputAttribute(hScr, FOREGROUND_GREEN | FOREGROUND_BLUE, strlen(buf), pos, &linfo);
	}
	else
		card(NULL, true);
}

void playerib()
{
	CHAR_INFO* ci; // contains final text block for output
	char*** cd; // contains an array (by player) of each player's text block
	int i, j, k, l;
	int length = 0;
	int maxl = 0;
	int maxw = 0;
	char pad[37] = "                                    ";
	COORD coord = {0, 0};

	cd = new char**[cf.nump];

	for(i = 0; i < cf.nump; i++)
	{
		length = 0;
		if(i == cf.cnum && !cf.hand.empty()) // If it's this player...
			length += cf.hand.size() + 1; // ...include the hand
		if(!cf.cp[i].item.empty())
			length += cf.cp[i].item.size() + 1;
		if(!cf.cp[i].ally.empty())
			length += cf.cp[i].ally.size() + 1;
		if(!cf.cp[i].enmy.empty())
			length += cf.cp[i].enmy.size() + 1;
		if(!cf.cp[i].othr.empty())
			length += cf.cp[i].othr.size() + 1;
		length += 2; // For the borders
		if(length > maxl)
			maxl = length;
	}
	if(maxl == 2) // Empty
		maxl += 2; // To make the "empty" box

	if(playerw.xchoice == cf.cnum)
	{
		for(i = 0; i < (signed)cf.hand.size(); i++)
		{
			length = strlen(cf.hand[i]->name);
			if(length > maxw)
				maxw = length;
		}
	}
	for(i = 0; i < (signed)cf.cp[playerw.xchoice].item.size(); i++)
	{
		length = strlen(cf.cp[playerw.xchoice].item[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)cf.cp[playerw.xchoice].ally.size(); i++)
	{
		length = strlen(cf.cp[playerw.xchoice].ally[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)cf.cp[playerw.xchoice].enmy.size(); i++)
	{
		length = strlen(cf.cp[playerw.xchoice].enmy[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)cf.cp[playerw.xchoice].othr.size(); i++)
	{
		length = strlen(cf.cp[playerw.xchoice].othr[i]->name);
		if(length > maxw)
			maxw = length;
	}

	maxw += 5;
	pad[maxw - 4] = '\0';

	for(i = 0; i < cf.nump; i++)
	{
		cd[i] = new char*[maxl];
		for(j = 0; j < maxl; j++)
		{
			if(i == playerw.xchoice)
			{
				cd[i][j] = new char[maxw + 1];
				cd[i][j][maxw] = '\0';
				_strnset_s(cd[i][j], maxw + 1, ' ', maxw);
			}
			else
			{
				cd[i][j] = new char[5];
				cd[i][j][4] = '\0';
				_strnset_s(cd[i][j], 5, ' ', 4);
			}
		}

		sprintf_s(cd[i][0], 5, " P %d", i + 1);
		strcpy_s(cd[i][1], 5, " �Ŀ");
		if(i == playerw.xchoice)
		{
			strcat_s(cd[i][0], maxw + 1, pad);
			strcat_s(cd[i][1], maxw + 1, pad);
		}
		j = 2;
		if(i == cf.cnum && !cf.hand.empty())
		{
			for(k = 0; k < (signed)cf.hand.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  H ");
				if(i == playerw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", cf.hand[k]->name, maxw - 5 - strlen(cf.hand[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!cf.cp[i].item.empty())
		{
			for(k = 0; k < (signed)cf.cp[i].item.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  I ");
				if(i == playerw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", cf.cp[i].item[k]->name, maxw - 5 - strlen(cf.cp[i].item[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!cf.cp[i].ally.empty())
		{
			for(k = 0; k < (signed)cf.cp[i].ally.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  A ");
				if(i == playerw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", cf.cp[i].ally[k]->name, maxw - 5 - strlen(cf.cp[i].ally[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!cf.cp[i].enmy.empty())
		{
			for(k = 0; k < (signed)cf.cp[i].enmy.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  E ");
				if(i == playerw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", cf.cp[i].enmy[k]->name, maxw - 5 - strlen(cf.cp[i].enmy[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!cf.cp[i].othr.empty())
		{
			for(k = 0; k < (signed)cf.cp[i].othr.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  O ");
				if(i == playerw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", cf.cp[i].othr[k]->name, maxw - 5 - strlen(cf.cp[i].othr[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(j == 2) // Empty
			j += 2;
		strcpy_s(cd[i][--j], 5, " ���");
		if(i == playerw.xchoice)
			strcat_s(cd[i][j], maxw + 1, pad);
	}

	playerw.oldheight = playerw.size.Y;
	playerw.size.X = cf.nump * 4 + (short)maxw - 4;
	playerw.size.Y = (short)maxl;
	playerw.sm.Top = 0;
	playerw.sm.Left = 80;
	playerw.sm.Bottom = (short)maxl;
	playerw.sm.Right = 80 + cf.nump * 4 + (short)maxw - 4;

	l = 0;
	if(playerw.normAttrib != NULL)
		delete [] playerw.normAttrib;
	if(playerw.selAttrib != NULL)
		delete [] playerw.selAttrib;
	playerw.sz = maxl * (cf.nump * 4 + maxw - 4);
	ci = new CHAR_INFO[playerw.sz];
	playerw.normAttrib = new WORD[playerw.sz];
	playerw.selAttrib = new WORD[playerw.sz];
	for(j = 0; j < maxl; j++) // Yes, this is correct; j should be in the outer loop
	{
		for(i = 0; i < cf.nump; i++)
		{
			for(k = 0; k < (i == playerw.xchoice ? maxw : 4); k++)
			{
				ci[l].Char.AsciiChar = cd[i][j][k];
				playerw.normAttrib[l] = FOREGROUND_INTENSITY;
				playerw.selAttrib[l] = 7 | (i == playerw.xchoice ? FOREGROUND_INTENSITY : 0);
				ci[l].Attributes = (infoboxchoice == 1 ? playerw.selAttrib[l] : playerw.normAttrib[l]);
				l++;
			}
		}
	}
	for(i = 0; i < cf.nump; i++)
	{
		for(j = 0; j < maxl; j++)
		{
			delete [] cd[i][j];
		}
		delete [] cd[i];
	}
	delete [] cd;

	for(i = 0; i < maxl; i++)
	{
		cwrite("", 80, i, 0, 52);
	}

	for(i = playerw.oldheight - 1; i >= playerw.size.Y; i--)
	{
		cwrite("", 80, i, 0, 52);
	}
	WriteConsoleOutputA(hScr, ci, playerw.size, coord, &playerw.sm);
	delete [] ci;

	playerw.maxval = (short)((playerw.xchoice == cf.cnum ? cf.hand.size() : 0) + 
						cf.cp[playerw.xchoice].item.size() + 
						cf.cp[playerw.xchoice].ally.size() + 
						cf.cp[playerw.xchoice].enmy.size() + 
						cf.cp[playerw.xchoice].othr.size());
	Arrow(1); // Set the arrow for the player infobox
}

void fieldib()
{
	CHAR_INFO* ci; // contains final text block for output
	int i, j, k;
	int length = 0;
	int maxl = 0;
	int maxw = 0;
	char pad[37] = "                                    ";
	COORD coord = {0, 0};

	maxl = cf.field.size() + cf.pranknum + 3;
	if((cf.field.size() == 0 || cf.pranknum == 0) && !((cf.field.size() == 0 && cf.pranknum == 0)))
		maxl -= 1; // Since we won't need a dividing space (if both are zero, we still need a blank space for an empty box)
	char** cfd = new char*[maxl];
	maxw = 0;
	for(i = 0; i < (signed)cf.field.size(); i++)
	{
		length = strlen(cf.field[i]->name);
		if(length > maxw)
			maxw = length;
	}

	maxw += 5; // For the box itself
	pad[maxw - 4] = '\0';

	for(i = 0; i < maxl; i++)
		cfd[i] = new char[maxw + 1];

	strcpy_s(cfd[0], 5, " �Ŀ");
	strcat_s(cfd[0], maxw + 1, pad);
	strcpy_s(cfd[1], 5, "    ");
	strcat_s(cfd[1], maxw + 1, pad);
	j = 1;
	if(cf.pranknum != 0)
	{
		for(k = 0; k < cf.pranknum; k++)
		{
			strcpy_s(cfd[j], 5, "  P ");
			strncat_s(cfd[j] + 4, maxw - 3, pad, maxw - 4);
			j++;
		}
		strcpy_s(cfd[j], 5, "    ");
		strcat_s(cfd[j], maxw + 1, pad);
		j++;
	}
	if(!cf.field.empty())
	{
		for(k = 0; k < (signed)cf.field.size(); k++)
		{
			strcpy_s(cfd[j], 5, "  F ");
			sprintf_s(cfd[j] + 4, maxw - 3, " %s%.*s", cf.field[k]->name, maxw - 5 - strlen(cf.field[k]->name), pad);
			j++;
		}
		strcpy_s(cfd[j], 5, "    ");
		strcat_s(cfd[j], maxw + 1, pad);
		j++;
	}
	if(j == 1) // Empty
		j += 2;
	strcpy_s(cfd[--j], 5, " ���");
	strcat_s(cfd[j], maxw + 1, pad);

	fieldw.oldheight = fieldw.size.Y;
	fieldw.size.X = (short)maxw;
	fieldw.size.Y = (short)maxl;
	fieldw.sm.Top = HEIGHT - (short)maxl;
	fieldw.sm.Left = 80;
	fieldw.sm.Bottom = HEIGHT - 1;
	fieldw.sm.Right = 80 + (short)maxw;

	k = 0;
	if(fieldw.normAttrib != NULL)
		delete [] fieldw.normAttrib;
	if(fieldw.selAttrib != NULL)
		delete [] fieldw.selAttrib;
	fieldw.sz = maxl * maxw;
	ci = new CHAR_INFO[fieldw.sz];
	fieldw.normAttrib = new WORD[fieldw.sz];
	fieldw.selAttrib = new WORD[fieldw.sz];
	for(i = 0; i < maxl; i++)
	{
		for(j = 0; j < maxw; j++)
		{
			ci[k].Char.AsciiChar = cfd[i][j];
			fieldw.normAttrib[k] = FOREGROUND_INTENSITY;
			fieldw.selAttrib[k] = 7 | FOREGROUND_INTENSITY;
			ci[k].Attributes = (infoboxchoice == 2 ? fieldw.selAttrib[k] : fieldw.normAttrib[k]);
			k++;
		}
	}
	delete [] cfd;

	for(i = (HEIGHT - maxl); i < HEIGHT; i++)
	{
		cwrite("", 80, i, 0, 52);
	}

	for(i = HEIGHT - playerw.oldheight; i < HEIGHT - playerw.size.Y; i++)
	{
		cwrite("", 80, i, 0, 52);
	}
	WriteConsoleOutputA(hScr, ci, fieldw.size, coord, &fieldw.sm);
	delete [] ci;

	fieldw.maxval = (short)cf.field.size() + cf.pranknum;
	Arrow(2); // Set the arrow for the field infobox

	if(infoboxchoice == 3)
	{
		discardw.choice = 0;
		Arrow(3); // Takes care of calling card(
	}
	else
		bottomcard();
}
