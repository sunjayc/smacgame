#include "CardCode.h"

int	NewCard(deque<CARD> *d, char* name, char* desc, char* flavtxt, unsigned short aff, unsigned long type, unsigned short id, unsigned short atype, short health, DPROC draw, FPROC func, char* custom_msg)
{
	CARD c;

	strcpy_s(c.name, 35, name);
	strcpy_s(c.desc, 129, desc);
	strcpy_s(c.flavtxt, 65, flavtxt);
	c.aff = aff;
	c.type = type;
	c.id = id;
	c.atype = atype;
	c.health = health;

	c.draw = draw;
	c.func = func;
	c.custom_msg = custom_msg;

	c.ep = NULL;

	d->push_back(c);

	return 0;
}

MAKE_HPROC(NegativeOne)
{
	UNREFERENCED_PARAMETER(f);
	UNREFERENCED_PARAMETER(e);
	UNREFERENCED_PARAMETER(card);
	UNREFERENCED_PARAMETER(ocard);
	UNREFERENCED_PARAMETER(pnum);
	UNREFERENCED_PARAMETER(data);
	return -1;
}

MAKE_HPROC(ForceNotMe)
{
	UNREFERENCED_PARAMETER(f);
	UNREFERENCED_PARAMETER(e);
	UNREFERENCED_PARAMETER(card);
	UNREFERENCED_PARAMETER(ocard);
	if (*(short*)data == pnum) // Don't force player to target themselves
		return 0;
	return 1;
}

EVENT* AffEvent(PLAYFIELD *f, CARD *card, short to, short S, short M, short A, short C, short I)
{
	int i;
	short hit = 0;

	for(i = 0; i < (signed)f->p[to]->hand.size(); i++)
	{
		if(f->p[to]->hand[i]->aff & AFF_S)
			hit += S;
		if(f->p[to]->hand[i]->aff & AFF_M)
			hit += M;
		if(f->p[to]->hand[i]->aff & AFF_A)
			hit += A;
		if(f->p[to]->hand[i]->aff & AFF_C)
			hit += C;
		if(f->p[to]->hand[i]->aff & AFF_I)
			hit += I;
	}
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	etmp->hit = hit;

	return etmp;
}

void RegisterHookEvent(EVENT *etmp, short target, CARD *targetcard, HOOK hook, unsigned long specific, short priority, HPROC hfunc)
{
	_ASSERTE(hook != HOOK_SUCC); // HOOK_SUCC should be registered directly into the card
	EVDONE *ereg = new EVDONE;
	ereg->edonetype = evdone_reg;
	ereg->target = target;
	ereg->targetcard = targetcard;
	ereg->hook = hook;
	ereg->lval = specific;
	ereg->sval = priority;
	ereg->hfunc = hfunc;
	etmp->evdone.push_back(ereg);
}
