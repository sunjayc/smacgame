#include <deque>
#include <map>
using namespace std;

// Hooks
enum HOOK
{
	HOOK_TOP,			// Beginning of turn
	HOOK_BOTTOM,		// End of turn
	HOOK_SELECT,		// On player selecting a card
	HOOK_TARGET1,		// On player being targeted by another player: Limit targets
	HOOK_TARGET2,		// On player being targeted by another player: Force targets (register on player that receives the targeting; data field should be short* with value &from)
	HOOK_PLAY1,			// On player having played a card upon: another player
	HOOK_PLAY2,			// On player having played a card upon: this player
	HOOK_DAMAGE0,		// Just before damage is calculated for player (to nullify damage etc.)
	HOOK_DAMAGE1,		// On player having done damage to: this player
	HOOK_DAMAGE2,		// On player having done damage to: another player
	HOOK_CARD,			// On a specific type of card being played (only register for field)
	HOOK_DEATH,			// On death
	HOOK_SUCC,			// On card getting it's succ event run
	HOOK_DISCARD,		// When a card is about to be discarded
	HOOK_TARGET0,		// On player targeting another player: Add targets
	HOOK_SYSMESSAGE,	// Just before sending player a gameplay message (data field needs to be char**: address of message string)
};

// So that these can be easily changed if need be
#define PRIORITY_FIELD			1
#define PRIORITY_ITEM			2
#define PRIORITY_SPECIAL		3
#define PRIORITY_NORMAL			4

typedef int (__cdecl *HPROC)(struct PLAYFIELD *, class ELIST *, struct CARD *, struct CARD *, short, void*);

/**
 * Structure representing a single registered hook.
 * Contains information on how and when to be invoked.
 */
struct HITEM
{
	HITEM();

	struct CARD *card; ///< Card bound to this hook
	short priority; ///< 1 = field, 2 = item, 3 = special, 4 = normal, 5+ = etc. (0 = ZOMG[NOT]IMPORTANT)
	HPROC hfunc; ///< Pointer to list of actions to take
	unsigned long specific; ///< HK_*
};

class HLIST
{
public:
	int registerItem(struct CARD *card, HOOK hook, unsigned long specific, short priority, HPROC hfunc);
	int callFunc(struct PLAYFIELD *f, ELIST *e, HOOK hook, int index, struct CARD *card, short pnum, void* data);
	unsigned long getSpec(HOOK hook, int index);
	int getNum(HOOK hook, short priority);
	int getNextIndex(HOOK hook, short priority, int index);
	int getMaxPriority(HOOK hook);

	void removeItems(CARD *card, HOOK hook);
	void clearCard(CARD *card);
	void clearList();

private:
	HITEM* findItem(HOOK hook, int index);
	int addItem(HOOK hook, HITEM *hitem);
	int deleteItem(HOOK hook, int index);

	map<HOOK, deque<HITEM*>> hlist;
};