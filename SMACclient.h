#include "SMACgame.h"

/**
 * Structure for client side cards.
 */
struct CCARD
{
	char name[35]; ///< Name of card
	char desc[133]; ///< Description
	char flavtxt[67]; ///< Flavor text
	unsigned short aff; ///< SMAC! affiliation
	unsigned long type; ///< Card type(s)
	unsigned short id; ///< Card Identifier
	unsigned short atype; ///< Attack/Counter type
	short health; ///< Damage done by attack/counter OR Health healed by heal OR Starting health of enemies/allies
};

/**
 * Client side player data holder for players.
 * Note: Own player's hand is included in CPLAY
 */
struct CPLAYER
{
	char name[32]; ///< Player's name

	short health; ///< Current health

	deque<CCARD*> item; ///< Items in play

	deque<CCARD*> ally; ///< Allies attached to player
	deque<CCARD*> enmy; ///< Enemies attached to player
	deque<CCARD*> othr; ///< Anything else attached to player
};


struct CPLAY // Client side main playing field and player data
{
	deque<CCARD> d; // "Master" deck
	deque<CCARD*> discard; // Cards in the discard pile

	short nump; // Number of players
	short curp; // Current player
	short cnum; // Index of this player
	CPLAYER cp[5]; // Array of basic ("see-able") information on players

	deque<CCARD*> field; // Current field card(s)
	short pranknum; // Number of pranks total on field by all players

	deque<CCARD*> hand; // This player's hand

	SOCKET psock; // Socket to game host
};
