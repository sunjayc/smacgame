#include "SMAChost.h"
#include "SMAChelpers.h"

#define cv (*e)[e->curev]

/**
 * Creates a new ELIST.
 * curev defaults to -1
 */
ELIST::ELIST()
{
	curev = -1;
}

/**
 * Clears the event list.
 * curev is reset to -1
 */
void ELIST::clear()
{
	int i, j;
	EVENT *etmp;
	for(i = 0; i < (signed)this->size(); i++)
	{
		etmp = this->at(i);
		if(etmp->evmod.size() > 0)
		{
			for(j = 0; j < (signed)etmp->evmod.size(); j++)
			{
				delete etmp->evmod[j];
			}
			etmp->evmod.clear();
		}
		if(etmp->evdone.size() > 0)
		{
			for(j = 0; j < (signed)etmp->evdone.size(); j++)
			{
				delete etmp->evdone[j];
			}
			etmp->evdone.clear();
		}
		delete etmp;
	}
	deque::clear();
	curev = -1;
}

/**
 * Creates a new EVENT.
 */
EVENT::EVENT()
{
	card = NULL;
	from = 0;
	to = 0;
	type = 0;
	atype = 0;
	aff = 0;
	hit = 0;
	msg[0] = '\0';
	nulled = false;
	isprocessed = false;
	ischained = false;
	tproc = NULL;
	hcard = NULL;
}

/**
 * Sets a message to be displayed when the event is processed.
 * Special strings:<br/>
 * %%THISP: Name of the player that owns the #card associated with the event (CARD#thisp).<br/>
 * %%TO: Name of the player indexed by the #to field.<br/>
 * %%HIT: Amount of damage/heal this event will do, as specified by #hit.<br/>
 * 
 * @param f the current playing field.
 * @param msg the string to parse for the message.
 */
void EVENT::setmsg(PLAYFIELD *f, char* msg)
{
	char* res;
	char tmp[256];
	int len;

	strcpy_s(this->msg, msg);

	res = strstr(this->msg, "%THISP");
	while(res != NULL)
	{
		len = (int)(res - this->msg);
		strncpy_s(tmp, this->msg, len);
		if(card->thisp != -1)
			strcat_s(tmp, f->p[card->thisp]->name);
		strcat_s(tmp, &res[6]);
		strcpy_s(this->msg, tmp);
		res = strstr(this->msg, "%THISP");
	}

	res = strstr(this->msg, "%TO");
	while(res != NULL)
	{
		len = (int)(res - this->msg);
		strncpy_s(tmp, this->msg, len);
		if(ISREAL(to))
			strcat_s(tmp, f->p[to]->name);
		else
			strcat_s(tmp, f->ep[PINDX(to)]->name);
		strcat_s(tmp, &res[3]);
		strcpy_s(this->msg, tmp);
		res = strstr(this->msg, "%TO");
	}

	res = strstr(this->msg, "%HIT");
	while(res != NULL)
	{
		char ttmp[256];
		len = (int)(res - this->msg);
		strncpy_s(tmp, this->msg, len);
		sprintf_s(ttmp, "%d%s", hit, &res[4]);
		strcat_s(tmp, ttmp);
		strcpy_s(this->msg, tmp);
		res = strstr(this->msg, "%HIT");
	}
}

/**
 * Creates a new BPLAYER.
 */
BPLAYER::BPLAYER()
{
	name[0] = '\0';
	pnum = -1;
	health = 0;
	ptype = bplayer;
}

/**
 * Creates a new PLAYER.
 */
PLAYER::PLAYER() : BPLAYER()
{
	ptype = player;
	hmax = 0;
	imax = 0;
}

bool PLAYER::damage(PLAYFIELD *f, ELIST *e, short hit)
{
	health -= hit;
	CheckHook(f, e, HOOK_DAMAGE1, cv->card, cv->to, true);
	if(health <= 0)
		return (CheckHook(f, e, HOOK_DEATH, NULL, cv->to, true) == 0); // If this == 0, then they actually died. If not, then they survived somehow.
	return false;
}

bool PLAYER::heal(PLAYFIELD *f, ELIST *e, short hit)
{
	UNREFERENCED_PARAMETER(f);
	UNREFERENCED_PARAMETER(e);
	health += hit;
	return false;
}

/**
 * Creates a new EPLAYER.
 */
EPLAYER::EPLAYER() : BPLAYER()
{
	ptype = eplayer;
	card = NULL;
}

bool EPLAYER::damage(PLAYFIELD *f, ELIST *e, short hit)
{
	health -= hit;
	CheckHook(f, e, HOOK_DAMAGE1, cv->card, cv->to, true);
	if(health <= 0 && !(this->params & EPLAY_EMO))
	{
		health = 0;
		SendEPlayerHealth(f, this);
		RemoveEvents(e, this->card);
		DiscardCard(f, e, this->card);
		return true;
	}
	else if(health >= 0 && this->params & EPLAY_EMO)
	{
		health = 0;
		SendEPlayerHealth(f, this);
		RemoveEvents(e, this->card);
		DiscardCard(f, e, this->card);
		return true;
	}
	SendEPlayerHealth(f, this);
	return false;
}

bool EPLAYER::heal(PLAYFIELD *f, ELIST *e, short hit)
{
	health += hit;
	if(health >= 0 && this->params & EPLAY_EMO)
	{
		health = 0;
		SendEPlayerHealth(f, this);
		RemoveEvents(e, this->card);
		DiscardCard(f, e, this->card);
		return true;
	}
	SendEPlayerHealth(f, this);
	return false;
}
