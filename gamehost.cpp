#define _CRT_RAND_S // Needed for rand_s (must be defined before #including stdlib.h)
#include <stdlib.h>
#include <conio.h>
#include <algorithm>
#include <list>
#include "SMAChostdebug.h"
#include "SMAChelpers.h"

extern int lzf;
extern HANDLE hScr;
extern int hostdebug(PLAYFIELD *f, ELIST *e);
extern unsigned long hostswrite(char* str, WORD attr);

// Globals
bool dbgmodeh;
short dbgtarget;

// Ease-of-access #defines
#define cp f->p[f->curp]
#define cv (*e)[e->curev]

#define GETV_S(x) (x & UVAR_S ? (short)card->uservar[x ^ UVAR_S] : x)
#define GETU_S(x) ((short)card->uservar[x & (!UVAR_S)])
#define SETU_S(x) (card->uservar[x & (!UVAR_S)])
#define GETV_L(x) (x & UVAR_L ? (long)card->uservar[x ^ UVAR_L] : x)
#define GETU_L(x) ((long)card->uservar[x & (!UVAR_L)])

// Functions
int		InitGame(PLAYFIELD *f);
int		InitDeck(PLAYFIELD *f, ELIST *e);
int		GameLoopH(PLAYFIELD *f, ELIST *e);
int		ExitGame(PLAYFIELD *f);

short	Handler(PLAYFIELD *f, ELIST *e, short *pnum);

int		SendInfo(PLAYFIELD *f, short pindx, INFO linfo, va_list arglist);
int		SendMsg(PLAYFIELD *f, EVENT *ev);

void	AddCard(PLAYFIELD *f, CARD* card, short pindx, unsigned long loc);
CARD*	GetCard(PLAYFIELD *f, short pindx, unsigned long loc);
void	RemoveCard(PLAYFIELD *f, CARD *card);
void	ShuffleDeck(PLAYFIELD *f);

int		RegisterEPlayer(PLAYFIELD *f, short pindx, CARD *card, char* name, short health, short params);

int		ProcessEvents(PLAYFIELD *f, ELIST *e);
int		ProcessEventsHelperSelect(PLAYFIELD *f, ELIST *e, CARD *card, long target);
int		ProcessEventsHelper(PLAYFIELD *f, ELIST *e, CARD *card, long target);
int		CardSucc(PLAYFIELD *f, ELIST *e); // Acts only on current event

void	SelectNoBlock(PLAYFIELD *f, ELIST *e, short thisp, deque<TARGET> *t, INFO linfo, char* msg, CARD *card);
void	SelectCallback(PLAYFIELD *f, ELIST *e, CARD *card, FPROC tproc, short thisp, bool counters); // counters = true if selecting for counter

bool	CheckSpecific(unsigned long check, unsigned short aff, unsigned long type, unsigned short atype, bool counters);

void	LazyFailH(char* errmsg);

int gameserv()
{
	lzf = 0;
	dbgmodeh = false;
	PLAYFIELD f;
	ELIST e;

	if(InitGame(&f) != 0)
		return 1;
	if(InitDeck(&f, &e) != 0)
	{
		ExitGame(&f);
		return 1;
	}
	ProcessEvents(&f, &e);

	return GameLoopH(&f, &e);
}

int InitGame(PLAYFIELD *f)
{
	char buf[81];
	int i, j;
	int c;
	int res;
	PLAYER *p;
	SOCKET psock, mysock;
	SOCKADDR_IN paddr;

	cwrite("Starting health?", 0, 0);
	f->shealth = (short)iget(1);

	cwrite("Amount of cards in each hand?", 0, 3);
	f->shmax = (short)iget(4);

	cwrite("Amount of items allowed?", 0, 6);
	f->simax = (short)iget(7);

	cwrite("Initializing game...", 0, 9);
	if(InitSock() != 0) // Initialize sockets, sets sets blocking on by default
		return 1;
	if(InitServ(&mysock) != 0) // Initialize server side code; handles its own errors
		return 1;
	
	f->nump = 0;
	gethostname(buf, 81);
	cwrite("Host name: ", 0, 9);
	cwrite(buf, 11, 9); // Write the hostname to the screen so clients know who to connect to
	cwrite("Waiting for players... Press 'N' when all players are in", 0, 11);

	j = 0; // Number of pending connections
	for(;;)
	{
		res = GetClient(&mysock, &psock, &paddr, false); // Will not block; handles its own errors
		if(res == -1) // No new connections
		{
			if(_kbhit() != 0) // Check for keystroke
			{
				c = tolower(kget());
				if(c == 'd')
				{
					dbgmodeh = true;
					c = 'n';
				}
				if(c == 'n') // Stop listening for connections
				{
					closesocket(mysock); // Close the listening socket; no more connections can be made
					deque<PLAYER*>::const_iterator iter = f->p.begin();
					while(iter != f->p.end())
					{
						if((*iter)->name[0] == '\0')
						{
							closesocket((*iter)->psock); // Close any pending sockets
							iter = f->p.erase(iter); // And delete those players
						}
						else
							iter++;
					}
					break;
				}
			}
			for(i = 0; i < j; i++) // For each existing connection
			{
				if(f->p[i]->name[0] == '\0') // If the connection is pending
				{
					if(SockBlock(1, 0, false, &f->p[i]->psock) > 0) // If the socket can be read
					{
						LazyRecv(&f->p[i]->psock, f->p[i]->name, 35); // Get the player's name
						sprintf_s(buf, 81, "\"%s\" (%s) joined the game!", 
							f->p[i]->name, inet_ntoa(f->p[i]->paddr.sin_addr));
						cwrite(buf, 0, 13 + (f->nump * 2)); // Print name to screen
						f->nump++;
					}
				}
			}
			continue;
		}

		sprintf_s(buf, 81, "Player %d (%s) is connecting...", j + 1, inet_ntoa(paddr.sin_addr));
		cwrite(buf, 0, 13 + (j * 2));
		p = new PLAYER;
		p->psock = psock;
		p->paddr = paddr;
		f->p.push_back(p);
		j++; // Increment pending connections
	}

	if(f->nump == 0)
	{
		return 0;
	}

	// Initialize each player with starting stats
	for(i = 0; i < f->nump; i++)
	{
		f->p[i]->pnum = (short)i;
		f->p[i]->health = f->shealth;
		f->p[i]->hmax = f->shmax; // Number of max cards is set to starting max cards
		f->p[i]->imax = f->simax; // Max number of items is set to starting max items
	}

	for(i = 0; i < f->nump; i++) // For each player...
	{
		LazySend(&f->p[i]->psock, &f->nump, 2, "I"); // ...send info for that player
		LazySend(&f->p[i]->psock, &f->p[i]->pnum, 2, "I");
		SendInfo(f, (short)i, INFO_NAME);
		SendInfo(f, (short)i, INFO_HLTH);
	}
	f->curp = 0; // Player 1 starts

	return 0;
}

int InitDbgH(int nump)
{
	int i, j;
	int res;
	PLAYFIELD f;
	ELIST e;
	PLAYER *p;
	SOCKET mysock;

	lzf = 0;
	dbgmodeh = true;
	dbgtarget = -1;

	f.shealth = 15;
	f.shmax = 3;
	f.simax = 1;

	if(InitSock() != 0) // Initialize sockets, sets blocking on by default
		return 1;
	if(InitServ(&mysock) != 0) // Initialize server side code; handles its own errors
		return 1;
	
	j = 0;
	f.nump = (short)nump;
	if(nump == 0)
	{
		p = new PLAYER;
		GetClient(&mysock, &p->psock, &p->paddr, true); // Will block; handles its own errors
		LazyRecv(&p->psock, &f.nump, sizeof(f.nump));
		strcpy_s(p->name, 35, "Player 1");
		p->pnum = 0;
		p->health = f.shealth;
		p->hmax = f.shmax; // Number of max cards is set to starting max cards
		p->imax = f.simax; // Max number of items is set to starting max items
		j = 1;
		f.p.push_back(p);
	}

	for(i = j; i < f.nump; i++) // i will start at 1 if nump == 0 to skip the first player since we already got them
	{
		p = new PLAYER;
		GetClient(&mysock, &p->psock, &p->paddr, true); // Will block; handles its own errors
		sprintf_s(p->name, 35, "Player %d", i + 1);
		p->pnum = (short)i;
		p->health = f.shealth;
		p->hmax = f.shmax; // Number of max cards is set to starting max cards
		p->imax = f.simax; // Max number of items is set to starting max items
		f.p.push_back(p);
	}
	closesocket(mysock);

	for(i = 0; i < f.nump; i++) // For each player...
	{
		LazySend(&f.p[i]->psock, &f.nump, 2, "I"); // ...send info for that player
		LazySend(&f.p[i]->psock, &f.p[i]->pnum, 2, "I");
		SendInfo(&f, (short)i, INFO_NAME);
		SendInfo(&f, (short)i, INFO_HLTH);
	}
	f.curp = 0; // Player 1 starts

	res = InitDeck(&f, &e);
	if(res != 0)
	{
		return res;
	}
	ProcessEvents(&f, &e);

	return GameLoopH(&f, &e);
}

int InitDeck(PLAYFIELD *f, ELIST *e)
{
	int i, j;

	InitCards(&f->d);

	for(j = 0; j < (signed)f->d.size(); j++)
	{
		if(f->d[j].draw == NULL) // If card does not have a func...
			f->d[j].draw = &AutomagicDraw; // ...create one for it
		if(f->d[j].func == NULL) // If card does not have a func...
			f->d[j].func = &AutomagicFunc; // ...create one for it
		f->d[j].indx = (short)j;
		f->d[j].thisp = -1;
		f->d[j].loc = 0;
	}

	if(f->nump == 0)
	{
//		hostcmenu(f); // Card menu with all cards for debug purposes
		return 1; // So that the program closes
	}

	short dnum = (short)f->d.size();
	for(i = 0; i < f->nump; i++) // For each player
	{
		SendInfo(f, (short)i, INFO_DECK);
		LazySend(&f->p[i]->psock, &dnum, 2, "I");
		for(j = 0; j < dnum; j++) // For each card in the master deck
		{
			LazySend(&f->p[i]->psock, f->d[j].name, 0, "S");
			LazySend(&f->p[i]->psock, f->d[j].desc, 0, "S");
			LazySend(&f->p[i]->psock, f->d[j].flavtxt, 0, "S");
			LazySend(&f->p[i]->psock, &f->d[j].aff, 2, "I");
			LazySend(&f->p[i]->psock, &f->d[j].type, 4, "I");
			LazySend(&f->p[i]->psock, &f->d[j].id, 2, "I");
			LazySend(&f->p[i]->psock, &f->d[j].atype, 2, "I");
			LazySend(&f->p[i]->psock, &f->d[j].health, 2, "I");
		}
	}

	ShuffleDeck(f); // Create the draw pile by randomly mixing the master deck
	if(dbgmodeh)
		hostdebug(f, e);

	for(i = 0; i < f->nump; i++)
	{
		if(!dbgmodeh)
			DrawHand(f, e, (short)i); // Draw cards from the draw pile
		SendInfo(f, (short)i, INFO_HAND);
		SendInfo(f, (short)i, INFO_FFLD);
	}

	ProcessEvents(f, e);

	return 0;
}

int GameLoopH(PLAYFIELD *f, ELIST *e)
{
	int i;
	char buf[128];

	f->curp = 0;
	for(;;) // Main game loop
	{
		e->clear();

		sprintf_s(buf, 128, "%s's turn!", cp->name);
		SendInfo(f, -1, INFO_MMSG, buf, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		if(CheckHook(f, e, HOOK_TOP, NULL, f->curp, true) > 0) // HOOK_TOP: Beginning of turn
		{
			f->curp++;
			if(f->curp == f->nump)
				f->curp = 0;
			continue; //  if any hooks return >0, end the turn immediately
		}

		SelectEvent(e, f->curp); // Enter an event for curp to select a card

		ProcessEvents(f, e);

		CheckHook(f, e, HOOK_BOTTOM, NULL, f->curp, true); // HOOK_BOTTOM: End of turn
		ProcessEvents(f, e); // For any events made during HOOK_BOTTOM
		for(i = (signed)f->flux.size() - 1; i >= 0; i--)
		{
			DiscardCard(f, e, f->flux[i]);
		}

		for(i = 0; i < f->nump; i++)
		{
			if(f->p[i]->health <= 0)
			{
				ExitGame(f);
				return 0;
			}
		}

		SendInfo(f, -1, INFO_MMSG, "", 0);

		f->curp++;
		if(f->curp == f->nump)
			f->curp = 0;
	}
//	Unreachable
}

int ExitGame(PLAYFIELD *f)
{
	int i;

	SendInfo(f, -1, INFO_QUIT);
	for(i = 0; i < f->nump; i++) // For each player
	{
		closesocket(f->p[i]->psock); // Close the socket to that player
	}
	for(i = 0; i < (signed)f->p.size(); i++)
	{
		delete f->p[i];
	}
	f->p.empty();

	WSACleanup(); // Clean up WinSock

	return 0;
}




short Handler(PLAYFIELD *f, ELIST *e, short *pnum)
{
	SOCKET *psock;
	int i;
	int nErr;
	short choice, endx;

	for(i = 0; i < f->nump; i++)
	{
		psock = &f->p[i]->psock;
		nErr = LazyRecv(psock, &endx, 2, true);
		if(nErr == 0) // Something was retrieved (if nothing was retrived, nErr == -1)
		{
			e->curev = endx;
			cv->isprocessed = true;
			LazyRecv(psock, &choice, 2);
			if(cv->tproc != NULL)
			{
				cv->tproc(f, e, cv->hcard, choice);
				DrawHand(f, e, f->curp); // Refill the hand after playing
			}
			*pnum = (short)i;
			return choice;
		}
	}

	return -2;
}





int SendInfo(PLAYFIELD *f, short pindx, INFO linfo, ...)
{
	va_list arglist;
	int val;
	short i;

	va_start(arglist, linfo);
	if(pindx == -1)
	{
		val = 0;
		for(i = 0; i < f->nump; i++)
		{
			val += SendInfo(f, i, linfo, arglist);
		}
		if(linfo == INFO_MMSG)
		{
			char* buf = va_arg(arglist, char*);
			WORD attr = va_arg(arglist, WORD);
			if(dbgmodeh)
				hostswrite(buf, attr);
		}
	}
	else
		val = SendInfo(f, pindx, linfo, arglist);

	va_end(arglist);

	return val;
}

int SendInfo(PLAYFIELD *f, short pindx, INFO linfo, va_list arglist)
{
	char* buf;
	int i;
	SOCKET *psock = &f->p[pindx]->psock;
	WORD attr;
	long info = (long)linfo;

	LazySend(psock, &info, 4, "I"); // Send the info code
#ifdef _DEBUG
switch(linfo)
{
case INFO_NAME:
	OutputDebugStringA("INFO_NAME");
	break;
case INFO_HLTH:
	OutputDebugStringA("INFO_HLTH");
	break;
case INFO_HAND:
	OutputDebugStringA("INFO_HAND");
	break;
case INFO_CARD:
	OutputDebugStringA("INFO_CARD");
	break;
case INFO_XSCR:
	OutputDebugStringA("INFO_XSCR");
	break;
case INFO_CLRS:
	OutputDebugStringA("INFO_CLRS");
	break;
case INFO_CNTR:
	OutputDebugStringA("INFO_CNTR");
	break;
case INFO_TRGT:
	OutputDebugStringA("INFO_TRGT");
	break;
case INFO_DECK:
	OutputDebugStringA("INFO_DECK");
	break;
case INFO_FFLD:
	OutputDebugStringA("INFO_FFLD");
	break;
case INFO_MMSG:
	OutputDebugStringA("INFO_MMSG");
	break;
case INFO_EPLR:
	OutputDebugStringA("INFO_EPLR");
	break;
case INFO_MENU:
	OutputDebugStringA("INFO_MENU");
	break;
case INFO_MSEL:
	OutputDebugStringA("INFO_MSEL");
	break;
}
OutputDebugStringA("\n");
#endif // _DEBUG

	// Extra code to handle codes with parameters or codes with other extra stuff
	switch(linfo)
	{
		case INFO_NAME:
		{
			for(i = 0; i < f->nump; i++)
			{
				LazySend(psock, &f->p[i]->name, 0, "S");
			}
			break;
		}
		case INFO_HLTH:
		{
			for(i = 0; i < f->nump; i++)
			{
				LazySend(psock, &f->p[i]->health, 2, "I");
			}
			break;
		}
		case INFO_HAND:
		{
			short hnum = (short)f->p[pindx]->hand.size();
			LazySend(&f->p[pindx]->psock, &hnum, 2, "I");
			for(i = 0; i < hnum; i++) // For each card in each player's hand
			{
				LazySend(&f->p[pindx]->psock, &f->p[pindx]->hand[i]->indx, 2, "I");
			}
			break;
		}
		case INFO_MMSG:
		{
			buf = va_arg(arglist, char*);
			attr = va_arg(arglist, WORD);
			unsigned int len = strlen(buf);
			char* tmpbuf = new char[len + 1];
			strcpy_s(tmpbuf, len + 1, buf); // Make a backup copy in case HOOK_SYSMESSAGE edits the message
			char* tmpptr = tmpbuf; // Make a backup pointer in case HOOK_SYSMESSAGE changes where the variable points
			CheckHook(f, NULL, HOOK_SYSMESSAGE, NULL, pindx, true, false, &tmpptr);
			LazySend(psock, tmpptr, strlen(tmpptr), "S"); // Need to pack to make sure \n is sent nicely
			LazySend(psock, &attr, 2, "I");
			delete [] tmpbuf; // NOT tmpptr, since that might be different!
			break;
		}
		case INFO_FFLD:
		{
			deque<CARD*> pile;
			short num = 0;

			pile.assign(f->discard.begin(), f->discard.end());
			pile.insert(pile.end(), f->field.begin(), f->field.end());
			pile.insert(pile.end(), f->prank.begin(), f->prank.end());
			for(i = 0; i < f->nump; i++)
			{
				pile.insert(pile.end(), f->p[i]->item.begin(), f->p[i]->item.end());
				pile.insert(pile.end(), f->p[i]->ally.begin(), f->p[i]->ally.end());
				pile.insert(pile.end(), f->p[i]->enmy.begin(), f->p[i]->enmy.end());
				pile.insert(pile.end(), f->p[i]->othr.begin(), f->p[i]->othr.end());
			}

			num = (short)pile.size();
			LazySend(psock, &num, 2, "I");
			for(i = 0; i < num; i++)
			{
				LazySend(psock, &pile[i]->indx, 2, "I");
				LazySend(psock, &pile[i]->thisp, 2, "I");
				LazySend(psock, &pile[i]->loc, 4, "I");
			}
			break;
		}
		case INFO_EPLR:
		{
			short indx = va_arg(arglist, short);
			short health = va_arg(arglist, short);
			LazySend(psock, &indx, 2, "I");
			LazySend(psock, &health, 2, "I");
			break;
		}
		case INFO_CLRS:
			break;
		case INFO_CARD:
		case INFO_CNTR:
		{
			short tnum, endx;
			endx = va_arg(arglist, short);
			LazySend(psock, &endx, 2, "I");
			deque<TARGET> *t = va_arg(arglist, deque<TARGET>*);
			buf = va_arg(arglist, char*);
			LazySend(psock, buf, strlen(buf), "S");
			CARD *c = va_arg(arglist, CARD*);
			if(c != NULL)
				LazySend(psock, &c->indx, 2, "I");
			else
			{
				tnum = -1;
				LazySend(psock, &tnum, 2, "I");
			}
			tnum = (short)t->size();
			LazySend(psock, &tnum, 2, "I"); // Send number of cards/targets
			for(i = 0; i < tnum; i++)
			{
				LazySend(psock, &t->at(i).pnum, 2, "I"); // Send card index
				LazySend(psock, &t->at(i).stat, 2, "I"); // Send targetable status
			}
			break;
		}
		case INFO_MSEL:
		{
			short tnum, endx;
			endx = va_arg(arglist, short);
			LazySend(psock, &endx, 2, "I");
			deque<TARGET> *t = va_arg(arglist, deque<TARGET>*);
			buf = va_arg(arglist, char*);
			LazySend(psock, buf, strlen(buf), "S");
			tnum = (short)t->size();
			LazySend(psock, &tnum, 2, "I"); // Send number of cards/targets
			for(i = 0; i < tnum; i++)
			{
				LazySend(psock, &t->at(i).pnum, 2, "I"); // Send card index
			}
			break;
		}
		case INFO_TRGT:
		{
			short tnum, endx;
			endx = va_arg(arglist, short);
			LazySend(psock, &endx, 2, "I");
			deque<TARGET> *t = va_arg(arglist, deque<TARGET>*);
			buf = va_arg(arglist, char*);
			LazySend(psock, buf, strlen(buf), "S");
			CARD *c = va_arg(arglist, CARD*);
			if(c != NULL)
				LazySend(psock, &c->indx, 2, "I");
			else
			{
				tnum = -1;
				LazySend(psock, &tnum, 2, "I");
			}
			tnum = (short)t->size();
			LazySend(psock, &tnum, 2, "I"); // Send number of targets
			for(i = 0; i < tnum; i++)
			{
				LazySend(psock, t->at(i).name, strlen(t->at(i).name), "S"); // Send player name
				LazySend(psock, &t->at(i).pnum, 2, "I"); // Send player index
				LazySend(psock, &t->at(i).stat, 2, "I"); // Send targetable status
			}
			break;
		}
		case INFO_QUIT:
			break;
		default:
			break;
	}

	return 0;
}

int SendMsg(PLAYFIELD *f, EVENT *ev)
{
	if(ev->msg[0] != '\0')
	{
		char *token, *context;
		token = strtok_s(ev->msg, "\n", &context);
		while(token != NULL)
		{
			SendInfo(f, -1, INFO_MMSG, token, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			token = strtok_s(NULL, "\n", &context);
		}
	}

	return 0;
}




void AddCard(PLAYFIELD *f, CARD* card, short pindx, unsigned long loc)
{
	unsigned short indx = 0;
	PLAYER *thisp = NULL;
	
	if(pindx != -1)
		thisp = f->p[pindx];

	card->thisp = pindx; // Set card's owner
	// Card's location is set after card is added to the proper location

	switch(ID(loc))
	{
		case LOC_HAND:
		{
			thisp->hand.push_back(card);
			indx = (unsigned short)(thisp->hand.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_ITEM:
		{
			thisp->item.push_back(card);
			indx = (unsigned short)(thisp->item.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_ALLY:
		{
			thisp->ally.push_back(card);
			indx = (unsigned short)(thisp->ally.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_ENMY:
		{
			thisp->enmy.push_back(card);
			indx = (unsigned short)(thisp->enmy.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_OTHR:
		{
			thisp->othr.push_back(card);
			indx = (unsigned short)(thisp->othr.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_FFLD:
		{
			f->field.push_back(card);
			indx = (unsigned short)(f->field.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_PRNK:
		{
			f->prank.push_back(card);
			indx = (unsigned short)(f->prank.size() - 1); // To convert size to 0-based index
			break;
		}
		case LOC_DCRD:
		{
			f->discard.push_front(card);
			indx = 0xFFFF;
			break;
		}
		case LOC_FLUX:
		{
			f->flux.push_back(card);
			indx = (unsigned short)(f->flux.size() - 1); // To convert size to 0-based index
			break;
		}
	}

	card->loc = loc | indx; // Set the card's location

	return;
}

CARD* GetCard(PLAYFIELD *f, short pindx, unsigned long loc)
{
	PLAYER *thisp = f->p[pindx];

	switch(ID(loc))
	{
		case LOC_HAND:
		{
			return thisp->hand[INDEX(loc)];
		}
		case LOC_ITEM:
		{
			return thisp->item[INDEX(loc)];
		}
		case LOC_ALLY:
		{
			return thisp->ally[INDEX(loc)];
		}
		case LOC_ENMY:
		{
			return thisp->enmy[INDEX(loc)];
		}
		case LOC_OTHR:
		{
			return thisp->othr[INDEX(loc)];
		}
		case LOC_FFLD:
		{
			return f->field[INDEX(loc)];
		}
		case LOC_PRNK:
		{
			return f->prank[INDEX(loc)];
		}
		case LOC_DCRD:
		{
			return f->discard[INDEX(loc)];
		}
		case LOC_FLUX:
		{
			return f->flux[INDEX(loc)];
		}
		default:
			return NULL;
	}
}

void RemoveCard(PLAYFIELD *f, CARD *card)
{
	short index = INDEX(card->loc);
	short pindx = card->thisp;
	long loc = ID(card->loc);
	PLAYER *thisp = (pindx >= 0 ? f->p[pindx] : NULL);
	deque<CARD*> *pile = NULL;
	int i;

	switch(loc)
	{
		case LOC_DRAW:
		{
			pile = &f->draw;
			break;
		}
		case LOC_HAND:
		{
			pile = &thisp->hand;
			break;
		}
		case LOC_ITEM:
		{
			pile = &thisp->item;
			break;
		}
		case LOC_ALLY:
		{
			pile = &thisp->ally;
			break;
		}
		case LOC_ENMY:
		{
			pile = &thisp->enmy;
			break;
		}
		case LOC_OTHR:
		{
			pile = &thisp->othr;
			break;
		}
		case LOC_FFLD:
		{
			pile = &f->field;
			break;
		}
		case LOC_PRNK:
		{
			pile = &f->prank;
			break;
		}
		case LOC_DCRD:
		{
			pile = &f->discard;
			auto iter = f->discard.begin();
			for(; iter != f->discard.end(); iter++)
			{
				if(*iter == card)
				{
					f->discard.erase(iter);
					break;
				}
			}
			break;
		}
		case LOC_FLUX:
		{
			pile = &f->flux;
			break;
		}
	}

	if(pile != &f->discard)
	{
		pile->erase(pile->begin() + index);
		for(i = 0; i < (signed)pile->size(); i++)
			(*pile)[i]->loc = ID((*pile)[i]->loc) | (short)i;
	}

	card->thisp = -1; // Card has no owner
	card->loc = 0; // Card has no location

	if(loc == LOC_HAND)
		SendInfo(f, pindx, INFO_HAND);

	return;
}

void MoveCardEvent(EVENT *etmp, short pindx, unsigned long loc)
{
	EVDONE *emove = new EVDONE;
	emove->edonetype = evdone_move;
	emove->target = pindx;
	emove->targetcard = NULL;
	emove->lval = loc;
	etmp->evdone.push_back(emove);
}

void MoveCard(PLAYFIELD *f, CARD *card, short pindx, unsigned long loc)
{
	RemoveCard(f, card);
	AddCard(f, card, pindx, loc);

	if(loc == LOC_HAND)
		SendInfo(f, pindx, INFO_HAND);
	else
		SendInfo(f, -1, INFO_FFLD);
}

void DrawHand(PLAYFIELD *f, ELIST *e, short pindx)
{
	int i;
	PLAYER *thisp = f->p[pindx];
	CARD *c;

	for(i = thisp->hand.size(); i < thisp->hmax; i++)
	{
		if(f->draw.empty())
		{
			ShuffleDeck(f);
			SendInfo(f, pindx, INFO_FFLD);
		}
		c = f->draw.back();
		f->draw.pop_back();
		AddCard(f, c, pindx, LOC_HAND);
		c->draw(e, c);
	}
	SendInfo(f, pindx, INFO_HAND);
}

void DiscardCardEvent(EVENT *etmp, CARD *card)
{
	EVDONE *ediscard = new EVDONE;
	ediscard->edonetype = evdone_discard;
	ediscard->targetcard = card;
	etmp->evdone.push_back(ediscard);
}

void DiscardCard(PLAYFIELD *f, ELIST *e, CARD *card)
{
	int i;

	CheckHook(f, e, HOOK_DISCARD, card, -1, false);
	card->hookreg.clearList();
	for(i = 0; i < f->nump; i++)
	{
		f->p[i]->hookreg.clearCard(card);
	}
	f->hookreg.clearCard(card);
	DeregisterEPlayer(f, card);
	MoveCard(f, card, -1, LOC_DCRD);
}

void ShuffleDeck(PLAYFIELD *f) // "Inside-out" version of Durstenfeld's implementation of the Fisher-Yates shuffle
{								// Thank you, Wikipedia! [http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm]
	unsigned int i, j;

	if(!f->draw.empty()) // If the draw pile still has cards...
		return; // ...then there is nothing that needs to be shuffled.
	if(f->discard.empty()) // If the discard pile is empty then we are shuffling the deck for the start of the game
	{
		f->draw.resize(f->d.size(), NULL); // So that we don't get any out-of-bound errors
		f->draw[0] = &f->d[0]; // a[0] = source[0]
		for(i = 1; i < f->d.size(); i++)// for i from 1 to n - 1 do
		{
			rand_s(&j); // j = random integer
			j %= (i + 1); // with 0 <= j <= i
			f->draw[i] = f->draw[j]; // a[i] = a[j]
			if(f->draw[i] != NULL)
				f->draw[i]->loc = LOC_DRAW | (short)i;
			f->draw[j] = &f->d[i]; // a[j] = source[i]
			f->draw[j]->loc = LOC_DRAW | (short)j;
		}
	}
	else // The discard pile is not empty so we are refilling the draw pile from the discard pile
	{
		f->draw.resize(f->discard.size(), NULL);
		f->draw[0] = f->discard[0]; // a[0] = source[0]
		for(i = 1; i < f->discard.size(); i++)// for i from 1 to n - 1 do
		{
			rand_s(&j); // j = random integer
			j %= (i + 1); // with 0 <= j <= i
			f->draw[i] = f->draw[j]; // a[i] = a[j]
			if(f->draw[i] != NULL)
				f->draw[i]->loc = LOC_DRAW | (short)i;
			f->draw[j] = f->discard[i]; // a[j] = source[i]
			f->draw[j]->loc = LOC_DRAW | (short)j;
		}
		f->discard.clear(); // Clear the discard pile
	}
	return;
}




void RegisterEPlayerEvent(EVENT *etmp, short pindx, short health, short params)
{
	EVDONE *eeplay = new EVDONE;

	eeplay->edonetype = evdone_eplay;
	eeplay->target = pindx;
	eeplay->sval = health;
	eeplay->lval = params;

	etmp->evdone.push_back(eeplay);
}

int RegisterEPlayer(PLAYFIELD *f, short pindx, CARD *card, char* name, short health, short params)
{
	EPLAYER *eptmp = new EPLAYER;

	strcpy_s(eptmp->name, 35, name);
	eptmp->health = health;
	eptmp->card = card;
	eptmp->pnum = EP | (short)f->ep.size(); // Because this will be the index after we push_back
	eptmp->thisp = pindx;
	eptmp->params = params;
	f->ep.push_back(eptmp); // Register into extra player deque
	card->ep = eptmp; // Point card to its registered player
	return 0;
}

void SendEPlayerHealth(PLAYFIELD *f, EPLAYER *ep)
{
	SendInfo(f, -1, INFO_EPLR, ep->card->indx, ep->health);
}

int DeregisterEPlayer(PLAYFIELD *f, CARD *card)
{
	EPLAYER *etmp = card->ep;
	deque<EPLAYER*>::iterator iter;

	if(etmp == NULL)
		return -1;

	f->ep.erase(f->ep.begin() + PINDX(etmp->pnum)); // Remove from extra player deque
	for(iter = f->ep.begin() + PINDX(etmp->pnum); iter != f->ep.end(); iter++)
	{
		(*iter)->pnum--;
	}
	delete etmp;
	card->ep = NULL;

	return 0;
}








int AutomagicDraw(ELIST *e, CARD *card) // See GameNotes.txt for full specs
{
	UNREFERENCED_PARAMETER(e);
	if(card->type & C_CNTR)
	{
		card->hookreg.registerItem(card, HOOK_SELECT, HK_SELCNTR, 0, &AutomagicSelC);
	}

	return 0;
}

int AutomagicSelC(PLAYFIELD *f, ELIST *e, CARD *card, CARD *ocard, short pnum, void* data)
{
	UNREFERENCED_PARAMETER(f);
	UNREFERENCED_PARAMETER(ocard);
	UNREFERENCED_PARAMETER(pnum);
	UNREFERENCED_PARAMETER(data);
	if(e->curev == -1)
	{
_ASSERTE(false);
		return -1;
	}
	if(card->atype != cv->atype)
		return -1;
	else
		return 0;
}

int AutomagicFunc(PLAYFIELD *f, ELIST *e, CARD *card, long target) // See GameNotes.txt for full specs
{
	short to = TO(target);
	if(card->type & C_ATCK)
	{
		if(target == NOTARGET)
			Target(f, e, card, AutomagicFunc, card->thisp);
		else
		{
			EVENT *etmp = new EVENT;
			etmp->card = card;
			etmp->from = card->thisp;
			etmp->to = to;
			etmp->type = C_ATCK;
			etmp->atype = card->atype;
			etmp->aff = card->aff;
			etmp->hit = card->health;

			if(card->custom_msg == NULL)
			{
				char msg[128];
				sprintf_s(msg, 128, "%%THISP played %s on %%TO", card->name);
				etmp->setmsg(f, msg);
			}
			else
				etmp->setmsg(f, card->custom_msg);

			e->push_back(etmp);
			DiscardCard(f, e, card);
		}
	}
	else if(card->type & C_CNTR)
	{
		EVENT *etmp = new EVENT;
		etmp->card = card;
		etmp->from = card->thisp;
		etmp->to = cv->from;
		etmp->type = C_CNTR;
		etmp->atype = card->atype;
		etmp->aff = card->aff;
		etmp->hit = card->health;

		if(card->custom_msg == NULL)
		{
			char msg[128];
			sprintf_s(msg, 128, "%%THISP countered with %s", card->name);
			etmp->setmsg(f, msg);
		}
		else
			etmp->setmsg(f, card->custom_msg);

		e->push_back(etmp);
		DiscardCard(f, e, card);
	}
	else if(card->type & C_HEAL)
	{
		if(target == NOTARGET)
			Target(f, e, card, AutomagicFunc, card->thisp, TARGET_HEAL);
		else 
		{
			EVENT *etmp = new EVENT;
			etmp->card = card;
			etmp->from = card->thisp;
			etmp->to = to;
			etmp->type = C_HEAL;
			etmp->atype = 0;
			etmp->aff = card->aff;
			etmp->hit = card->health;

			if(card->custom_msg == NULL)
			{
				char msg[128];
				sprintf_s(msg, 128, "%%THISP played %s", card->name);
				etmp->setmsg(f, msg);
			}
			else
				etmp->setmsg(f, card->custom_msg);

			e->push_back(etmp);
			DiscardCard(f, e, card);
		}
	}

	return 0;
}


int ProcessEvents(PLAYFIELD *f, ELIST *e)
{
	short pnum;
	int i;

	if(e == NULL) // If the event list doesn't exist
		return -1;
	if(e->empty()) // If the event list is empty
		return 1;
	else // The event list is not empty
	{
		i = 0;
		do
		{
			e->curev = (short)i;
			if(!cv->isprocessed)
			{
				if(cv->type & E_SLCT)
					SelectCallback(f, e, NULL, ProcessEventsHelperSelect, f->curp, false); // Tell curp to select a card
				else
				{
					SendMsg(f, cv);
					if(cv->type & C_ATCK)
					{
						CheckHook(f, e, HOOK_PLAY1, cv->card, cv->from, true);
						CheckHook(f, e, HOOK_PLAY2, NULL, cv->to, true);
					}
					if((cv->type & C_ATCK) && !(cv->atype & TYPE_U) && ISREAL(cv->to)) // If the event is an attack and we're not targeting an extra player and it's not uncounterable (so countering is ok)
						SelectCallback(f, e, NULL, ProcessEventsHelper, cv->to, true); // Have the other player select a counter
					else
					{
						cv->isprocessed = true;
						CheckHook(f, e, HOOK_CARD, NULL, -1, true);
						i = ProcessEventsHelper(f, e, NULL, -1);
					}
				}
			}
			if(i == (signed)e->size() - 1)
			{
				bool flag;
				do
				{
					flag = false;
					for(unsigned int j = 0; j < e->size(); j++)
					{
						// If any unprocessed, set flag
						if(!e->at(j)->isprocessed)
						{
							flag = true;
							break;
						}
					}
					Handler(f, e, &pnum);
				} while(flag && i == (signed)e->size() - 1); // While still unprocessed and no new items in event queue
			}
			i++;
		} while(i < (signed)e->size());
	}

	return 0;
}
int ProcessEventsHelperSelect(PLAYFIELD *f, ELIST *e, CARD *card, long target)
{
	UNREFERENCED_PARAMETER(card);
	short choice = TO(target);
	if(choice == -1)
	{
		char buf[128];
		sprintf_s(buf, 128, "%s can't play anything", cp->name);
		SendInfo(f, -1, INFO_MMSG, buf, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	}
	else
		f->d[choice].func(f, e, &f->d[choice], NOTARGET); // Call the card's main function (choice == index in master deck)
	return 0;
}
int ProcessEventsHelper(PLAYFIELD *f, ELIST *e, CARD *card, long target)
{
	UNREFERENCED_PARAMETER(card);
	short to = TO(target);
	if(target != -1)
	{
		f->d[to].func(f, e, &f->d[to], NOTARGET);
		DrawHand(f, e, cv->to);
	}
	else
	{
		while(CardSucc(f, e) == 1) // ...Succ the current event
			e->curev++;
	}
	return e->curev;
}

int CardSucc(PLAYFIELD *f, ELIST *e)
{
	int i, j;
	char tmp[128];

	short to = PINDX(cv->to);
	bool istofield = (cv->to == -1);
	bool istoreal = ISREAL(cv->to);
	bool isdead = false;

	if(!cv->nulled)
	{
		if(cv->type == C_ITEM)
			MoveCard(f, cv->card, cv->to, LOC_ITEM);
		else if(cv->type == C_FFLD)
			MoveCard(f, cv->card, cv->to, LOC_FFLD);
		else if(cv->type == C_PRNK)
			MoveCard(f, cv->card, cv->to, LOC_PRNK);
		else
		{
			for(j = 0; j < (signed)cv->evmod.size(); j++) // Multiply first
			{
				if(cv->evmod[j]->emodtype == evmod_multiply)
				{
					SendInfo(f, -1, INFO_MMSG, cv->evmod[j]->msg, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
					cv->hit *= cv->evmod[j]->value;
				}
			}
			for(j = 0; j < (signed)cv->evmod.size(); j++) // Then add
			{
				if(cv->evmod[j]->emodtype == evmod_add)
				{
					SendInfo(f, -1, INFO_MMSG, cv->evmod[j]->msg, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
					cv->hit += cv->evmod[j]->value;
				}
			}

			CheckHook(f, e, HOOK_DAMAGE0, cv->card, cv->to, true);
			if(cv->type & (C_ATCK | C_CNTR))
			{
				char *name;
				if(!istofield)
				{
					if(istoreal)
						name = f->p[to]->name;
					else
						name = f->ep[to]->name;
					if(cv->hit == 0)
						sprintf_s(tmp, 128, "%s: -0", name); // to get "-0" instead of "+0"
					else
						sprintf_s(tmp, 128, "%s: %+d", name, -cv->hit);
					if(istoreal || !(f->ep[to]->params & EPLAY_NONTARGET))
						SendInfo(f, -1, INFO_MMSG, tmp, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
				}
			}
			else if(cv->type & C_HEAL)
			{
				if(!istofield)
				{
					if(istoreal)
						sprintf_s(tmp, 128, "%s: %+d", f->p[to]->name, cv->hit);
					else
						sprintf_s(tmp, 128, "%s: %+d", f->ep[to]->name, cv->hit);
					SendInfo(f, -1, INFO_MMSG, tmp, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
				}
			}
			switch(cv->type)
			{
				case C_ATCK:
				case C_CNTR:
				{
					if(!istofield)
					{
						if(istoreal)
							f->p[to]->damage(f, e, cv->hit);
						else
							isdead = f->ep[to]->damage(f, e, cv->hit);
						CheckHook(f, e, HOOK_DAMAGE2, cv->card, cv->from, true);
					}
					break;
				}
				case C_HEAL:
				{
					if(!istofield)
					{
						if(istoreal)
							f->p[to]->heal(f, e, cv->hit);
						else
							isdead = f->ep[to]->heal(f, e, cv->hit);
					}
					break;
				}
			}
			SendInfo(f, -1, INFO_HLTH);
		}
		CheckHook(f, e, HOOK_SUCC, cv->card, cv->from, false);
		if(!cv->evdone.empty()) // If there is additional stuff to be done...
		{
			HLIST *hookreg;
			EVDONE *edone;
			for(i = 0; i < (signed)cv->evdone.size(); i++)
			{
				edone = cv->evdone[i];
				switch(edone->edonetype)
				{
					case evdone_reg:
					{
						if(edone->target == -1)
						{
							if(edone->targetcard == NULL)
								hookreg = &f->hookreg;
							else
								hookreg = &edone->targetcard->hookreg;
						}
						else if(edone->target == EP)
							hookreg = &cv->card->ep->hookreg;
						else if(ISREAL(edone->target))
							hookreg = &f->p[edone->target]->hookreg;
						else
							hookreg = &f->ep[PINDX(edone->target)]->hookreg;
						hookreg->registerItem(cv->card, edone->hook, edone->lval, edone->sval, edone->hfunc);
						break;
					}
					case evdone_move:
					{
						MoveCard(f, cv->card, edone->target, edone->lval);
						break;
					}
					case evdone_eplay:
					{
						RegisterEPlayer(f, edone->target, cv->card, cv->card->name, edone->sval, (short)edone->lval);
						break;
					}
					case evdone_discard:
					{
						DiscardCard(f, e, edone->targetcard);
						break;
					}
				}
			}
		}
	}

	return cv->ischained ? 1 : 0;
}

void RemoveEvents(ELIST *e, CARD *card)
{
	deque<EVENT*>::const_iterator iter = e->begin();
	while(iter != e->end())
	{
		if((*iter)->card == card)
			(*iter)->isprocessed = true;
		iter++;
	}
}


void SelectEvent(ELIST *e, short thisp)
{
	EVENT *etmp = new EVENT;
	etmp->to = thisp;
	etmp->type = E_SLCT;
	e->push_back(etmp);
}

void SelectNoBlock(PLAYFIELD *f, ELIST *e, short thisp, deque<TARGET> *t, INFO linfo, char* msg, CARD *card)
{
	cv->isprocessed = false;
	SendInfo(f, thisp, linfo, e->curev, t, msg, card);
}

short Select(PLAYFIELD *f, ELIST *e, short thisp, deque<TARGET> *t, char* msg, CARD *card, bool multisel)
{
	short choice;
	short pnum;

	cv->tproc = NULL;
	cv->hcard = NULL;
	SelectNoBlock(f, e, thisp, t, multisel ? INFO_MSEL : INFO_CARD, msg, card);
	do
	{
		choice = Handler(f, e, &pnum);
	} while(pnum != thisp || choice == -2); // Block on Handler until it receives an answer for us

	return choice;
}

void SelectCallback(PLAYFIELD *f, ELIST *e, CARD *card, FPROC tproc, short thisp, bool counters)
{
	int i;
	TARGET ttmp;
	deque<TARGET> t;
	INFO linfo;
	CARD *scard;

	for(i = 0; i < (signed)f->p[thisp]->hand.size(); i++)
	{
		ttmp.pnum = f->p[thisp]->hand[i]->indx;
		ttmp.stat = (short)CheckHook(f, e, HOOK_SELECT, f->p[thisp]->hand[i], thisp, true, counters);
		t.push_back(ttmp);
	}
	if(counters)
	{
		linfo = INFO_CNTR;
		scard = cv->card;
	}
	else
	{
		linfo = INFO_CARD;
		scard = NULL;
	}
	cv->tproc = tproc;
	cv->hcard = card;
	SelectNoBlock(f, e, thisp, &t, linfo, "", scard);
}

short TargetBlock(PLAYFIELD *f, ELIST *e, CARD *card, short from, unsigned short type, char* msg, CARD *activecard)
{
	short choice, pnum;

	Target(f, e, card, NULL, from, type, msg, activecard);
	do
	{
		choice = Handler(f, e, &pnum);
	} while(pnum != from || choice == -2); // Block on Handler until it receives an answer for us

	return choice;
}

void Target(PLAYFIELD *f, ELIST *e, CARD *card, FPROC tproc, short from, unsigned short type, char* msg, CARD *activecard)
{
	int i;
	TARGET ttmp;
	deque<TARGET> t;

	if(dbgmodeh && dbgtarget != -1)
	{
		_ASSERTE(false && "When do we get here?");
		tproc(f, e, card, dbgtarget);
		return;
	}

	if(type & TARGET_NORMAL)
	{	
		for(i = 0; i < f->nump; i++)
		{
			strcpy_s(ttmp.name, 35, f->p[i]->name);
			ttmp.pnum = (short)i;
			ttmp.stat = (i == from ? -2 : 0);
			t.push_back(ttmp);
		}
		if(!(type & TARGET_NOEP))
		{
			for(i = 0; i < (signed)f->ep.size(); i++) // For "extra players"
			{
				if(f->ep[i]->thisp == f->curp || f->ep[i]->thisp == -1) // If it's for this player or if it's for everyone
				{
					if(!(f->ep[i]->params & EPLAY_NONTARGET))
					{
						strcpy_s(ttmp.name, 35, f->ep[i]->name);
						ttmp.pnum = f->ep[i]->pnum;
						switch(f->ep[i]->card->type)
						{
						case C_ALLY:
							if(f->ep[i]->params & EPLAY_EMO)
								ttmp.stat = 0;
							else
								ttmp.stat = -2;
							break;
						case C_ENMY:
							if(f->ep[i]->params & EPLAY_EMO)
								ttmp.stat = -2;
							else
								ttmp.stat = 0;
							break;
						default:
							ttmp.stat = 0;
							break;
						}
						t.push_back(ttmp);
					}
				}
			}
		}
	}
	else if(type & TARGET_HEAL) // heal
	{
		strcpy_s(ttmp.name, 35, cp->name);
		ttmp.pnum = from;
		ttmp.stat = 0;
		t.push_back(ttmp);
		if(!(type & TARGET_NOEP))
		{
			for(i = 0; i < (signed)f->ep.size(); i++) // For "extra players"
			{
				if(f->ep[i]->thisp == f->curp || f->ep[i]->thisp == -1) // If it's for this player or if it's for everyone
				{
					if(!(f->ep[i]->params & EPLAY_NONTARGET))
					{
						strcpy_s(ttmp.name, 35, f->ep[i]->name);
						ttmp.pnum = f->ep[i]->pnum;
						switch(f->ep[i]->card->type)
						{
						case C_ALLY:
							if(f->ep[i]->params & EPLAY_EMO)
								ttmp.stat = -2;
							else
								ttmp.stat = 0;
							break;
						case C_ENMY:
							if(f->ep[i]->params & EPLAY_EMO)
								ttmp.stat = 0;
							else
								ttmp.stat = -2;
							break;
						default:
							ttmp.stat = -2;
							break;
						}
						t.push_back(ttmp);
					}
				}
			}
		}
	}

	for(i = 0; i < (signed)t.size(); i++)
	{
		int rval;
		rval = CheckHook(f, e, HOOK_TARGET2, card, t[i].pnum, true, false, &from);
		if(rval != 0)
			t[i].stat = (short)rval;
		rval = CheckHook(f, e, HOOK_TARGET1, card, t[i].pnum, true);
		if(rval != 0)
			t[i].stat = (short)rval;
	}

	cv->tproc = tproc;
	cv->hcard = card;
	cv->isprocessed = false;
	SendInfo(f, from, INFO_TRGT, e->curev, &t, msg, activecard);
}




long CheckHook(PLAYFIELD *f, ELIST *e, HOOK hook, CARD *card, short pnum, bool checkpf, bool counters, void* data)
{
	int i, num;
	short curpriority;
	unsigned long check;
	bool flag;
	int maxpriority = 0, tmp1 = 0, tmp2 = 0;
	int index;
	long rval = 0, rvaltotal = 0;
	BPLAYER *bp;

	if(card != NULL)
	{
		maxpriority = card->hookreg.getMaxPriority(hook);
		if(hook == HOOK_SELECT)
		{
			if((counters && !(card->type & C_CNTR)) || (!counters && !(card->type & C_PLAY)))
				return -1;
		}
	}
	if(!ISREAL(pnum)) // pnum has to be a real player
		pnum = -1;
	if(pnum == -1)
		bp = NULL;
	else if(ISREAL(pnum))
		bp = f->p[pnum];
	else
		bp = f->ep[PINDX(pnum)];
	if(bp != NULL)
	{
		tmp1 = bp->hookreg.getMaxPriority(hook);
		maxpriority = max(tmp1, maxpriority);
	}
	if(checkpf)
	{
		tmp2 = f->hookreg.getMaxPriority(hook);
		maxpriority = max(tmp2, maxpriority);
	}

	for(curpriority = 0; curpriority <= maxpriority; curpriority++)
	{
		if(card != NULL)
		{
			num = card->hookreg.getNum(hook, curpriority);
			index = -1;
			for(i = 0; i < num; i++)
			{
				flag = true;
				index = card->hookreg.getNextIndex(hook, curpriority, index);
				check = card->hookreg.getSpec(hook, index);
				flag = CheckSpecific(check, card->aff, card->type, card->atype, counters);
				if(flag)
				{
					rval = card->hookreg.callFunc(f, e, hook, index, card, pnum, data);
					rvaltotal += rval;
				}
			}
		}
		if(bp != NULL)
		{
			num = bp->hookreg.getNum(hook, curpriority);
			index = -1;
			for(i = 0; i < num; i++)
			{
				flag = true;
				index = bp->hookreg.getNextIndex(hook, curpriority, index);
				if(card != NULL)
				{
					check = bp->hookreg.getSpec(hook, index);
					flag = CheckSpecific(check, card->aff, card->type, card->atype, counters);
				}
				if(flag)
				{
					rval = bp->hookreg.callFunc(f, e, hook, index, card, pnum, data);
					rvaltotal += rval;
				}
			}
		}
		if(checkpf)
		{
			num = f->hookreg.getNum(hook, curpriority);
			index = -1;
			for(i = 0; i < num; i++)
			{
				flag = true;
				index = f->hookreg.getNextIndex(hook, curpriority, index);
				check = f->hookreg.getSpec(hook, index);
				if(card != NULL)
					flag = CheckSpecific(check, card->aff, card->type, card->atype, counters);
				else
					flag = CheckSpecific(check, cv->aff, cv->type, cv->atype, counters);
				if(flag)
				{
					rval = f->hookreg.callFunc(f, e, hook, index, card, pnum, data);
					rvaltotal += rval;
				}
			}
		}
	}

	if(hook == HOOK_TOP) // If any hooks return >0, signal the game loop
		return rvaltotal;
	return rval;
}

bool CheckSpecific(unsigned long check, unsigned short aff, unsigned long type, unsigned short atype, bool counters)
{
	if(check == 0) // hook is unspecific so it applies to all cases
		return true;

	if(check & HK_SELCNTR && counters)
		return true;
	else if(check & HK_AFF)
	{
		if((~HK_AFF & check) & aff) // If the aff part of check AND aff
			return true;
	}
	else if(check & HK_TYPE)
	{
		if((~HK_TYPE & check) & type) // If the type part of check AND type
			return true;
	}
	else if(check & HK_ATYPE)
	{
		if((~HK_ATYPE & check) & atype) // If the atype part of check AND atype
			return true;
	}

	return false;
}







void LazyFailH(char* errmsg)
{
	MessageBox(NULL, errmsg, "Error from gamehost's LazyFailH(", MB_ICONERROR);
	exit(1);
}
