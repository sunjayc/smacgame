#pragma once

struct CARD;
struct CCARD;

void card(int, bool bottom = false); // Hack to handle when first parameter is NULL (otherwise compiler can't tell which of the other two to call)
void card(CARD *c, bool bottom = false, WORD color = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY); // Casts to CCARD and passes on to actual function
void card(CCARD *c, bool bottom = false, WORD color = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY); // Actual function
