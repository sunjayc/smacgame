#include <conio.h>
#include <Windows.h>
#include <stdio.h>
#include <deque>
using namespace std;
#include "SMACclient.h"
#include "interface.h"

extern HANDLE hScr;

void card(int, bool bottom)
{
	card((CCARD*)NULL, bottom);
}

void card(CARD *c, bool bottom, WORD color)
{
	card((CCARD*)c, bottom, color);
}

void card(CCARD *c, bool bottom, WORD color)
{
	int i, j, k;
	CHAR_INFO ci[38 * 15];
	int len, left, right;
	COORD size, pos;	//		   1		 2		   3
	SMALL_RECT sm;	   //01234567890123456789012345678901234567
	char cd[15][39] = {	"旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커", // 0
						"�                               SMAC!�", // 1 
						"�                Name                �", // 2
						"�                                    �", // 3 [Health for enemies or allies or whatever would go here]
						"� 旼컴컫컴컴컴컴컴컴컴컴컴컴컴쩡컴커 �", // 4
						"� �    �         Type         �    � �", // 5
						"� �    읕컴컴컴컴컴컴컴컴컴컴켸    � �", // 6
						"� �              Desc              � �", // 7
						"� �                                � �", // 8
						"� �                                � �", // 9
						"� �                                � �", // 10
						"� 읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸 �", // 11
						"�              FlavText              �", // 12
						"�                                    �", // 13
						"읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸"};// 14
	char atypes[][10] = {"General/", "Physical/", "Verbal/", "Mental/"};
	char types[][10] = {"Attack/", "Counter/", "Item/", "Field/", "Heal/", "Special/", "Ally/", "Enemy/", "Prank/"};
	char pad[18] = "                 ";
	char buf[133] = "";

	size.X = 38;
	size.Y = 15;
	pos.X = 0;
	pos.Y = 0;
	sm.Top = (bottom ? 15 : 0);
	sm.Left = 80 - 38;
	sm.Bottom = (bottom ? 29 : 14);
	sm.Right = 79;

	if(c == NULL)
	{
		for(i = 0; i < 15; i++)
		{
			for(j = 0; j < 38; j++)
			{
				ci[i*38 + j].Char.AsciiChar = ' ';
				ci[i*38 + j].Attributes = 7;
			}
		}
		WriteConsoleOutputA(hScr, ci, size, pos, &sm);
		return;
	}

	cd[1][32] = (c->aff & AFF_S) ? 'S' : ' ';
	cd[1][33] = (c->aff & AFF_M) ? 'M' : ' ';
	cd[1][34] = (c->aff & AFF_A) ? 'A' : ' ';
	cd[1][35] = (c->aff & AFF_C) ? 'C' : ' ';
	cd[1][36] = (c->aff & AFF_I) ? '!' : ' ';

	len = strlen(c->name);
	right = (34 - len) / 2;
	left = (34 - len) - right;
	sprintf_s(cd[2], 39, "� %.*s%s%.*s �", left, pad, c->name, right, pad);

	if(c->type & (C_ENMY | C_ALLY) && c->health != 0)
	{
		sprintf_s(buf, 133, "Health: %d", c->health);
		len = strlen(buf);
		right = (34 - len) / 2;
		left = (34 - len) - right;
		sprintf_s(cd[3], 39, "� %.*s%s%.*s �", left, pad, buf, right, pad);
		buf[0] = '\0';
	}

	if(c->atype & (TYPE_G | TYPE_P | TYPE_V | TYPE_M))
	{
		if(c->atype & TYPE_G)
			strcat_s(buf, 133, atypes[0]);
		if(c->atype & TYPE_P)
			strcat_s(buf, 133, atypes[1]);
		if(c->atype & TYPE_V)
			strcat_s(buf, 133, atypes[2]);
		if(c->atype & TYPE_M)
			strcat_s(buf, 133, atypes[3]);
		buf[strlen(buf) - 1] = ' ';
	}

	if(c->type & C_ATCK)
		strcat_s(buf, 133, types[0]);
	if(c->type & C_CNTR)
		strcat_s(buf, 133, types[1]);
	if(c->type & C_ITEM)
		strcat_s(buf, 133, types[2]);
	if(c->type & C_FFLD)
		strcat_s(buf, 133, types[3]);
	if(c->type & C_HEAL)
		strcat_s(buf, 133, types[4]);
	if(c->type & C_SPCL)
		strcat_s(buf, 133, types[5]);
	if(c->type & C_ALLY)
		strcat_s(buf, 133, types[6]);
	if(c->type & C_ENMY)
		strcat_s(buf, 133, types[7]);
	if(c->type & C_PRNK)
		strcat_s(buf, 133, types[8]);
	len = strlen(buf);
	if(len > 0)
	{
		len--;
		buf[len] = '\0';
	}
	right = (22 - len) / 2;
	left = (22 - len) - right;
	sprintf_s(cd[5], 39, "� �    �%.*s%s%.*s�    � �", left, pad, buf, right, pad);

	strcpy_s(buf, 133, c->desc);
	j = 0;
	len = strlen(buf);
	for(i = 0; i < len; i++)
	{
		if(buf[i] == '\n')
		{
			buf[i] = '\0';
			j++;
		}
	}
	j++; // '\0' that is already at end of string
	i = 0;
	k = 0;
	while(i < j)
	{
		len = strlen(buf + k);
		right = (32 - len) / 2;
		left = (32 - len) - right;
		if(len > 32)
		{
			j++;
			if(j > 4)
				j = 4;
			sprintf_s(cd[7 + i], 39, "� �%.32s� �", buf + k);
			k += 32;
		}
		else
		{
			sprintf_s(cd[7 + i], 39, "� �%.*s%s%.*s� �", left, pad, buf + k, right, pad);
			k += len + 1;
		}
		i++;
	}

	strcpy_s(buf, 133, c->flavtxt);
	j = 0;
	len = strlen(buf);
	for(i = 0; i < len; i++)
	{
		if(buf[i] == '\n')
		{
			buf[i] = '\0';
			j++;
		}
	}
	j++; // '\0' that is already at end of string
	i = 0;
	k = 0;
	while(i < j)
	{
		len = strlen(buf + k);
		right = (32 - len) / 2;
		left = (32 - len) - right;
		if(len > 32)
		{
			j++;
			if(j > 4)
				j = 4;
			sprintf_s(cd[12 + i], 39, "�  %.32s  �", buf + k);
			k += 32;
		}
		else
		{
			sprintf_s(cd[12 + i], 39, "�  %.*s%s%.*s  �", left, pad, buf + k, right, pad);
			k += len + 1;
		}
		i++;
	}
	
	for(i = 0; i < 15; i++)
	{
		for(j = 0; j < 38; j++)
		{
			ci[i*38 + j].Char.AsciiChar = cd[i][j];
			ci[i*38 + j].Attributes = color;
		}
	}

	WriteConsoleOutputA(hScr, ci, size, pos, &sm);
}
