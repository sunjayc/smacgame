#pragma once

// Defined in Cards.cpp:
void		InitCards(deque<CARD> *d);

// Defined in CardCode.cpp:
void		RegisterHookEvent(EVENT *etmp, short target, CARD *targetcard, HOOK hook, unsigned long specific, short priority, HPROC hfunc);

// Defined in gamehost.cpp:
void		MoveCardEvent(EVENT *etmp, short pindx, unsigned long loc);
void		MoveCard(PLAYFIELD *f, CARD *card, short pindx, unsigned long loc);
void		DrawHand(PLAYFIELD *f, ELIST *e, short pindx);
void		DiscardCardEvent(EVENT *etmp, CARD *card);
void		DiscardCard(PLAYFIELD *f, ELIST *e, CARD *card);

int			AutomagicDraw(ELIST *e, CARD *card);
int			AutomagicSelC(PLAYFIELD *f, ELIST *e, CARD *card, CARD *ocard, short p, void* data);
int			AutomagicFunc(PLAYFIELD *f, ELIST *e, CARD *card, long target);

void		SelectEvent(ELIST *e, short thisp);
short		Select(PLAYFIELD *f, ELIST *e, short thisp, deque<TARGET> *t, char* msg, CARD *card, bool multisel = false);

#define TARGET_NORMAL	0x0001
#define TARGET_HEAL		0x0002
#define TARGET_NOEP		0x0004 // For when we don't want to be able to target extra players
void		Target(PLAYFIELD *f, ELIST *e, CARD *card, FPROC tproc, short from, unsigned short type = TARGET_NORMAL, char* msg = "", CARD *activecard = NULL);
short		TargetBlock(PLAYFIELD *f, ELIST *e, CARD *card, short from, unsigned short type = TARGET_NORMAL, char* msg = "", CARD *activecard = NULL);

void		RegisterEPlayerEvent(EVENT *etmp, short pindx, short health, short params);
void		SendEPlayerHealth(PLAYFIELD *f, EPLAYER *ep);
int			DeregisterEPlayer(PLAYFIELD *f, CARD *card);

void		RemoveEvents(ELIST *e, CARD *card);

long		CheckHook(PLAYFIELD *f, ELIST *e, HOOK hook, CARD *card, short pnum, bool checkpf, bool counters = false, void* data = NULL);
