#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <direct.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

fstream fs, cfs;

#define DPROC "(PLAYFIELD *f, ELIST *e, CARD *card);"
#define FPROC "(PLAYFIELD *f, ELIST *e, CARD *card, long target);"

int getnext()
{
	int c;
	
	do
	{
		c = fs.get();
	} while(c == ' ' || c == '\t' || c == '\n');

	return c;
}

int readstring(char* buf, size_t len)
{
	int c = getnext();
	unsigned int n = 0;
	
	if(c == EOF)
		return c;
	else if(c != '"')
	{
		cfs.close();
		fs.close();
		cerr << "Invalid file [%c|%s]" << endl;
		cerr << "Invalid file [" << c << "|" << buf << "]" << endl;
		exit(1);
	}
	c = fs.get();
	while(c != '"' && n < len - 1)
	{
		if(c == '\\')
		{
			buf[n++] = c;
			c = fs.get();
		}
		buf[n++] = c;
		c = fs.get();
	}
	buf[n++] = '\0';
	return c;
}

int readvalue(char* buf, size_t len, int end=',')
{
	int c = getnext();
	unsigned int n = 0;
	while(c != end && c != EOF && n < len - 1)
	{
		buf[n++] = c;
		c = fs.get();
	}
	buf[n++] = '\0';
	return c;
}

void readfunc(char* buf, size_t len, char* format, int end=',')
{
	readvalue(buf, len, end);
	if(strcmp(buf, "NULL") != 0)
	{
		cfs << "int " << buf << format << "\n";
	}
}

int main(int argc, char** argv)
{
	int c;
	char buf[1024];
	unsigned long n = 0;
	stringstream cards;

	if(argc <= 3)
	{
		cerr << "Usage: CardBuilder.exe <dir> <input file> <card output file>" << endl;
		return 1;
	}

	_chdir(argv[1]);

	fs.open(argv[2], fstream::in);
	cfs.open(argv[3], fstream::out | fstream::trunc);
	
	cfs << "// Generated from " << argv[2] << "\n\n#include \"../SMAChost.h\"\n\n";

	do
	{
		// Name
		if(readstring(buf, 1024) == EOF)
			break;
		cards << "{\"" << buf << "\", \n";
		c = getnext();
		if(c != ',')
		{
			cfs.close();
			fs.close();
			cerr << "Invalid file [" << c << "|" << buf << "]" << endl;
			return 1;
		}

		// Desc
		readstring(buf, 1024);
		cards << "\"" << buf << "\", \n";
		c = getnext();
		if(c != ',')
		{
			cfs.close();
			fs.close();
			cerr << "Invalid file [" << c << "|" << buf << "]" << endl;
			return 1;
		}

		// Flavtxt
		readstring(buf, 1024);
		cards << "\"" << buf << "\", \n";
		c = getnext();
		if(c != ',')
		{
			cfs.close();
			fs.close();
			cerr << "Invalid file [" << c << "|" << buf << "]" << endl;
			return 1;
		}
		
		// Aff
		readvalue(buf, 1024);
		cards << buf << ", \n";
		
		// Type
		readvalue(buf, 1024);
		cards << buf << ", \n";
		
		// Id
		readvalue(buf, 1024);
		cards << buf << ", \n";
		
		// Atype
		readvalue(buf, 1024);
		cards << buf << ", \n";
		
		// Health
		readvalue(buf, 1024);
		cards << buf << ", \n";
		
		// Draw
		readfunc(buf, 1024, DPROC);
		cards << buf << ", \n";
		
		// Func
		readfunc(buf, 1024, FPROC);
		cards << buf << ", \n";
		
		// Custom_message
		c = readvalue(buf, 1024, ';');
		cards << buf << "}, \n\n";
		n++;
	} while(c != EOF);

	cfs << "\nCARD mdeck[] = {\n" << cards.str() << "};\n\nunsigned long mdecklen = " << n << ";\n";

	cfs.close();
	fs.close();
	return 0;
}