#include "..\CardCode.h"

MAKE_FPROC(NoYourMomFunc)
{
	EVENT *etmp = new EVENT;
	if((cv->card->type & C_ATCK) && (cv->card->id == ID_YMOM))
	{
		etmp->card = card;
		etmp->from = card->thisp;
		etmp->to = cv->from;
		etmp->type = C_CNTR;
		etmp->atype = TYPE_V;
		etmp->aff = AFF_S | AFF_M | AFF_A | AFF_C;
		etmp->hit = 4;
		etmp->setmsg(f, "%THISP countered with No Your Mom");
		e->push_back(etmp); // Push counter's event onto original event's event list
		DiscardCard(f, e, card);
	}
	else
		return AutomagicFunc(f, e, card, target);
	return 0;
}