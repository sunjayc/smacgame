#include "..\CardCode.h"

MAKE_FPROC(CrudComixFunc)
{
	short to;
	GET_TARGET(to, CrudComixFunc);
	EVENT *etmp = AffEvent(f, card, to, 2, 0, 0, -1, 0);
	etmp->setmsg(f, "%THISP played Crud Comix on %TO (%HIT damage)");
	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}

MAKE_FPROC(NoLimbsFunc)
{
	short to;
	GET_TARGET(to, NoLimbsFunc);
	EVENT *etmp = AffEvent(f, card, to, -1, 0, 0, 2, 0);
	etmp->setmsg(f, "%THISP: \"Hey %TO! You have no limbs!\" (%HIT damage)");
	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}

MAKE_FPROC(CondesendingFunc)
{
	short to;
	GET_TARGET(to, CondesendingFunc);
	EVENT *etmp = AffEvent(f, card, to, 0, 0, 1, 0, 0);
	etmp->hit = card->health + (etmp->hit >= 3 ? 1 : 0);
	etmp->setmsg(f, "%THISP played condesending on %TO");
	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}
