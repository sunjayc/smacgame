#include "..\CardCode.h"

MAKE_FPROC(RollingBotherFunc)
{
	short to, num, choice;
	unsigned short i;
	TARGET ttmp;
	deque<TARGET> t;
	deque<CARD*> todiscard;
	char buf[128] = "%THISP played rolling bother on %TO";
	char tmp[128];

	GET_TARGET(to, RollingBotherFunc);

	for(i = 0; i < f->p[card->thisp]->hand.size(); i++)
	{
		CARD *c = f->p[card->thisp]->hand[i];
		if(c->id == ID_BTHR && c != card)
		{
			ttmp.pnum = f->p[card->thisp]->hand[i]->indx;
			t.push_back(ttmp);
		}
	}
	cv->isprocessed = false; // cv is the E_SLCT from when the player was selecting this card from their hand
	// Select needs a cv; we'll just piggy-back off of the existing one
	num = Select(f, e, card->thisp, &t, "", NULL, true);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	
	todiscard.push_back(card);
	for(int i = 0; i < num; i++)
	{
		LazyRecv(&f->p[card->thisp]->psock, &choice, 2);
		CARD *ocard = &f->d[choice];
		etmp->hit += ocard->health;
		sprintf_s(tmp, "\n%%THISP also used %s", ocard->name);
		strcat_s(buf, tmp);
		todiscard.push_back(ocard);
	}
	etmp->hit = (short)(2 * todiscard.size()) + 1;
	strcat_s(buf, "\nTotal: %HIT damage");
	etmp->setmsg(f, buf);
	e->push_back(etmp);
	for(unsigned int i = 0; i < todiscard.size(); i++)
	{
		DiscardCard(f, e, todiscard[i]);
	}

	return 0;
}

MAKE_FPROC(BotherMachineGunFunc)
{
	short to, num, choice;
	unsigned short i, j;
	TARGET ttmp;
	deque<TARGET> t;
	deque<CARD*> todiscard;
	char buf[128] = "%THISP played bother machine gun on %TO";
	char tmp[128];

	GET_TARGET(to, BotherMachineGunFunc);

	for(j = 0; j < f->nump; j++)
	{
		deque<CARD*> &hand = f->p[j]->hand;
		for(i = 0; i < hand.size(); i++)
		{
			CARD *c = hand[i];
			if(c->id == ID_BTHR && c != card)
			{
				ttmp.pnum = hand[i]->indx;
				t.push_back(ttmp);
			}
		}
	}
	for(i = 0; i < f->discard.size(); i++)
	{
		CARD *c = f->discard[i];
		if(c->id == ID_BTHR)
		{
			ttmp.pnum = f->discard[i]->indx;
			t.push_back(ttmp);
		}
	}
	cv->isprocessed = false; // cv is the E_SLCT from when the player was selecting this card from their hand
	// Select needs a cv; we'll just piggy-back off of the existing one
	num = Select(f, e, card->thisp, &t, "", NULL, true);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	
	todiscard.push_back(card);
	for(int i = 0; i < num; i++)
	{
		LazyRecv(&f->p[card->thisp]->psock, &choice, 2);
		CARD *ocard = &f->d[choice];
		etmp->hit += ocard->health;
		sprintf_s(tmp, "\n%%THISP also used %s", ocard->name);
		strcat_s(buf, tmp);
		todiscard.push_back(ocard);
	}
	etmp->hit = (short)(2 * todiscard.size()) + 1;
	strcat_s(buf, "\nTotal: %HIT damage");
	etmp->setmsg(f, buf);
	e->push_back(etmp);
	for(unsigned int i = 0; i < todiscard.size(); i++)
	{
		DiscardCard(f, e, todiscard[i]);
	}

	return 0;
}
