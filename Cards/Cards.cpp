#include "..\CardCode.h"

extern CARD mdeck[];
extern unsigned long mdecklen;

#pragma warning(push)
#pragma warning(disable:4100)

MAKE_FPROC(AgreeFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = cv->from;
	etmp->type = C_CNTR;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	etmp->hit = cv->hit + 1;

	etmp->setmsg(f, "%THISP Agreed");

	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}

MAKE_HPROC(AmnesiaDustHTop)
{
	DiscardCard(f, e, card);
	return 1;
}
MAKE_FPROC(AmnesiaDustFunc)
{
	short to;

	GET_TARGET(to, AmnesiaDustFunc);
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	etmp->hit = card->health;
	etmp->setmsg(f, "%THISP played Amnesia dust on %TO");

	MoveCardEvent(etmp, to, LOC_OTHR);
	RegisterHookEvent(etmp, to, NULL, HOOK_TOP, 0, PRIORITY_SPECIAL, AmnesiaDustHTop);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_FPROC(BoomFunc)
{
	short i;
	EVENT *etmp;

	etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = E_FAKE;
	etmp->setmsg(f, "%THISP played BOOM");
	e->push_back(etmp);

	for(i = 0; i < f->nump; i++)
	{
		etmp = new EVENT;
		etmp->card = card;
		etmp->from = card->thisp;
		etmp->to = i;
		etmp->type = C_ATCK;
		etmp->atype = card->atype;
		etmp->aff = card->aff;
		etmp->hit = card->health;
		e->push_back(etmp);
	}
	DiscardCard(f, e, card);
	return 0;
}

MAKE_HPROC(ExtraArmHDiscard)
{
	f->p[card->thisp]->hmax -= 1;
	return 0;
}
MAKE_HPROC(ExtraArmHSucc)
{
	f->p[card->thisp]->hmax += 1;
	card->hookreg.registerItem(card, HOOK_DISCARD, 0, PRIORITY_ITEM, ExtraArmHDiscard);
	DrawHand(f, e, card->thisp);
	return 0;
}
MAKE_FPROC(ExtraArmFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_ITEM;
	etmp->setmsg(f, "%THISP now has an extra arm");

	card->hookreg.registerItem(card, HOOK_SUCC, 0, PRIORITY_ITEM, ExtraArmHSucc);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_FPROC(OopsIDroppedItFunc)
{
	short to, choice;
	unsigned short i;
	TARGET ttmp;
	deque<TARGET> t;

	GET_TARGET(to, OopsIDroppedItFunc);

	for(i = 0; i < f->p[to]->item.size(); i++)
	{
		ttmp.pnum = f->p[to]->item[i]->indx;
		ttmp.stat = 0;
		t.push_back(ttmp);
	}
	cv->isprocessed = false; // cv is the E_SLCT from when the player was selecting this card from their hand
	// Select needs a cv; we'll just piggy-back off of the existing one
	choice = Select(f, e, card->thisp, &t, "Choose an item:", NULL);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_SPCL;
	etmp->setmsg(f, "%THISP played Oops I dropped it on %TO");
	DiscardCardEvent(etmp, &f->d[choice]);

	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}

MAKE_HPROC(DeafHSysmessage)
{
	char** msg = (char**)data;
	if((*msg)[0] != '\0') // If not a blank message
		*msg = "(noise)";
	return 0;
}
MAKE_HPROC(DeafHDamage0)
{
	_ASSERTE(cv->atype & TYPE_V);
	cv->hit = 0;
	return 0;
}
MAKE_FPROC(DeafFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_ITEM;
	etmp->setmsg(f, "%THISP is now deaf");

	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_SYSMESSAGE, 0, PRIORITY_ITEM, DeafHSysmessage);
	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_DAMAGE0, HK_ATYPE | TYPE_V, PRIORITY_ITEM, DeafHDamage0);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(BigFatTargetHBottom)
{
	if(card->ldata == 0)
	{
		EVENT *etmp = new EVENT;
		etmp->card = card;
		etmp->from = card->thisp;
		etmp->to = -1;
		etmp->type = E_FAKE;
		etmp->setmsg(f, "%THISP is no longer a big fat target");
		e->push_back(etmp);
		DiscardCard(f, e, card);
	}
	card->ldata--; // This is afterwards to get around off-by-one, since we'll get a call to HOOK_BOTTOM right after the card is first played
	return 0;
}
MAKE_FPROC(BigFatTargetFunc)
{
	short to;

	GET_TARGET(to, BigFatTargetFunc);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_SPCL;
	etmp->setmsg(f, "%THISP played big fat target on %TO");

	MoveCardEvent(etmp, to, LOC_OTHR);
	RegisterHookEvent(etmp, to, NULL, HOOK_TARGET2, 0, PRIORITY_SPECIAL, ForceNotMe);

	card->ldata = 1;
	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_BOTTOM, 0, PRIORITY_SPECIAL, BigFatTargetHBottom);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(RanchInMilkHCard)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = -1;
	etmp->to = cv->from;
	etmp->type = C_ATCK;
	etmp->atype = TYPE_P;
	etmp->aff = 0;
	etmp->hit = cv->hit;
	etmp->setmsg(f, "but there was ranch in the milk!");
	cv->nulled = true;
	e->push_back(etmp);
	DiscardCard(f, e, card);
	return 0;
}
MAKE_FPROC(RanchInMilkFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = C_PRNK;
	etmp->setmsg(f, "%THISP put down a prank");

	RegisterHookEvent(etmp, -1, NULL, HOOK_CARD, HK_TYPE | C_HEAL, PRIORITY_FIELD, RanchInMilkHCard);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_FPROC(ChairStareFunc)
{
	short to, num, choice;
	unsigned short i;
	TARGET ttmp;
	deque<TARGET> t;
	deque<CARD*> todiscard;
	char buf[128] = "%THISP played Chair Stare on %TO";
	char tmp[128];

	GET_TARGET(to, ChairStareFunc);

	for(i = 0; i < f->p[card->thisp]->hand.size(); i++)
	{
		CARD *c = f->p[card->thisp]->hand[i];
		if(c->id == ID_STRE && c != card)
		{
			ttmp.pnum = f->p[card->thisp]->hand[i]->indx;
			t.push_back(ttmp);
		}
	}
	cv->isprocessed = false; // cv is the E_SLCT from when the player was selecting this card from their hand
	// Select needs a cv; we'll just piggy-back off of the existing one
	num = Select(f, e, card->thisp, &t, "", NULL, true);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = card->atype;
	etmp->aff = card->aff;
	etmp->hit = card->health;
	
	todiscard.push_back(card);
	for(int i = 0; i < num; i++)
	{
		LazyRecv(&f->p[card->thisp]->psock, &choice, 2);
		CARD *ocard = &f->d[choice];
		etmp->hit += ocard->health;
		sprintf_s(tmp, "\n%%THISP also used %s", ocard->name);
		strcat_s(buf, tmp);
		todiscard.push_back(ocard);
	}
	strcat_s(buf, "\nTotal: %HIT damage");
	etmp->setmsg(f, buf);
	e->push_back(etmp);
	for(unsigned int i = 0; i < todiscard.size(); i++)
	{
		DiscardCard(f, e, todiscard[i]);
	}

	return 0;
}

MAKE_HPROC(NoBrainerHSysmessage)
{
	char** msg = (char**)data;
	int len = strlen(*msg);
	for(int i = 0; i < len; i++)
	{
		(*msg)[i] = rand() % 255;
	}
	return 0;
}
MAKE_HPROC(NoBrainerHDamage0)
{
	_ASSERTE(cv->atype & TYPE_M);
	cv->hit = 0;
	return 0;
}
MAKE_FPROC(NoBrainerFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_ITEM;
	etmp->setmsg(f, "%THISP has no mind!");

	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_SYSMESSAGE, 0, PRIORITY_ITEM, NoBrainerHSysmessage);
	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_DAMAGE0, HK_ATYPE | TYPE_M, PRIORITY_ITEM, NoBrainerHDamage0);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(RevengeHDamage1)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = cv->from;
	etmp->type = C_ATCK;
	etmp->atype = cv->atype | TYPE_U;
	etmp->aff = 0;
	etmp->hit = cv->hit;
	etmp->setmsg(f, "%THISP gets revenge on %TO");
	e->push_back(etmp);
	return 0;
}
MAKE_FPROC(RevengeFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_ITEM;
	etmp->setmsg(f, "%THISP played revenge");

	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_DAMAGE1, 0, PRIORITY_ITEM, RevengeHDamage1);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(ChocolateCowHTop)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_HEAL;
	etmp->hit = card->health;

	card->ldata--;
	if(card->ldata == 0)
	{
		etmp->setmsg(f, "%THISP finished the Chocolate Cow");
		e->push_back(etmp);
		DiscardCard(f, e, card);
	}
	else
	{
		etmp->setmsg(f, "%THISP ate more of the Chocolate Cow");
		e->push_back(etmp);
	}
	return 0;
}
MAKE_FPROC(ChocolateCowFunc)
{
	short to;

	GET_TARGET_SPEC(to, ChocolateCowFunc, TARGET_HEAL);

	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_HEAL;
	etmp->hit = card->health;
	etmp->setmsg(f, "%THISP ate part of a Chocolate Cow");

	MoveCardEvent(etmp, to, LOC_OTHR);

	card->ldata = 2; // Two additional turns
	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_TOP, 0, PRIORITY_SPECIAL, ChocolateCowHTop);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(FolderMazeHTop)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = E_FAKE;
	e->push_back(etmp); // Since Target needs a cv, and we may not have one during HOOK_TOP
	
	short curev = e->curev; // but just in case we do
	e->curev = (short)e->size() - 1;
	short to = TargetBlock(f, e, card, pnum, TARGET_NORMAL | TARGET_NOEP, "Choose someone to attack:", card);
	e->curev = curev;

	etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = to;
	etmp->type = C_ATCK;
	etmp->atype = TYPE_M | TYPE_U;
	etmp->aff = 0;
	etmp->hit = 1;
	etmp->setmsg(f, "%THISP attacked %TO with Folder maze");

	e->push_back(etmp);
	return 0;
}
MAKE_FPROC(FolderMazeFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = card->thisp;
	etmp->type = C_ITEM;
	etmp->setmsg(f, "%THISP played Folder maze");

	RegisterHookEvent(etmp, card->thisp, NULL, HOOK_TOP, 0, PRIORITY_ITEM, FolderMazeHTop);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

#pragma warning(pop)

void InitCards(deque<CARD> *d)
{
	unsigned long i;
	for(i = 0; i < mdecklen; i++)
	{
		d->push_back(mdeck[i]);
	}
}
