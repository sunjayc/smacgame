#include "..\CardCode.h"

#pragma warning(push)
#pragma warning(disable:4100)

MAKE_FPROC(LibraryFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = C_FFLD;
	etmp->setmsg(f, "%THISP played Library");

	RegisterHookEvent(etmp, -1, NULL, HOOK_SELECT, HK_ATYPE | TYPE_V, PRIORITY_FIELD, NegativeOne);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(CautionLowCeilingHSelect)
{
	if(ocard->aff & AFF_A)
		return 0;
	return -1;
}
MAKE_FPROC(CautionLowCeilingFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = C_FFLD;
	etmp->setmsg(f, "%THISP played Caution: low ceiling");

	RegisterHookEvent(etmp, -1, NULL, HOOK_SELECT, HK_AFF | AFF_M, PRIORITY_FIELD, CautionLowCeilingHSelect);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_HPROC(TagHTop)
{	
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = pnum;
	etmp->to = -1;
	etmp->type = E_FAKE;
	e->push_back(etmp); // Since Target needs a cv, and we may not have one during HOOK_TOP
	
	short curev = e->curev; // but just in case we do
	e->curev = (short)e->size() - 1;
	short to = TargetBlock(f, e, card, pnum, TARGET_NORMAL | TARGET_NOEP, "Choose someone to tag:", card);
	e->curev = curev;

	etmp = new EVENT;
	etmp->card = card;
	etmp->from = pnum;
	etmp->to = to;
	etmp->type = E_FAKE;
	etmp->setmsg(f, "tag, %TO is it");

	f->p[pnum]->hookreg.clearCard(card); // Clear out our hooks for this player

	RegisterHookEvent(etmp, to, NULL, HOOK_TARGET2, 0, PRIORITY_FIELD, ForceNotMe);
	RegisterHookEvent(etmp, to, NULL, HOOK_TOP, 0, PRIORITY_FIELD, TagHTop);

	e->push_back(etmp);
	return 0;
}
MAKE_HPROC(TagHSucc)
{
	card->hookreg.clearCard(card); // Clear the HOOK_SUCC hook
	return TagHTop(f, e, card, ocard, pnum, data);
}
MAKE_FPROC(TagFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = C_FFLD;
	etmp->setmsg(f, "%THISP played tag");
	e->push_back(etmp);

	card->hookreg.registerItem(card, HOOK_SUCC, 0, PRIORITY_FIELD, TagHSucc);

	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

MAKE_FPROC(DumpingAreaFunc)
{
	EVENT *etmp = new EVENT;
	etmp->card = card;
	etmp->from = card->thisp;
	etmp->to = -1;
	etmp->type = C_FFLD;
	etmp->setmsg(f, "%THISP played dumping area");

	RegisterHookEvent(etmp, -1, NULL, HOOK_SELECT, HK_ATYPE | TYPE_P, PRIORITY_FIELD, NegativeOne);

	e->push_back(etmp);
	MoveCard(f, card, -1, LOC_FLUX);
	return 0;
}

#pragma warning(pop)
