#include <tchar.h>
#include "CommFunc/CommFunc.h"
#include "Hlist.h"
#include "SMACgame.h"

// Event types
#define E_NACT				0x00000001 // Event cannot be acted upon (like heals issued from items to prevent milk steals)
#define E_FAKE				0x00010000 // "Fake" event; an event used only to generate other events or probe for stats e.g. PeanutButterSelectX
#define E_XSEL				0x00020000 // Event to signal Select( to call SelectX(
#define E_SLCT				0x00040000 // Event to signal call to Select(

// Use these to identify extra players
#define EP						0x4000 // Usage: EP | pindx (making this 0x8000 would mess with negative numbers)
#define PINDX(pindx) (pindx & ~EP)
#define ISREAL(pindx) (bool)!(pindx & EP)

// EPLAYER params
#define EPLAY_NONTARGET		0x0001 // This EPLAYER will not appear on any target list (not something directly attackable, like PeanutButter)
#define EPLAY_EMO			0x0002 // This EPLAYER is an emo enemy (health should be negative, dies when zero or positive)

#define NOTARGET			0xFFFF0000
#define TO(target) (short)(target & 0x0000FFFF)

// Event modifiers
enum EVMODTYPE
{
	evmod_add, 
	evmod_multiply
};

enum EVDONETYPE
{
	evdone_reg, 
	evdone_move, 
	evdone_eplay, 
	evdone_discard
};


typedef int (__cdecl *DPROC)(class ELIST *, struct CARD *);
typedef int (__cdecl *FPROC)(struct PLAYFIELD *, class ELIST *, struct CARD *, long);

class EPLAYER;
/**
 * Structure for server side cards.
 */
struct CARD
{
	char name[35]; ///< Name of card
	char desc[133]; ///< Description
	char flavtxt[67]; ///< Flavor text
	unsigned short aff; ///< SMAC! affiliation
	unsigned long type; ///< Card type(s)
	unsigned short id; ///< Card Identifier
	unsigned short atype; ///< Attack/Counter type
	short health; ///< Damage done by attack/counter OR Health healed by heal OR Starting health of enemies/allies

	// Above fields must match with CCARD

	DPROC draw; ///< Set of instructions on card being drawn
	FPROC func; ///< Set of instructions for card function
	char* custom_msg; ///< Optional custom message if using AutomagicFunc

	// Above fields must match with CardBuilder
	// Fields that don't need to be initialized on allocation start here

	long ldata; ///< For card to keep track of any card-specific data

	short indx; ///< Index of card in the master deck
	short thisp; ///< Current owner of the card
	unsigned long loc; ///< Current location

	HLIST hookreg; ///< For hooks to register into
	EPLAYER *ep; ///< If this card creates an EPLAYER, this should point to it
};

/**
 * Structure for numerical/algebraic modifications to EVENTs.
 */
struct EVMOD
{
	EVMODTYPE emodtype; ///< Type of modification
	short value; ///< Amount of the modification
	char msg[128]; ///< Message to be displayed when the modification is processed
};

/**
 * Structure for stuff to do on event success.
 */
struct EVDONE
{
	EVDONETYPE edonetype; ///< The type of the thing to do
	short target; ///< Player to register/move to, or -1
	CARD *targetcard; ///< Card to register to if target is -1 (if this is also null, register/move to playfield)

	// Parameters
	HOOK hook; ///< (reg)
	unsigned long lval; ///< Specific (reg) / Location (move) / Params [short] (eplay)
	short sval; ///< Priority (reg) / Health (eplay)
	HPROC hfunc; ///< (reg)
};

/**
 * Structure for holding an "event" to send between cards and the server.
 */
struct EVENT
{
	CARD *card; ///< Pointer to card calling event, or NULL

	short from; ///< If applicable, index of player that used the card, else -1
	short to; ///< If applicable, index of player card is being played on, else -1

	unsigned long type; ///< Type of card and/or event, else NONE (also, for XSELECT events, this sets 'specific' to counter if it is C_CNTR)
	unsigned short atype; ///< Type of attack if 'type' contains C_ATCK or C_CNTR, ignored otherwise
	unsigned short aff; ///< Affiliation of card and/or event, else NONE
	short hit; ///< Amount damage if 'type' is C_ATCK or C_CNTR, amount healed if 'type' is C_HEAL

	bool nulled; ///< True if this event has been nullified by some effect
	deque<EVMOD*> evmod; ///< Event modifiers attached to event
	deque<EVDONE*> evdone; ///< Stuff to do upon event success

	char msg[256]; ///< Message associated with event

	bool isprocessed; ///< True if processed
	bool ischained; ///< True if next event is chained to this event

	FPROC tproc; ///< func to call when player responds
	CARD *hcard; ///< card associated with the callback

	EVENT();
	void setmsg(PLAYFIELD *f, char* msg);
};

/**
 * A list of events for the current turn.
 * Events remain in the list after being processed, 
 * they are not removed until the next turn.
 */
class ELIST : public deque<EVENT*>
{
public:
	ELIST();
	void clear();

	short curev; ///< Index of event currently being acted upon
};

enum PLAYERTYPE
{
	bplayer, 
	player, 
	eplayer
};

class BPLAYER // Base class for server side player data holders
{
public:
	BPLAYER();

	/**
	 * Processes damage dealt to this player.
	 * Calls additional related hooks.
	 * 
	 * @param f the current playing field.
	 * @param e the current events list.
	 * @param hit the amount of damage to apply.
	 * @return true if the player dies, false otherwise.
	 */
	virtual bool damage(PLAYFIELD *f, ELIST *e, short hit) = 0;

	/**
	 * Processes healing done to this player.
	 * Calls additional related hooks.
	 * 
	 * @param f the current playing field.
	 * @param e the current events list.
	 * @param hit the amount of health to heal.
	 * @return true if the player dies, false otherwise.
	 */
	virtual bool heal(PLAYFIELD *f, ELIST *e, short hit) = 0;

	PLAYERTYPE ptype; ///< PLAYER or EPLAYER
	char name[35]; ///< Name to display
	short pnum; ///< This player's index number
	short health; ///< This player's health

	HLIST hookreg; ///< For hooks to register into
};

/**
 * Server side player data holder.
 * Stores all information related to a single player.
 */
class PLAYER : public BPLAYER
{
public:
	PLAYER();
	bool damage(PLAYFIELD *f, ELIST *e, short hit);
	bool heal(PLAYFIELD *f, ELIST *e, short hit);

	SOCKET psock; ///< Socket to player's game client
	SOCKADDR_IN paddr; ///< Player's game client's address

	deque<CARD*> hand; ///< This player's hand
	deque<CARD*> item; ///< Items in play
	short hmax; ///< Max number of cards in hand
	short imax; ///< Max number of items in play

	deque<CARD*> ally; ///< Allies attached to player
	deque<CARD*> enmy; ///< Enemies attached to player
	deque<CARD*> othr; ///< Anything else attached to player
};

/**
 * Server side data holder for "extra" players registered to a player.
 */
class EPLAYER : public BPLAYER
{
public:
	EPLAYER();
	bool damage(PLAYFIELD *f, ELIST *e, short hit);
	bool heal(PLAYFIELD *f, ELIST *e, short hit);

	short thisp; ///< Player that this extra player is attached to
	long params; ///< EPLAY_

	HPROC hfunc; ///< For the callback function if this extra player's card uses SelectX(

	CARD *card; ///< Card that this extra player is based off of
};

/**
 * Server side main playing field data.
 */
struct PLAYFIELD
{
	deque<CARD> d; ///< Master deck
	deque<CARD*> draw; ///< Cards in the draw pile
	deque<CARD*> discard; ///< Cards in the discard pile
	deque<CARD*> flux; ///< Cards in flux; everything here gets discarded at the end of each turn

	deque<PLAYER*> p; ///< Array for player information
	deque<EPLAYER*> ep; ///< Array for extra player information
	short nump; ///< Number of players
	short curp; ///< Current player

	deque<CARD*> field; ///< Current field card(s)
	deque<CARD*> prank; ///< Pranks in play

	short shealth; ///< Starting health
	short shmax; ///< Starting max number of cards in hand
	short simax; ///< Starting max number of items

	HLIST hookreg; ///< For hooks to register into
};
