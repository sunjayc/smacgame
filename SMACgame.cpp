#include "CommFunc/CommFunc.h"
#include <conio.h>
#include <tchar.h>
#include <crtdbg.h>
#include "SMACfunc.h"
#include <vld.h>

HANDLE hOldscr;
HANDLE hScr;
CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
CONSOLE_CURSOR_INFO cciInfo;
int lzf;


void	InitWindow(void);

extern int gameserv(void);
extern int gameclient(void);
extern int InitDbgH(int nump);
extern int InitDbgC(int nump);
extern void LazyFailH(char* errmsg);
extern void LazyFailC(char* errmsg);

int hostdebugcards(void);

int _tmain(int argc, char** __targv)
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	int c;
	int r;
	int i;

	InitWindow();

	if(argc > 1)
	{
		if(strcmp(__targv[1], "DEBUGC") == 0)
		{
			return InitDbgC(0);
		}
		else if(strcmp(__targv[1], "DEBUGH") == 0)
		{
			return InitDbgH(0);
		}
	}

	cwrite("Welcome to SMACgame!", 0, 0);
	cwrite("  1. Start server", 0, 1);
	cwrite("  2. Start client", 0, 2);

	c = menu(1, 5);
	cwrite("", 0, 0);
	cwrite("", 0, 1);
	cwrite("", 0, 2);
	switch(c)
	{
		case 0:
		{
			r = gameserv();
			break;
		}
		case 1:
		{
			r = gameclient();
			break;
		}
		case 2:
		{
			cwrite("Number of players?", 0, 4);
			r = iget(5);
			for(i = 0; i < r; i++)
			{
				ShellExecute(NULL, "open", "SMACgame.exe", "DEBUGC", NULL, SW_SHOWNA);
			}
			r = InitDbgH(r);
			break;
		}
		case 3:
		{
			cwrite("Number of players?", 0, 4);
			r = iget(5);
			ShellExecute(NULL, "open", "SMACgame.exe", "DEBUGH", NULL, SW_SHOWNA);
			r = InitDbgC(r);
			break;
		}
		case 4:
		{
			r = hostdebugcards();
			break;
		}
		default:
			r = -1;
			break;
	}

	SetConsoleActiveScreenBuffer(hOldscr);
	return r;
}

void InitWindow(void)
{
	hOldscr = GetStdHandle(STD_OUTPUT_HANDLE);
	hScr = CreateConsoleScreenBuffer(
		GENERIC_READ |           // read/write access
		GENERIC_WRITE,
		0,                       // not shared
		NULL,                    // default security attributes
		CONSOLE_TEXTMODE_BUFFER, // must be TEXTMODE
		NULL);                   // reserved; must be NULL

	csbiInfo.dwSize.X = 80 + 52;
	csbiInfo.dwSize.Y = HEIGHT;
	csbiInfo.dwCursorPosition.X = 0;
	csbiInfo.dwCursorPosition.Y = 0;
	csbiInfo.wAttributes = 7;
	csbiInfo.srWindow.Left = 0;
	csbiInfo.srWindow.Top = 0;
	csbiInfo.srWindow.Right = 79 + 52;
	csbiInfo.srWindow.Bottom = HEIGHT - 1;
	csbiInfo.dwMaximumWindowSize.X = 80 + 52;
	csbiInfo.dwMaximumWindowSize.Y = HEIGHT;
	cciInfo.bVisible = FALSE;
	cciInfo.dwSize = HEIGHT;

	SetConsoleScreenBufferSize(hScr, csbiInfo.dwSize);
	SetConsoleCursorPosition(hScr, csbiInfo.dwCursorPosition);
	SetConsoleTextAttribute(hScr, csbiInfo.wAttributes);
	SetConsoleWindowInfo(hScr, TRUE, &csbiInfo.srWindow);
	SetConsoleCursorInfo(hScr, &cciInfo);

	SetConsoleActiveScreenBuffer(hScr);

	return;
}

void LazyFail(char* errmsg)
{
	if(lzf == 0)
		LazyFailH(errmsg);
	else
		LazyFailC(errmsg);
}













unsigned long cwrite(char* str, int x, int y, WORD attr, int clrlength)
{
	COORD coord;
	unsigned long lnum = 0;
	int len = strlen(str);

	coord.X = (short)x;
	coord.Y = (short)y;
	FillConsoleOutputCharacter(hScr, ' ', clrlength, coord, &lnum);
	WriteConsoleOutputCharacter(hScr, str, len, coord, &lnum);
	FillConsoleOutputAttribute(hScr, attr, len, coord, &lnum);

	return lnum;
}

int kget(void)
{
	int c = _getch();

	return c;
}

void clearkbd()
{
	while(_kbhit() != 0) // Clear the keyboard buffer
		_getch();
}

int iget(int scrindex, int x, WORD attr)
{
	char buf[32];
	int i = 0;
	int c = 0;
	int result;

	clearkbd();

	buf[0] = '\0';
	while(c != 13)
	{
		c = kget();
		if(c >= '0' && c <= '9')
		{
			buf[i++] = (char)c;
			buf[i] = '\0';
			cwrite(buf, x, scrindex, attr);
		}
		else if(c == 8 && i > 0)
		{
			buf[--i] = 0;
			cwrite(buf, x, scrindex, attr);
		}
	}
	result = atoi(buf);

	return result;
}

int lget(int scrindex, char buf[], int len)
{
	int i, c;

	if(len <= 0)
		return -1;

	clearkbd();

	buf[0] = '\0'; // "Blank out" buf
	i = 0;
	for(;;)
	{
		c = kget();
		if(c != 13)
		{
			if(i < (len - 1) && c != 8)
			{
				buf[i++] = (char)c;
				buf[i] = 0;
			}
			else if(c == 8 && i > 0)
				buf[--i] = 0;
			cwrite(buf, 0, scrindex);
		}
		else
			break;
	}

	return i;
}

bool bget(int scrindex)
{
	char buf[2];
	int c = 0;

	clearkbd();

	while(c != 13)
	{
		c = kget();
		if(c >= '0' && c <= '1')
		{
			buf[0] = (char)c;
			buf[1] = '\0';
			cwrite(buf, 0, scrindex);
		}
	}
	return (c == '1');
}

int menu(int scrindex, int max, int defchoice)
{
	COORD coord;
	unsigned long lnum = 0;
	int choice, prevchoice = defchoice; // Default choice
	bool flag = true;
	char arrow[1];

	clearkbd();

	for(int i = 0; i < max; i++)
	{
		coord.X = 0;
		coord.Y = (short)(scrindex + i);
		arrow[0] = (i == defchoice ? '>' : ' ');
		WriteConsoleOutputCharacter(hScr, arrow, 1, coord, &lnum);
	}

	do
	{
		do
		{
			choice = kget();
			if(choice == 224) // Arrow key
			{
				choice = kget();
				if(choice == 72 && prevchoice > 0) // Up
					choice = prevchoice - 1;
				else if(choice == 80 && prevchoice < (max - 1)) // Down
					choice = prevchoice + 1;
			}
			else if(choice == 13)
			{
				flag = false;
				break;
			}
			else
				choice -= '1'; // 0-based index
		} while(choice < 0 || choice >= max); // Make sure it's within proper range
		if(flag == false)
			break;

		if(choice != prevchoice)
		{
			coord.X = 0;
			coord.Y = (short)(scrindex + prevchoice);
			arrow[0] = ' ';
			WriteConsoleOutputCharacter(hScr, arrow, 1, coord, &lnum);

			coord.X = 0;
			coord.Y = (short)(scrindex + choice);
			arrow[0] = '>';
			WriteConsoleOutputCharacter(hScr, arrow, 1, coord, &lnum);
			prevchoice = choice;
		}
	} while(flag);

	return prevchoice; // Since choice right now equals \n
}