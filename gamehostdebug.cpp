#include <conio.h>
#include <list>
#include "SMAChostdebug.h"
#include "SMAChelpers.h"
#include "interface.h"

extern HANDLE hScr;
extern short dbgtarget;

// Globals
int hinfoboxchoice; // Currently selected infobox (0 = main window  1 = player window  2 = field window)

/**
 * A structure for the layout of one of the three main infoboxes.
 * Currently shares a lot of overlap with INFOBOX.
 */
struct HOSTINFOBOX
{
	WORD* normAttrib; ///< Unselected colors
	WORD* selAttrib; ///< Selected colors
	COORD size; ///< Size in the screen buffer
	COORD pos; ///< Position in the screen buffer
	SMALL_RECT sm; ///< Also the size and position in the screen buffer (I think)
	int sz; ///< Number of total WORDs in normAttrib and selAttrib
	int oldheight; ///< For playerw and fieldw so they know how much to clear when their lists decrease

	short choice; ///< Currently selected card or menu item
	short xchoice; ///< Currently selected player in player infobox OR currently selected submenu in mainw
	short maxval; ///< Number of cards / menu items in infobox
	CARD *curc; ///< Current card pointed to, once again can also be used for the menus _when_ I get around to it
};
HOSTINFOBOX hib[3];

// Ease-of-access #defines
#define mainhw hib[0]
#define playerhw hib[1]
#define fieldhw hib[2]
#define curhib hib[hinfoboxchoice]

// Functions
int				hostdebug(PLAYFIELD *f, ELIST *e);
int				hostdebugsetup();
int				hostinterface(PLAYFIELD *f, ELIST *e);
void			hostchangeib(PLAYFIELD *f);
void			hostarrow(PLAYFIELD *f, int ibnum = -1);
unsigned long	hostswrite(char* str, WORD attr = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
int				hostcmenu(PLAYFIELD *f, deque<CARD*> *pile = NULL);
int				hostfield(PLAYFIELD *f);
bool			card_alphabetical(pair<char*, short> p1, pair<char*, short> p2);

int hostdebugcards()
{
	PLAYFIELD f;
	InitCards(&f.d);
	hostdebugsetup();
	hostcmenu(&f);
	delete [] mainhw.normAttrib;
	delete [] mainhw.selAttrib;

	return 0;
}

int hostdebug(PLAYFIELD *f, ELIST *e)
{
	hostdebugsetup();

	hostfield(f);
	hostinterface(f, e);

	return 0;
}

int hostdebugsetup()
{
	hinfoboxchoice = 1;

	mainhw.normAttrib = NULL;
	mainhw.selAttrib = NULL;
	mainhw.choice = 0;
	mainhw.xchoice = 0;
	mainhw.curc = NULL;
	playerhw.normAttrib = NULL;
	playerhw.selAttrib = NULL;
	playerhw.choice = 0;
	playerhw.xchoice = 0;
	playerhw.oldheight = 0;
	fieldhw.normAttrib = NULL;
	fieldhw.selAttrib = NULL;
	fieldhw.choice = 0;
	fieldhw.oldheight = 0;

	mainhw.sz = MAINWIDTH * HEIGHT;
	mainhw.normAttrib = new WORD[mainhw.sz];
	mainhw.selAttrib = new WORD[mainhw.sz];
	mainhw.size.X = MAINWIDTH;
	mainhw.size.Y = HEIGHT;
	mainhw.pos.X = 0;
	mainhw.pos.Y = 0;
	mainhw.sm.Top = 0;
	mainhw.sm.Left = 0;
	mainhw.sm.Bottom = HEIGHT - 1;
	mainhw.sm.Right = 41;

	return 0;
}

int hostinterface(PLAYFIELD *f, ELIST *e)
{
	int i;
	int c;
	static int choice;

	for(;;)
	{
		if(_kbhit() != 0)
		{
			c = kget();
			switch(c)
			{
				case 27: // Esc key
				{
					return -1;
				}
				case 9: // Tab key
				{
					hostchangeib(f);
					break;
				}
				case 13: // Enter key
				{
					// dd;
					break;
				}
				case 'C': // Starting hand size (all players)
				{
					cwrite("Cards: ", 0, 1, 7 | FOREGROUND_INTENSITY);
					f->shmax = (short)iget(1, 7, 7 | FOREGROUND_INTENSITY);
					for(unsigned int pi = 0; pi < f->p.size(); pi++)
					{
						f->p[pi]->hmax = f->shmax;
					}
					hostfield(f);
					break;
				}
				case 'd': // Draw (current player)
				{
					if(hinfoboxchoice == 1)
					{
						int choice = hostcmenu(f, &f->draw);
						if(choice >= 0)
						{
							CARD *card = f->draw[choice];
							MoveCard(f, card, playerhw.xchoice, LOC_HAND);
							card->draw(e, card);
						}
						hostfield(f);
					}
					break;
				}
				case 'D': // Draw remaining hand randomly (current player)
				{
					if(hinfoboxchoice == 1)
					{
						DrawHand(f, e, playerhw.xchoice);
						hostfield(f);
					}
					break;
				}
				case 'e': // Enemy (current player)
				{
					if(hinfoboxchoice == 1)
					{
						deque<CARD*> enemies;
						for(i = 0; i < (signed)f->d.size(); i++)
						{
							if(f->d[i].type & C_ENMY)
								enemies.push_back(&f->d[i]);
						}
						int choice = hostcmenu(f, &enemies);
						if(choice >= 0)
						{
							CARD *card = enemies[choice];
							dbgtarget = playerhw.xchoice;
							card->func(f, e, card, NOTARGET);
							MoveCard(f, card, playerhw.xchoice, LOC_ENMY);
							dbgtarget = -1;
						}
						hostfield(f);
					}
					break;
				}
				case 'H': // Starting health (all players)
				{
					cwrite("Health: ", 0, 0, 7 | FOREGROUND_INTENSITY);
					f->shealth = (short)iget(0, 8, 7 | FOREGROUND_INTENSITY);
					for(unsigned int pi = 0; pi < f->p.size(); pi++)
					{
						f->p[pi]->health = f->shealth;
					}
					SendInfo(f, -1, INFO_HLTH);
					hostfield(f);
					break;
				}
				case 'i': // Item (current player)
				{
					if(hinfoboxchoice == 1)
					{
						deque<CARD*> items;
						for(i = 0; i < (signed)f->d.size(); i++)
						{
							if(f->d[i].type & C_ITEM)
								items.push_back(&f->d[i]);
						}
						int choice = hostcmenu(f, &items);
						if(choice >= 0)
						{
							CARD *card = items[choice];
							MoveCard(f, card, playerhw.xchoice, LOC_HAND);
							card->draw(e, card);
							card->func(f, e, card, NOTARGET);
							MoveCard(f, card, playerhw.xchoice, LOC_ITEM);
						}
						hostfield(f);
					}
					break;
				}
				case 224: // Arrow key
				{
					c = kget(); // To get the second part of the keycode
					if(c == 72 && curhib.choice > 0) // Up
					{
						curhib.choice--;
					}
					else if(c == 80 && curhib.choice < (curhib.maxval - 1)) // Down
					{
						curhib.choice++;
					}
					else if(hinfoboxchoice == 1) // Player infobox
					{
						if(c == 75 && playerhw.xchoice > 0) // Left
						{
							playerhw.xchoice--;
							hostfield(f);
						}
						else if(c == 77 && playerhw.xchoice < (f->nump - 1)) // Right
						{
							playerhw.xchoice++;
							hostfield(f);
						}
					}
					hostarrow(f);
				}
			}
		}
	}
}

void hostchangeib(PLAYFIELD *f)
{
	COORD coord;
	short i;
	unsigned long linfo;

	coord.X = curhib.sm.Left;
	for(i = 0; i < curhib.size.Y; i++)
	{
		coord.Y = i + curhib.sm.Top;
		WriteConsoleOutputAttribute(hScr, curhib.normAttrib + (i * (curhib.size.X)), curhib.size.X, coord, &linfo);
	}

	if(hinfoboxchoice == 2)
		hinfoboxchoice = 1;
	else
		hinfoboxchoice++;

	coord.X = curhib.sm.Left;
	for(i = 0; i < curhib.size.Y; i++)
	{
		coord.Y = i + curhib.sm.Top;
		WriteConsoleOutputAttribute(hScr, curhib.selAttrib + (i * (curhib.size.X)), curhib.size.X, coord, &linfo);
	}

	hostarrow(f); // This also takes care of calling hostcard()
}

void hostarrow(PLAYFIELD *f, int ibnum)
{
	int i, j;
	deque<CARD*> *pile = NULL;

	if(ibnum == -1)
		ibnum = hinfoboxchoice;
	if(ibnum == 1) // Player Infobox
	{
		int hn, in, an, en, ncards;
		i = playerhw.xchoice;
		hn = f->p[i]->hand.size();
		in = hn + f->p[i]->item.size();
		an = in + f->p[i]->ally.size();
		en = an + f->p[i]->enmy.size();
		ncards = en + f->p[i]->othr.size();
		// No need for an on

		for(i = 0; i < playerhw.size.Y; i++)
			cwrite("", 80 + playerhw.xchoice * 4, i, FOREGROUND_INTENSITY, 1);

		if(ncards < 0)
			return;
		else if(ncards == 0)
		{
			cwrite(">", 80 + playerhw.xchoice * 4, 2, FOREGROUND_INTENSITY | (hinfoboxchoice == 1 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);
			playerhw.curc = NULL;
			card(NULL);
			return;
		}

		if(playerhw.choice >= playerhw.maxval)
			playerhw.choice = playerhw.maxval - 1;

		i = playerhw.choice;
		j = i;
		if(hn != 0 && i >= hn)
			j++;
		if(in != hn && i >= in)
			j++;				// For these, if i changes, then the if statements get messed up
		if(an != in && i >= an)
			j++;
		if(en != an && i >= en)
			j++;
		cwrite(">", 80 + playerhw.xchoice * 4, j + 2, FOREGROUND_INTENSITY | (hinfoboxchoice == 1 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);

		i = playerhw.choice;
		if(i >= en)
		{
			i -= en;
			pile = &f->p[playerhw.xchoice]->othr;
		}
		else if(i >= an)
		{
			i -= an;
			pile = &f->p[playerhw.xchoice]->enmy;
		}
		else if(i >= in)
		{
			i -= in;
			pile = &f->p[playerhw.xchoice]->ally;
		}
		else if(i >= hn)
		{
			i -= hn;
			pile = &f->p[playerhw.xchoice]->item;
		}
		else
			pile = &f->p[playerhw.xchoice]->hand;
		playerhw.curc = (*pile)[i];
		if(hinfoboxchoice == 1)
			card(playerhw.curc);
	}
	else if(ibnum == 2) // Field Infobox
	{
		int fn, ncards;
		fn = f->field.size();
		ncards = fn + f->prank.size();

		for(i = 0; i < fieldhw.size.Y; i++)
			cwrite("", 80, fieldhw.sm.Top + i, FOREGROUND_INTENSITY, 1);

		if(ncards < 0)
			return;
		else if(ncards == 0)
		{
			cwrite(">", 80, fieldhw.sm.Top + 1, FOREGROUND_INTENSITY | (hinfoboxchoice == 2 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);
			fieldhw.curc = NULL;
			card(NULL);
			return;
		}

		if(fieldhw.choice >= fieldhw.maxval)
			fieldhw.choice = fieldhw.maxval - 1;

		i = fieldhw.choice;
		if(fn != 0 && i >= fn)
			i++;
		cwrite(">", 80, fieldhw.sm.Top + i + 1, FOREGROUND_INTENSITY | (hinfoboxchoice == 2 ? FOREGROUND_GREEN | FOREGROUND_BLUE : 0), 1);

		i = fieldhw.choice;
		pile = &f->field;
		if(i >= fn)
		{
			i -= fn;
			pile = &f->prank;
		}
		fieldhw.curc = (*pile)[i];
		if(hinfoboxchoice == 2)
			card(fieldhw.curc);
	}
}






unsigned long hostswrite(char* str, WORD attr)
{
	int i, j;
	short val;
	COORD dest;
	CHAR_INFO cInfo;
	int len = strlen(str);
	short numlines = (short)((len - 1) / MAINWIDTH) + 1; // 1-MAINWIDTH -> 1; 43-84 -> 2; etc. (0 -> 1)
	char buf[MAINWIDTH + 1]; //                     The width of the message area is MAINWIDTH characters
	short scrndex;
	SMALL_RECT sm = mainhw.sm;

	if(strcmp(str, "\n") == 0)
	{
		for(i = 0; i < HEIGHT; i++)
		{
			cwrite("", 0, i);
		}
		return 0;
	}

	dest.X = 0;
	dest.Y = -numlines;
	cInfo.Attributes = 0;
	cInfo.Char.AsciiChar = ' ';
	ScrollConsoleScreenBuffer(hScr, &sm, &sm, dest, &cInfo);
	scrndex = HEIGHT - numlines;
	
	val = scrndex;
	for(j = 0; j < numlines; j++)
	{
		strncpy_s(buf, MAINWIDTH + 1, str + (MAINWIDTH * j), MAINWIDTH);
		cwrite(buf, 0, scrndex, attr);
		scrndex++;
	}

	return val;
}

int hostcmenu(PLAYFIELD *f, deque<CARD*> *pile)
{
	COORD coord;
	unsigned long lnum = 0;
	int choice, prevchoice = 0; // Default choice
	bool flag = true;
	char buf[81];
	int i;
	int max;
	char namesel[35] = "";
	list<pair<char*, short>> namelist, tmplist;
	list<pair<char*, short>>::iterator listchoice;
	int len = 0;
	bool usepile = (pile != NULL);
	COORD dest;
	CHAR_INFO cInfo;
	SMALL_RECT sm = mainhw.sm;
	
	hostswrite("\n");

	if(usepile == true)
	{
		max = pile->size();
		for(i = 0; i < max; i++)
		{
			namelist.push_back(make_pair((*pile)[i]->name, i));
		}
		namelist.sort(card_alphabetical);
		for(i = 0, listchoice = namelist.begin(); listchoice != namelist.end(); listchoice++, i++)
		{
			tmplist.push_back(make_pair(listchoice->first, listchoice->second));
			if(i < HEIGHT)
			{
				sprintf_s(buf, 81, " %3d. %s", listchoice->second + 1, listchoice->first);
				cwrite(buf, 0, i);
			}
		}
	}
	else
	{
		max = f->d.size();
		for(i = 0; i < max; i++)
		{
			namelist.push_back(make_pair(f->d[i].name, i));
			tmplist.push_back(make_pair(f->d[i].name, i));
			if(i < HEIGHT)
			{
				sprintf_s(buf, 81, " %3d. %s", namelist.back().second + 1, namelist.back().first);
				cwrite(buf, 0, i);
			}
		}
	}

	clearkbd();

	for(int i = 0; i < max; i++)
	{
		coord.X = 0;
		coord.Y = (short)i;
		buf[0] = (i == 0 ? '>' : ' ');
		WriteConsoleOutputCharacter(hScr, buf, 1, coord, &lnum);
	}
	card(usepile ? (*pile)[namelist.front().second] : &f->d[namelist.front().second]);

	listchoice = tmplist.begin();
	do
	{
		choice = tolower(kget());
		if(choice == 224) // hostarrow key
		{
			choice = kget();
			if(!tmplist.empty())
			{
				if(choice == 72 && listchoice != tmplist.begin()) // Up
				{
					listchoice--;
					if(prevchoice == 0)
					{
						dest.X = 0;
						dest.Y = 1;
						cInfo.Attributes = 0;
						cInfo.Char.AsciiChar = ' ';
						ScrollConsoleScreenBuffer(hScr, &sm, &sm, dest, &cInfo);
						sprintf_s(buf, 81, " %3d. %s", listchoice->second + 1, listchoice->first);
						cwrite(buf, 0, 0);
						choice = prevchoice;
						prevchoice = choice + 1;
					}
					else
						choice = prevchoice - 1;
				}
				else if(choice == 80) // Down
				{
					listchoice++;
					if(listchoice == tmplist.end())
					{
						listchoice--;
						choice = prevchoice;
					}
					else if(prevchoice == HEIGHT - 1)
					{
						
						dest.X = 0;
						dest.Y = -1;
						cInfo.Attributes = 0;
						cInfo.Char.AsciiChar = ' ';
						ScrollConsoleScreenBuffer(hScr, &sm, &sm, dest, &cInfo);
						sprintf_s(buf, 81, " %3d. %s", listchoice->second + 1, listchoice->first);
						cwrite(buf, 0, HEIGHT - 1);
						choice = prevchoice;
						prevchoice = choice - 1;
					}
					else
						choice = prevchoice + 1;
				}
				else
					choice = prevchoice;
			}
			else
				choice = prevchoice;
		}
		else if(choice == 27) // Esc
		{
			hostswrite("\n");
			card(NULL);
			return -1;
		}
		else if(choice == 13) // Enter
		{
			if(!tmplist.empty())
				flag = false;
			choice = prevchoice;
		}
		else if(choice == 8) // Backspace
		{
			if(len > 0)
			{
				len--;
				namesel[len] = '\0';
				tmplist.clear();
				hostswrite("\n");
				list<pair<char*, short>>::iterator iter;
				iter = namelist.begin();
				i = 0;
				while(iter != namelist.end())
				{
					if(_strnicmp(namesel, iter->first, len) == 0)
					{
						sprintf_s(buf, 81, " %3d. %s", iter->second + 1, iter->first);
						cwrite(buf, 0, i++);
						tmplist.push_back(*iter);
					}
					iter++;
				}
				choice = 0;
				listchoice = tmplist.begin();
				for(int i = 0; i < (signed)tmplist.size(); i++)
				{
					coord.X = 0;
					coord.Y = (short)i;
					buf[0] = (i == 0 ? '>' : ' ');
					WriteConsoleOutputCharacter(hScr, buf, 1, coord, &lnum);
				}
				if(!tmplist.empty())
					card(usepile ? (*pile)[listchoice->second] : &f->d[listchoice->second]);
			}
			else
				choice = prevchoice;
		}
		else
		{
			namesel[len] = (char)choice;
			len++;
			hostswrite("\n");
			list<pair<char*, short>>::iterator iter;
			iter = tmplist.begin();
			i = 0;
			while(iter != tmplist.end())
			{
				if(_strnicmp(namesel, iter->first, len) != 0)
					iter = tmplist.erase(iter);
				else
				{
					sprintf_s(buf, 81, " %3d. %s", iter->second + 1, iter->first);
					cwrite(buf, 0, i++);
					iter++;
				}
			}
			choice = 0;
			listchoice = tmplist.begin();
			for(int i = 0; i < (signed)tmplist.size(); i++)
			{
				coord.X = 0;
				coord.Y = (short)i;
				buf[0] = (i == 0 ? '>' : ' ');
				WriteConsoleOutputCharacter(hScr, buf, 1, coord, &lnum);
			}
			if(!tmplist.empty())
				card(usepile ? (*pile)[listchoice->second] : &f->d[listchoice->second]);
		}

		if(choice != prevchoice)
		{
			coord.X = 0;
			coord.Y = (short)prevchoice;
			buf[0] = ' ';
			WriteConsoleOutputCharacter(hScr, buf, 1, coord, &lnum);

			coord.X = 0;
			coord.Y = (short)choice;
			buf[0] = '>';
			WriteConsoleOutputCharacter(hScr, buf, 1, coord, &lnum);
			prevchoice = choice;
			if(!tmplist.empty())
				card(usepile ? (*pile)[listchoice->second] : &f->d[listchoice->second]);
		}
	} while(flag);

	hostswrite("\n");
	card(NULL); // Clear the card display
	return listchoice->second;
}

int hostfield(PLAYFIELD *f)
{
	CHAR_INFO* ci; // contains final text block for output
	char*** cd; // contains an array (by player) of each player's text block
	int i, j, k, l;
	int length = 0;
	int maxl = 0;
	int maxw = 0;
	char pad[37] = "                                    ";
	char buf[43];

	sprintf_s(buf, 43, "Health: %d", f->shealth);
	cwrite(buf, 0, 0);
	sprintf_s(buf, 43, "Cards: %d", f->shmax);
	cwrite(buf, 0, 1);

	cd = new char**[f->nump];

	for(i = 0; i < f->nump; i++)
	{
		length = 0;
		if(!f->p[i]->hand.empty())
			length += f->p[i]->hand.size() + 1;
		if(!f->p[i]->item.empty())
			length += f->p[i]->item.size() + 1;
		if(!f->p[i]->ally.empty())
			length += f->p[i]->ally.size() + 1;
		if(!f->p[i]->enmy.empty())
			length += f->p[i]->enmy.size() + 1;
		if(!f->p[i]->othr.empty())
			length += f->p[i]->othr.size() + 1;
		length += 2; // For the borders
		if(length > maxl)
			maxl = length;
	}
	if(maxl == 2) // Empty
		maxl += 2; // To make the "empty" box

	for(i = 0; i < (signed)f->p[playerhw.xchoice]->hand.size(); i++)
	{
		length = strlen(f->p[playerhw.xchoice]->hand[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)f->p[playerhw.xchoice]->item.size(); i++)
	{
		length = strlen(f->p[playerhw.xchoice]->item[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)f->p[playerhw.xchoice]->ally.size(); i++)
	{
		length = strlen(f->p[playerhw.xchoice]->ally[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)f->p[playerhw.xchoice]->enmy.size(); i++)
	{
		length = strlen(f->p[playerhw.xchoice]->enmy[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)f->p[playerhw.xchoice]->othr.size(); i++)
	{
		length = strlen(f->p[playerhw.xchoice]->othr[i]->name);
		if(length > maxw)
			maxw = length;
	}

	maxw += 5;
	pad[maxw - 4] = '\0';

	for(i = 0; i < f->nump; i++)
	{
		cd[i] = new char*[maxl];
		for(j = 0; j < maxl; j++)
		{
			if(i == playerhw.xchoice)
			{
				cd[i][j] = new char[maxw + 1];
				cd[i][j][maxw] = '\0';
				_strnset_s(cd[i][j], maxw + 1, ' ', maxw);
			}
			else
			{
				cd[i][j] = new char[5];
				cd[i][j][4] = '\0';
				_strnset_s(cd[i][j], 5, ' ', 4);
			}
		}

		sprintf_s(cd[i][0], 5, " P %d", i + 1);
		strcpy_s(cd[i][1], 5, " �Ŀ");
		if(i == playerhw.xchoice)
		{
			strcat_s(cd[i][0], maxw + 1, pad);
			strcat_s(cd[i][1], maxw + 1, pad);
		}
		j = 2;
		if(!f->p[i]->hand.empty())
		{
			for(k = 0; k < (signed)f->p[i]->hand.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  H ");
				if(i == playerhw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", f->p[i]->hand[k]->name, maxw - 5 - strlen(f->p[i]->hand[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerhw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!f->p[i]->item.empty())
		{
			for(k = 0; k < (signed)f->p[i]->item.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  I ");
				if(i == playerhw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", f->p[i]->item[k]->name, maxw - 5 - strlen(f->p[i]->item[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerhw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!f->p[i]->ally.empty())
		{
			for(k = 0; k < (signed)f->p[i]->ally.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  A ");
				if(i == playerhw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", f->p[i]->ally[k]->name, maxw - 5 - strlen(f->p[i]->ally[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerhw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!f->p[i]->enmy.empty())
		{
			for(k = 0; k < (signed)f->p[i]->enmy.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  E ");
				if(i == playerhw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", f->p[i]->enmy[k]->name, maxw - 5 - strlen(f->p[i]->enmy[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerhw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(!f->p[i]->othr.empty())
		{
			for(k = 0; k < (signed)f->p[i]->othr.size(); k++)
			{
				strcpy_s(cd[i][j], 5, "  O ");
				if(i == playerhw.xchoice)
					sprintf_s(cd[i][j] + 4, maxw - 3, " %s%.*s", f->p[i]->othr[k]->name, maxw - 5 - strlen(f->p[i]->othr[k]->name), pad);
				j++;
			}
			strcpy_s(cd[i][j], 5, "    ");
			if(i == playerhw.xchoice)
				strcat_s(cd[i][j], maxw + 1, pad);
			j++;
		}
		if(j == 2) // Empty
			j += 2;
		strcpy_s(cd[i][--j], 5, " ���");
		if(i == playerhw.xchoice)
			strcat_s(cd[i][j], maxw + 1, pad);
	}

	playerhw.oldheight = playerhw.size.Y;
	playerhw.size.X = f->nump * 4 + (short)maxw - 4;
	playerhw.size.Y = (short)maxl;
	playerhw.pos.X = 0;
	playerhw.pos.Y = 0;
	playerhw.sm.Top = 0;
	playerhw.sm.Left = 80;
	playerhw.sm.Bottom = (short)maxl;
	playerhw.sm.Right = 80 + f->nump * 4 + (short)maxw - 4;

	l = 0;
	if(playerhw.normAttrib != NULL)
		delete [] playerhw.normAttrib;
	if(playerhw.selAttrib != NULL)
		delete [] playerhw.selAttrib;
	playerhw.sz = maxl * (f->nump * 4 + maxw - 4);
	ci = new CHAR_INFO[playerhw.sz];
	playerhw.normAttrib = new WORD[playerhw.sz];
	playerhw.selAttrib = new WORD[playerhw.sz];
	for(j = 0; j < maxl; j++) // Yes, this is correct; j should be in the outer loop
	{
		for(i = 0; i < f->nump; i++)
		{
			for(k = 0; k < (i == playerhw.xchoice ? maxw : 4); k++)
			{
				ci[l].Char.AsciiChar = cd[i][j][k];
				playerhw.normAttrib[l] = FOREGROUND_INTENSITY;
				playerhw.selAttrib[l] = 7 | (i == playerhw.xchoice ? FOREGROUND_INTENSITY : 0);
				ci[l].Attributes = (hinfoboxchoice == 1 ? playerhw.selAttrib[l] : playerhw.normAttrib[l]);
				l++;
			}
		}
	}
	for(i = 0; i < f->nump; i++)
	{
		for(j = 0; j < maxl; j++)
		{
			delete [] cd[i][j];
		}
		delete [] cd[i];
	}
	delete [] cd;

	for(i = 0; i < maxl; i++)
	{
		cwrite("", 80, i, 0, 52);
	}

	for(i = playerhw.oldheight - 1; i >= playerhw.size.Y; i--)
	{
		cwrite("", 80, i, 0, 52);
	}
	WriteConsoleOutputA(hScr, ci, playerhw.size, playerhw.pos, &playerhw.sm);
	delete [] ci;

	playerhw.maxval = (short)(f->p[playerhw.xchoice]->hand.size() + 
						f->p[playerhw.xchoice]->item.size() + 
						f->p[playerhw.xchoice]->ally.size() + 
						f->p[playerhw.xchoice]->enmy.size() + 
						f->p[playerhw.xchoice]->othr.size());
	hostarrow(f, 1); // Set the arrow for the player infobox




	maxl = f->field.size() + f->prank.size() + 3;
	if((f->field.size() == 0 || f->prank.size() == 0) && !((f->field.size() == 0 && f->prank.size() == 0)))
		maxl -= 1; // Since we won't need a dividing space (if both are zero, we still need a blank space for an empty box)
	char** cfd = new char*[maxl];
	maxw = 0;
	for(i = 0; i < (signed)f->field.size(); i++)
	{
		length = strlen(f->field[i]->name);
		if(length > maxw)
			maxw = length;
	}
	for(i = 0; i < (signed)f->prank.size(); i++)
	{
		length = strlen(f->prank[i]->name);
		if(length > maxw)
			maxw = length;
	}

	maxw += 5; // For the box itself
	pad[maxw - 4] = '\0';

	for(i = 0; i < maxl; i++)
		cfd[i] = new char[maxw + 1];

	strcpy_s(cfd[0], 5, " �Ŀ");
	strcat_s(cfd[0], maxw + 1, pad);
	strcpy_s(cfd[1], 5, "    ");
	strcat_s(cfd[1], maxw + 1, pad);
	j = 1;
	if(!f->field.empty())
	{
		for(k = 0; k < (signed)f->field.size(); k++)
		{
			strcpy_s(cfd[j], 5, "  F ");
			sprintf_s(cfd[j] + 4, maxw - 3, " %s%.*s", f->field[k]->name, maxw - 5 - strlen(f->field[k]->name), pad);
			j++;
		}
		strcpy_s(cfd[j], 5, "    ");
		strcat_s(cfd[j], maxw + 1, pad);
		j++;
	}
	if(!f->prank.empty())
	{
		for(k = 0; k < (signed)f->prank.size(); k++)
		{
			strcpy_s(cfd[j], 5, "  P ");
			sprintf_s(cfd[j] + 4, maxw - 3, " %s%.*s", f->prank[k]->name, maxw - 5 - strlen(f->prank[k]->name), pad);
			j++;
		}
		strcpy_s(cfd[j], 5, "    ");
		strcat_s(cfd[j], maxw + 1, pad);
		j++;
	}
	if(j == 1) // Empty
		j += 2;
	strcpy_s(cfd[--j], 5, " ���");
	strcat_s(cfd[j], maxw + 1, pad);

	fieldhw.oldheight = fieldhw.size.Y;
	fieldhw.size.X = (short)maxw;
	fieldhw.size.Y = (short)maxl;
	fieldhw.pos.X = 0;
	fieldhw.pos.Y = 0;
	fieldhw.sm.Top = HEIGHT - (short)maxl;
	fieldhw.sm.Left = 80;
	fieldhw.sm.Bottom = HEIGHT - 1;
	fieldhw.sm.Right = 80 + (short)maxw;

	k = 0;
	if(fieldhw.normAttrib != NULL)
		delete [] fieldhw.normAttrib;
	if(fieldhw.selAttrib != NULL)
		delete [] fieldhw.selAttrib;
	fieldhw.sz = maxl * maxw;
	ci = new CHAR_INFO[fieldhw.sz];
	fieldhw.normAttrib = new WORD[fieldhw.sz];
	fieldhw.selAttrib = new WORD[fieldhw.sz];
	for(i = 0; i < maxl; i++)
	{
		for(j = 0; j < maxw; j++)
		{
			ci[k].Char.AsciiChar = cfd[i][j];
			fieldhw.normAttrib[k] = FOREGROUND_INTENSITY;
			fieldhw.selAttrib[k] = 7 | FOREGROUND_INTENSITY;
			ci[k].Attributes = (hinfoboxchoice == 2 ? fieldhw.selAttrib[k] : fieldhw.normAttrib[k]);
			k++;
		}
	}
	delete [] cfd;

	for(i = (HEIGHT - maxl); i < HEIGHT; i++)
	{
		cwrite("", 80, i, 0, 52);
	}

	for(i = HEIGHT - playerhw.oldheight; i < HEIGHT - playerhw.size.Y; i++)
	{
		cwrite("", 80, i, 0, 52);
	}
	WriteConsoleOutputA(hScr, ci, fieldhw.size, fieldhw.pos, &fieldhw.sm);
	delete [] ci;

	fieldhw.maxval = (short)f->field.size();
	hostarrow(f, 2); // Set the arrow for the field infobox

	return 0;
}













bool card_alphabetical(pair<char*, short> p1, pair<char*, short> p2)
{
	return (_stricmp(p1.first, p2.first) < 0);
}
