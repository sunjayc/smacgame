#define NONE						 0 // Anytime I happen to need it to make code slightly easier to read

// Affiliations
#define AFF_S					0x0001 // S
#define AFF_M					0x0002 // M
#define AFF_A					0x0004 // A
#define AFF_C					0x0008 // C
#define AFF_I					0x0010 // !
#define AFF_AJ					0x0020 // AJ

// Card types
#define C_PLAY				0x00000001 // Card is playable at turn start (attack, item, field, etc.)
#define C_ATCK				0x00000002 // Attack card
#define C_CNTR				0x00000004 // Counter card
#define C_ITEM				0x00000008 // Item card
#define C_FFLD				0x00000010 // Field card
#define C_HEAL				0x00000020 // Heal card
#define C_SPCL				0x00000040 // Special card
#define C_ALLY				0x00000080 // Ally card
#define C_ENMY				0x00000100 // Enemy card
#define C_PRNK				0x00000200 // Prank card

// Card identifiers
#define ID_YMOM						 1 // Your Mom card
#define ID_BTHR						 2 // Bother card
#define ID_STRE						 3 // Stare card
#define ID_MILK						 4 // Milk card
#define ID_SODA						 5 // Soda card
#define ID_SRPS						 6 // RPS card
#define ID_EEMO						 7 // Emo card

// Attack types
#define TYPE_U					0x0010 // Uncounterable
#define TYPE_G					0x0001 // General
#define TYPE_P					0x0002 // Physical
#define TYPE_V					0x0004 // Verbal
#define TYPE_M					0x0008 // Mental

#define ID(l) (l & 0xFFFF0000)
#define INDEX(l) LOWORD(l)

// Hook specifics
#define HK_AFF				0x10000000 // Usage: HK_AFF | AFF_
#define HK_TYPE				0x20000000 // Usage: HK_TYPE | C_
#define HK_ATYPE			0x40000000 // Usage: HK_ATYPE | TYPE_
#define HK_SELCNTR			0x80000000 // For HOOK_SELECT.counters

// Card locations
#define LOC_HAND			0x00010000 // Usage: LOC_HAND | [hand index]
#define LOC_ITEM			0x00020000 // Usage: LOC_ITEM | [item index]
#define LOC_OTHR			0x00040000 // Usage: LOC_OTHR | [other index]
#define LOC_FFLD			0x00080000 // Usage: LOC_FFLD | [field index]
#define LOC_PRNK			0x00100000 // Usage: LOC_PRNK | [prank index]
#define LOC_DRAW			0x00200000 // Usage: LOC_DRAW | [draw index]
#define LOC_DCRD			0x00400000 // Usage: LOC_DCRD | [discard index]
#define LOC_ALLY			0x00800000 // Usage: LOC_ALLY | [ally index]
#define LOC_ENMY			0x01000000 // Usage: LOC_ENMY | [enemy index]
#define LOC_FLUX			0x02000000 // Everything in flux gets discarded at the end of each turn


/**
 * Server and client data holder for targeting (and also card selection).
 */
struct TARGET
{
	char name[35]; ///< Name to display
	short stat; ///< Health of player / targetable status (0:normal  -1:red  1:green  -2:yellow)
	short pnum; ///< This "player"'s player index number or the index of this card
};
