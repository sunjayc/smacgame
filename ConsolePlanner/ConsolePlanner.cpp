﻿#include <windows.h>
#include <string.h>
#include <stdio.h>

#define HEIGHT 25

HANDLE hOldscr;
HANDLE hScr;
CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
CONSOLE_CURSOR_INFO cciInfo;

void	InitWindow();
void	convert(char** argv);

unsigned long cwrite(char* str, int x, int y, WORD attr = 4|2|1)
{
	COORD coord;
	unsigned long lnum = 0;
	int len = strlen(str);

	coord.X = (short)x;
	coord.Y = (short)y;
	FillConsoleOutputCharacterA(hScr, ' ', 38, coord, &lnum);
	WriteConsoleOutputCharacterA(hScr, str, len, coord, &lnum);
	FillConsoleOutputAttribute(hScr, attr, len, coord, &lnum);

	return lnum;
}

void card()
{
	int i = 0;
	cwrite("ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿", 0, i++);
	cwrite("³                               SMAC!³", 0, i++);
	cwrite("³  12345678901234567890123456789012  ³", 0, i++);
	cwrite("³                                    ³", 0, i++);
	cwrite("³ ÚÄÄÄÄÂÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÂÄÄÄÄ¿ ³", 0, i++);
	cwrite("³ ³    ³1234567890123456789012³    ³ ³", 0, i++);
	cwrite("³ ³    ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ    ³ ³", 0, i++);
	cwrite("³ ³12345678901234567890123456789012³ ³", 0, i++);
	cwrite("³ ³2                               ³ ³", 0, i++);
	cwrite("³ ³3                               ³ ³", 0, i++);
	cwrite("³ ³4                               ³ ³", 0, i++);
	cwrite("³ ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ ³", 0, i++);
	cwrite("³  12345678901234567890123456789012  ³", 0, i++);
	cwrite("³  2                                 ³", 0, i++);
	cwrite("ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ", 0, i++);
}

void ymom()
{
	int i = 0;
	cwrite("ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿", 0, i++);
	cwrite("³                               SMAC ³", 0, i++);
	cwrite("³              Your Mom              ³", 0, i++);
	cwrite("³                                    ³", 0, i++);
	cwrite("³ ÚÄÄÄÄÂÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÂÄÄÄÄ¿ ³", 0, i++);
	cwrite("³ ³    ³        Attack        ³    ³ ³", 0, i++);
	cwrite("³ ³    ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ    ³ ³", 0, i++);
	cwrite("³ ³          Verbal Attack         ³ ³", 0, i++);
	cwrite("³ ³                                ³ ³", 0, i++);
	cwrite("³ ³                2               ³ ³", 0, i++);
	cwrite("³ ³                                ³ ³", 0, i++);
	cwrite("³ ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ ³", 0, i++);
	cwrite("³     Your mom is not cool... 7      ³", 0, i++);
	cwrite("³                                    ³", 0, i++);
	cwrite("ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ", 0, i++);
}

int main(int argc, char** argv)
{
	INPUT_RECORD inRec[128];
	DWORD numRead;
	int i, j;
	COORD coord;
	unsigned long lnum = 0;
	char buf[4];
	char box[] = "ÃÄ³Ú¿ÀÙÁÂ´";
	HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
	bool flag = true;
	WORD attr = 7;
	char tbuf[33];

	coord.X = 0;
	coord.Y = 0;

	if(argc > 1)
	{
		convert(argv);
		return 7;
	}

	InitWindow();
	SetConsoleMode(hIn, ENABLE_MOUSE_INPUT | ENABLE_PROCESSED_INPUT);

//	ymom();
	tbuf[32] = '\0';
	for(i = 0; i < 8; i++)
	{
		for(j = 0; j < 32; j++)
		{
			tbuf[j] = i * 32 + j;
		}
		cwrite(tbuf, 0, i);
	}

	while(flag)
	{
		ReadConsoleInput(hIn, inRec, 128, &numRead);
		for(i = 0; i < (signed)numRead; i++)
		{
			switch(inRec[i].EventType)
			{
				case KEY_EVENT:
				{
					if(inRec[i].Event.KeyEvent.bKeyDown && inRec[i].Event.KeyEvent.wVirtualKeyCode != VK_SHIFT && inRec[i].Event.KeyEvent.wVirtualKeyCode != VK_CONTROL && inRec[i].Event.KeyEvent.wVirtualKeyCode != VK_MENU)
					{
						if(inRec[i].Event.KeyEvent.dwControlKeyState & 0x0003) // ALT key
						{
							buf[0] = box[inRec[i].Event.KeyEvent.uChar.AsciiChar - '0'];
							buf[1] = 0;
							WriteConsoleOutputCharacterA(hScr, buf, 1, coord, &lnum);
							WriteConsoleOutputAttribute(hScr, &attr, 1, coord, &lnum);
							coord.X++;
						}
						else if(inRec[i].Event.KeyEvent.dwControlKeyState & 0x000C) // CTRL key
						{
							ReadConsoleOutputAttribute(hScr, &attr, 1, coord, &lnum);
							switch(inRec[i].Event.KeyEvent.wVirtualKeyCode)
							{
								case 0x31:
									attr ^= FOREGROUND_RED;
									break;
								case 0x32:
									attr ^= FOREGROUND_GREEN;
									break;
								case 0x33:
									attr ^= FOREGROUND_BLUE;
									break;
								case 0x34:
									attr ^= FOREGROUND_INTENSITY;
									break;
								case 0x35:
									attr ^= BACKGROUND_RED;
									break;
								case 0x36:
									attr ^= BACKGROUND_GREEN;
									break;
								case 0x37:
									attr ^= BACKGROUND_BLUE;
									break;
								case 0x38:
									attr ^= BACKGROUND_INTENSITY;
									break;
							}
							WriteConsoleOutputAttribute(hScr, &attr, 1, coord, &lnum);
						}
						else if(inRec[i].Event.KeyEvent.dwControlKeyState & ENHANCED_KEY) // ARROW key
						{
							if(inRec[i].Event.KeyEvent.wVirtualKeyCode == 40) // DOWN
							{
								coord.X--;
								coord.Y++;
							}
							break;
						}
						else
						{
							buf[0] = inRec[i].Event.KeyEvent.uChar.AsciiChar;
							buf[1] = 0;
							WriteConsoleOutputCharacterA(hScr, buf, 1, coord, &lnum);
							WriteConsoleOutputAttribute(hScr, &attr, 1, coord, &lnum);
							coord.X++;
						}
					}
					if(inRec[i].Event.KeyEvent.uChar.AsciiChar == 'Q')
						flag = false;
					break;
				}
				case MOUSE_EVENT:
				{
					if(inRec[i].Event.MouseEvent.dwEventFlags == 0)
					{
						coord.X = inRec[i].Event.MouseEvent.dwMousePosition.X;
						coord.Y = inRec[i].Event.MouseEvent.dwMousePosition.Y;
					}
					break;
				}
			}
		}
	}
}

void InitWindow()
{
	hOldscr = GetStdHandle(STD_OUTPUT_HANDLE);
	hScr = CreateConsoleScreenBuffer(
		GENERIC_READ |           // read/write access
		GENERIC_WRITE,
		0,                       // not shared
		NULL,                    // default security attributes
		CONSOLE_TEXTMODE_BUFFER, // must be TEXTMODE
		NULL);                   // reserved; must be NULL

/*	csbiInfo.dwSize.X = 38;
	csbiInfo.dwSize.Y = 15;
	csbiInfo.dwCursorPosition.X = 0;
	csbiInfo.dwCursorPosition.Y = 0;
	csbiInfo.wAttributes = 7;
	csbiInfo.srWindow.Left = 0;
	csbiInfo.srWindow.Top = 0;
	csbiInfo.srWindow.Right = 37;
	csbiInfo.srWindow.Bottom = 14;
	csbiInfo.dwMaximumWindowSize.X = 38;
	csbiInfo.dwMaximumWindowSize.Y = 15;
	cciInfo.bVisible = FALSE;
	cciInfo.dwSize = HEIGHT;*/
	csbiInfo.dwSize.X = 80+52;
	csbiInfo.dwSize.Y = HEIGHT;
	csbiInfo.dwCursorPosition.X = 0;
	csbiInfo.dwCursorPosition.Y = 0;
	csbiInfo.wAttributes = 7;
	csbiInfo.srWindow.Left = 0;
	csbiInfo.srWindow.Top = 0;
	csbiInfo.srWindow.Right = 79+52;
	csbiInfo.srWindow.Bottom = HEIGHT - 1;
	csbiInfo.dwMaximumWindowSize.X = 80+52;
	csbiInfo.dwMaximumWindowSize.Y = HEIGHT;
	cciInfo.bVisible = FALSE;
	cciInfo.dwSize = HEIGHT;

	SetConsoleScreenBufferSize(hScr, csbiInfo.dwSize);
	SetConsoleCursorPosition(hScr, csbiInfo.dwCursorPosition);
	SetConsoleTextAttribute(hScr, csbiInfo.wAttributes);
	SetConsoleWindowInfo(hScr, TRUE, &csbiInfo.srWindow);
	SetConsoleCursorInfo(hScr, &cciInfo);

	SetConsoleActiveScreenBuffer(hScr);

	return;
}

void convert(char** argv)
{
	FILE* fp;
	WCHAR wbuf[50][80];
	char abuf[50][80];
	int i, j, k = 0;
	WCHAR wbox[] = L"─│┌┐└┘├┤┬┴┼";
	char abox[] = "Ä³Ú¿ÀÙÃ´ÂÁÅ";
	WCHAR *w;

	fopen_s(&fp, argv[1], "r, ccs=UNICODE");
	while(feof(fp) == 0 && k < 80)
	{
		fgetws(wbuf[k++], 80, fp);
	}
	fclose(fp);
	for(i = 0; i < k; i++)
	{
		for(j = 0; j < 80; j++)
		{
			if(wbuf[i][j] == L'\0')
			{
				abuf[i][j] = '\0';
				break;
			}
			w = wcschr(wbox, wbuf[i][j]);
			if(w != NULL)
				abuf[i][j] = *((w - wbox) + abox);
			else
				abuf[i][j] = (char)wbuf[i][j];
		}
	}
	fopen_s(&fp, argv[1], "w");
	for(i = 0; i < k; i++)
		fputs(abuf[i], fp);
}