#include <deque>
using namespace std;
#include "SMAChost.h"
#include "SMAChelpers.h"

#define MAKE_FPROC(fname) \
	int fname(PLAYFIELD *f, ELIST *e, CARD *card, long target)
#define MAKE_HPROC(fname) \
	int fname(PLAYFIELD *f, class ELIST *e, CARD *card, CARD *ocard, short pnum, void* data)

#define cv (*e)[e->curev]
#define GET_TARGET(to, func) if(target == NOTARGET) \
	{ \
		Target(f, e, card, func, card->thisp); \
		return -2; \
	} \
	to = TO(target);
#define GET_TARGET_SPEC(to, func, type) if(target == NOTARGET) \
	{ \
		Target(f, e, card, func, card->thisp, type); \
		return -2; \
	} \
	to = TO(target);

int		NegativeOne(PLAYFIELD *f, ELIST *e, CARD *card, CARD *ocard, short pnum, void* data);
int		ForceNotMe(PLAYFIELD *f, ELIST *e, CARD *card, CARD *ocard, short pnum, void* data);
EVENT*	AffEvent(PLAYFIELD *f, CARD *card, short to, short S, short M, short A, short C, short I);
