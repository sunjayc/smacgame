#include "Hlist.h"

/**
 * Constructs a new HITEM.
 */
HITEM::HITEM()
{
	this->hfunc = NULL;
}

int HLIST::registerItem(CARD *card, HOOK hook, unsigned long specific, short priority, HPROC hfunc)
{
	HITEM* tmp;

	tmp = new HITEM;
	tmp->card = card;
	tmp->priority = priority;
	tmp->hfunc = hfunc;
	tmp->specific = specific;

	return addItem(hook, tmp);
}

int HLIST::callFunc(struct PLAYFIELD *f, ELIST *e, HOOK hook, int index, CARD *card, short pnum, void* data)
{
	HITEM* tmp;

	tmp = findItem(hook, index);
	return tmp->hfunc(f, e, tmp->card, card, pnum, data);
}

unsigned long HLIST::getSpec(HOOK hook, int index)
{
	return findItem(hook, index)->specific;
}

int HLIST::getNum(HOOK hook, short priority)
{
	int total = 0;
	deque<HITEM*>::const_iterator hitem;

	for(hitem = hlist[hook].begin(); hitem != hlist[hook].end(); hitem++)
	{
		if((*hitem)->priority == priority)
			total++;
		if((*hitem)->priority > priority) // List is sorted by priority so you can end here
			break;
	}

	return total;
}

int HLIST::getNextIndex(HOOK hook, short priority, int index)
{
	int i = index + 1;
	deque<HITEM*>::const_iterator hitem;

	if(i >= (signed)hlist[hook].size())
	{
		return -1;
	}

	hitem = hlist[hook].begin() + i; // Item after index
	while((*hitem)->priority < priority && i < (signed)hlist[hook].size())
	{
		hitem++;
		i++;
	}
	if((*hitem)->priority == priority)
		return i;

	return -1;
}

int HLIST::getMaxPriority(HOOK hook)
{
	int maxpriority = 0;
	deque<HITEM*>::const_iterator hitem;

	hlist[hook].begin();
	for(hitem = hlist[hook].begin(); hitem != hlist[hook].end(); hitem++)
	{
		if((*hitem)->priority > maxpriority)
			maxpriority = (*hitem)->priority;
	}

	return maxpriority;
}




int HLIST::deleteItem(HOOK hook, int index)
{
	hlist[hook].erase(hlist[hook].begin() + index);

	return 0;
}

void HLIST::removeItems(CARD *card, HOOK hook)
{
	unsigned int i;

	for(i = 0; i < hlist[hook].size(); i++)
	{
		if(hlist[hook][i]->card == card) // A rare case of intended pointer equivalence comparison
		{
			hlist[hook].erase(hlist[hook].begin() + i); // Remove the card from the list
			i--; // To counteract the i++ of the for loop since we just deleted an item
		}
	}
}

void HLIST::clearCard(CARD *card)
{
	map<HOOK, deque<HITEM*>>::iterator iter;
	unsigned int i;
	
	for(iter = hlist.begin(); iter != hlist.end(); iter++)
	{
		for(i = 0; i < iter->second.size(); i++)
		{
			if(iter->second[i]->card == card) // A rare case of intended pointer equivalence comparison
			{
				iter->second.erase(iter->second.begin() + i); // Remove the card from the list
				i--; // To counteract the i++ of the for loop since we just deleted an item
			}
		}
	}
}

void HLIST::clearList()
{
	hlist.clear();
}



HITEM* HLIST::findItem(HOOK hook, int index)
{
	return hlist[hook][index];
}

int HLIST::addItem(HOOK hook, HITEM *hitem)
{
	deque<HITEM*>::const_iterator iter;
	int i = 0;

	for(iter = hlist[hook].begin(); iter != hlist[hook].end(); iter++)
	{
		if((*iter)->priority > hitem->priority)
			break;
		i++;
	}
	hlist[hook].insert(iter, hitem);

	return i; // Return 0-based index of item just added
}